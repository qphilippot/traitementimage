#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>   
#include "../lib/C++/ImageBase.h"

using namespace std;

void seuiller256 (unsigned short*** img, int width, int height, int epa, int seuil) {
  for (int k=0; k<epa; k++) {
    for (int j=0; j<height; j++) {
      for (int i=0; i<width;i++) {
	if(img[i][j][k] > seuil) img[i][j][k] = 256;
	else img[i][j][k] = 0;
      }
    }
  }
}

void seuiller (unsigned short*** img, int width, int height, int epa, int seuil) {
  for (int k=0; k<epa; k++) {
    for (int j=0; j<height; j++) {
      for (int i=0; i<width;i++) {
	if(img[i][j][k] > seuil) img[i][j][k] = 4000;
	else img[i][j][k] = 0;
      }
    }
  }
}


void printTriangle(fstream& fs, double p1x, double p1y, double p1z, double p2x, double p2y, double p2z, double p3x, double p3y, double p3z) {
  fs << "facet normal 0 0 0" << endl;
  fs << "outer loop" << endl;
  fs << "vertex " << p1x << " " << p1y << " "<< p1z << endl;
  fs << "vertex " << p2x << " " << p2y << " "<< p2z << endl;
  fs << "vertex " << p3x << " " << p3y << " "<< p3z << endl;
  fs << "endloop" << endl;
  fs << "endfacet" << endl;

}
void addTriangle (fstream& fs, int i, int j, int k, int i2, int j2, int k2, double sizeX, double sizeY, double sizeZ) {
  double p1x, p1y, p1z, p2x, p2y, p2z, p3x, p3y, p3z, p4x, p4y, p4z;
  // voxel gauche
  if (i > i2) {
    p1x = (i-0.5) * sizeX;
    p1y = (j+0.5) * sizeY;
    p1z = (k+0.5) * sizeZ;

    p2x = (i-0.5) * sizeX;
    p2y = (j-0.5) * sizeY;
    p2z = (k+0.5) * sizeZ;

    p3x = (i-0.5) * sizeX;
    p3y = (j-0.5) * sizeY;
    p3z = (k-0.5) * sizeZ;

    p4x = (i-0.5) * sizeX;
    p4y = (j+0.5) * sizeY;
    p4z = (k-0.5) * sizeZ;
  }

  // droit
  if (i < i2) {
    p1x = (i+0.5) * sizeX;
    p1y = (j+0.5) * sizeY;
    p1z = (k+0.5) * sizeZ;

    p2x = (i+0.5) * sizeX;
    p2y = (j-0.5) * sizeY;
    p2z = (k+0.5) * sizeZ;

    p3x = (i+0.5) * sizeX;
    p3y = (j-0.5) * sizeY;
    p3z = (k-0.5) * sizeZ;

    p4x = (i+0.5) * sizeX;
    p4y = (j+0.5) * sizeY;
    p4z = (k-0.5) * sizeZ;
  }

  // bas
  if (j > j2) {
    p1x = (i+0.5) * sizeX;
    p1y = (j-0.5) * sizeY;
    p1z = (k+0.5) * sizeZ;

    p2x = (i-0.5) * sizeX;
    p2y = (j-0.5) * sizeY;
    p2z = (k+0.5) * sizeZ;

    p3x = (i+0.5) * sizeX;
    p3y = (j-0.5) * sizeY;
    p3z = (k-0.5) * sizeZ;

    p4x = (i-0.5) * sizeX;
    p4y = (j-0.5) * sizeY;
    p4z = (k-0.5) * sizeZ;
  }

  // haut
  if (j < j2) {
    p1x = (i+0.5) * sizeX;
    p1y = (j+0.5) * sizeY;
    p1z = (k+0.5) * sizeZ;

    p2x = (i-0.5) * sizeX;
    p2y = (j+0.5) * sizeY;
    p2z = (k+0.5) * sizeZ;

    p3x = (i+0.5) * sizeX;
    p3y = (j+0.5) * sizeY;
    p3z = (k-0.5) * sizeZ;

    p4x = (i-0.5) * sizeX;
    p4y = (j+0.5) * sizeY;
    p4z = (k-0.5) * sizeZ;
  }


  // devant
  if (k < k2) {
    p1x = (i-0.5) * sizeX;
    p1y = (j+0.5) * sizeY;
    p1z = (k+0.5) * sizeZ;

    p2x = (i+0.5) * sizeX;
    p2y = (j+0.5) * sizeY;
    p2z = (k+0.5) * sizeZ;

    p3x = (i+0.5) * sizeX;
    p3y = (j-0.5) * sizeY;
    p3z = (k+0.5) * sizeZ;

    p4x = (i-0.5) * sizeX;
    p4y = (j-0.5) * sizeY;
    p4z = (k+0.5) * sizeZ;
  }


  // derriere
  if (k > k2) {
    p1x = (i-0.5) * sizeX;
    p1y = (j+0.5) * sizeY;
    p1z = (k-0.5) * sizeZ;

    p2x = (i+0.5) * sizeX;
    p2y = (j+0.5) * sizeY;
    p2z = (k-0.5) * sizeZ;

    p3x = (i+0.5) * sizeX;
    p3y = (j-0.5) * sizeY;
    p3z = (k-0.5) * sizeZ;

    p4x = (i-0.5) * sizeX;
    p4y = (j-0.5) * sizeY;
    p4z = (k-0.5) * sizeZ;
  }

    printTriangle(fs, p1x, p1y, p1z, p2x, p2y, p2z, p4x, p4y, p4z);
    printTriangle(fs, p3x, p3y, p3z, p4x, p4y, p4z, p2x, p2y, p2z);

}

int main(int argc, char **argv) {
  if (argc != 9) {
    printf("Usage: Image.img height width pronf seuil sizeX sizeY sizeZ\n"); 
    return 1;
  }

  const char* filename = argv[1];
  int width = atoi(argv[2]);
  int height = atoi(argv[3]);
  int epa = atoi(argv[4]);
  int seuil = atoi(argv[5]);
  double sizeX = atoi(argv[6]);
  double sizeY = atoi(argv[7]);
  double sizeZ = atoi(argv[8]);
  
  FILE* file = fopen(filename, "rb");
  unsigned char elt[2];
  
  // création image :
  unsigned short*** img = new unsigned short** [width];
  for (int i=0; i<width;i++) {
    img[i] = new unsigned short* [height];
    for (int j=0; j<height; j++) {
      img[i][j] = new unsigned short [epa];
    }
  }

  


  // Lecture image :
  for (int k=0; k<epa; k++) {
    for (int j=0; j<height; j++) {
      for (int i=0; i<width;i++) {
	fread(&elt, sizeof(elt), 1, file);
	unsigned short line = (elt[0]*256 + elt[1]);
	img[i][j][k] = line;
      }
    }
  }
  
  seuiller(img, width, height, epa, seuil);  
  
  fstream fs;
  fs.open("img2.stl", fstream::out);
  fs << "solid name" << endl;
  for (int i=1; i < (width-1); i++) {
    for (int j=1; j < (height-1); j++) {
      for (int k=1; k < (epa-1); k++) {
	if (img[i][j][k] > 0) {
	  if (img[i-1][j][k] == 0) {
	    addTriangle(fs, i, j, k, i-1, j, k, sizeX, sizeY, sizeZ);
	  }
	  if (img[i+1][j][k] == 0) {
	    addTriangle(fs, i, j, k, i-1, j, k, sizeX, sizeY, sizeZ);
	  }
	  if (img[i][j][k-1] == 0) {
	    addTriangle(fs, i, j, k, i, j, k-1, sizeX, sizeY, sizeZ);
	  }
	  if (img[i][j][k+1] == 0) {
	    addTriangle(fs, i, j, k, i, j, k+1, sizeX, sizeY, sizeZ);
	  }
	  if (img[i][j-1][k] == 0) {
	    addTriangle(fs, i, j, k, i, j-1, k, sizeX, sizeY, sizeZ);
	  }
	  if (img[i][j+1][k] == 0) {
	    addTriangle(fs, i, j, k, i, j+1, k, sizeX, sizeY, sizeZ);
	  }
	}
      }
    }
  }
  fs << "endsolid name" << endl;
  fs.close();
  //
  
  /*
  ImageBase im(height, width, false);
    
  for (int i=0; i< width;i++) {
     for (int j=0; j< height;j++) {
       int max = 0;
        for (int k=0; k<epa;k++) {
	  if (img[i][j][k] > max) max = img[i][j][k]; 
	}
	im[height-j-1][i] = max;
     }
  }
  im.save("imageMIP.ppm");

  

  
  for (int i=0; i< width;i++) {
     for (int j=0; j< height;j++) {
       int min = 256;
        for (int k=0; k<epa;k++) {
	  if (img[i][j][k] < min) min = img[i][j][k]; 
	}
	im[height-1-j][i] = min;
     }
  }
  
  im.save("imageMinIP.ppm");


   for (int i=0; i< width;i++) {
     for (int j=0; j< height;j++) {
       int sum = 0;
        for (int k=0; k<epa;k++) {
	  sum += img[i][j][k];
	}
	im[height-j-1][i] = int(double(sum) / epa);
     }
   }
  
  im.save("imageAIP.ppm");
  */  
}
