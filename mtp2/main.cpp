#include "../lib/C++/ImageBase.h"
#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <sstream>
#include <string>
#include <cmath>
#include <algorithm>

using namespace std;

ImageBase extractCanal(ImageBase& originale, const unsigned int canal) {
  const unsigned int w = originale.getWidth();
  const unsigned int h = originale.getHeight();

  ImageBase copy (w, h, false);
  for (int i=0; i < h; ++i) {
    for (int j=0; j < w; ++j) {
      copy[i][j] = originale [i * 3] [j * 3 + canal];
    }   
  }
  return copy;
}

ImageBase compress(ImageBase& img) {
  const unsigned int w = img.getWidth() / 2;
  const unsigned int h = img.getHeight() / 2;

  ImageBase copy (w, h, false);
  for (int i=0; i < h; ++i) {
    for (int j=0; j < w; ++j) {
      copy[i][j] = img [i * 2] [j * 2];
    }   
  }
  return copy;
}

ImageBase merge(ImageBase& major, ImageBase& c1, ImageBase& c2) {
  const unsigned int w = major.getWidth();
  const unsigned int h = major.getHeight();

  ImageBase copy (w, h, true);
  for (int i=0; i < h; ++i) {
    for (int j=0; j < w; ++j) {
      copy[i * 3][j * 3] = major [i] [j];
      if ((i%2) && (j%2)) {
        copy[i * 3][j*3 + 1] = c1[i / 2][j / 2];
        copy[i * 3][j*3 + 2] = c2[i / 2][j / 2];
      } else {
        if ( (i < (h-1)) && (j < (w-1))) {
          copy[i * 3][j*3 + 1] = (c1[i / 2][j / 2] + c1[(i + 1) / 2][(j + 1) / 2]) / 2;
          copy[i * 3][j*3 + 2] = (c2[i / 2][j / 2] + c2[(i + 1) / 2][(j + 1) / 2]) / 2;  
        } else {
          copy[i * 3][j*3 + 1] = 255;
          copy[i * 3][j*3 + 2] = 255;   
        }
        
      }
    }   
  }
  return copy;
}

ImageBase mergeYCRCB(ImageBase& y, ImageBase& cr, ImageBase& cb) {
  const unsigned int w = y.getWidth();
  const unsigned int h = y.getHeight();

  ImageBase copy (w, h, true);
  for (int i=0; i < h; ++i) {
    for (int j=0; j < w; ++j) {
      char vCr = 0, vCb = 0;
      
      if ((i%2) && (j%2)) { // si impair
        vCr = cr[i/2][j/2];
        vCb = cb[i/2][j/2];
      } else {
        if ( (i < (h-1)) && (j < (w-1))) {
          vCr = (cr[i / 2][j / 2] + cr[(i + 1) / 2][(j + 1) / 2]) / 2;
          vCb = (cb[i / 2][j / 2] + cb[(i + 1) / 2][(j + 1) / 2]) / 2;  
        } else {
          vCb = 255;
          vCr = 255;   
        }
      }
      

      copy[i * 3][j * 3] = std::max(0, std::min(255, int(double(y[i][j]) + double(1.402 * char(vCr - 128)))));
      copy[i * 3][j * 3 + 1] = std::max(0, std::min(255,int(double(y[i][j]) - double(0.344 * char(vCb - 128)) - double(0.714 * char(vCr - 128)))));
      copy[i * 3][j * 3 + 2] = std::max(0, std::min(255,int(double(y[i][j]) + double(1.772 * char(vCb - 128)))));
    }   
  }
  return copy;
}

double EQM (ImageBase& originale, ImageBase& compressed) {
  const unsigned int w = originale.getWidth();
  const unsigned int h = originale.getHeight();
  const unsigned int N = h*w;

  unsigned int sum = 0;
  for (int i = 0; i < h; ++i) {
    for (int j = 0; j < w; ++j) {
      sum += (originale[i * 3][j * 3] - compressed[i * 3][j * 3]) * (originale[i * 3][j * 3] - compressed[i * 3][j * 3]);
      sum += (originale[i * 3][j * 3 + 1] - compressed[i * 3][j * 3 + 1]) * (originale[i * 3][j * 3 +1] - compressed[i * 3][j * 3 + 1]);
      sum += (originale[i * 3][j * 3 + 2] - compressed[i * 3][j * 3 + 2]) * (originale[i * 3][j * 3 +2] - compressed[i * 3][j * 3 + 2]);
    }
  }

  return double(sum) / (3 * N); 
}


double PSNR (ImageBase& originale, ImageBase& compressed) {
  double eqm = EQM(originale, compressed);
  return 10 * log(double(255*255) / eqm);
}

ImageBase getY(ImageBase& img) {
  const unsigned int w = img.getWidth();
  const unsigned int h = img.getHeight();
  ImageBase lum (w, h, false);
  for (int i = 0; i < h; ++i) {
    for (int j = 0; j < w; ++j) {
      lum[i][j] = double(0.299 * img[i * 3][j * 3]) + double(0.587 * img[i*3][j*3 + 1]) + double(0.114 * img[i*3][j*3 + 2]);
    }
  }
  lum.save("y.pgm");
  return lum;
}

ImageBase getCb(ImageBase& img, ImageBase& lum) {
  const unsigned int w = img.getWidth();
  const unsigned int h = img.getHeight();
  ImageBase cb (w, h, false);
  for (int i = 0; i < h; ++i) {
    for (int j = 0; j < w; ++j) {
      cb[i][j] = double(-0.1687 * img[i * 3][j * 3]) - double(0.331 * img[i*3][j*3 + 1]) + double(0.499 * img[i*3][j*3 + 2]) + 128;
      //cb[i][j] = double(img[i * 3][j * 3 + 2] - lum[i][j]) / ( 2 - 2 * 0.114 * img[i * 3][j*3+2]) + 128;
    
    }
  }

  cb.save("cb.pgm");
  return cb;
}

ImageBase getCr(ImageBase& img, ImageBase& lum) {
  const unsigned int w = img.getWidth();
  const unsigned int h = img.getHeight();
  ImageBase cr (w, h, false);
  for (int i = 0; i < h; ++i) {
    for (int j = 0; j < w; ++j) {
      cr[i][j] = double(0.499 * img[i * 3][j * 3]) - double(0.418 * img[i*3][j*3 + 1]) - double(0.0813 * img[i*3][j*3 + 2]) + 128;
      //cr[i][j] = double(img[i * 3][j * 3] - lum[i][j]) / (2 - 2 * 0.229 * img[i * 3][j * 3]) + 128;
    }
  }

  cr.save("cr.pgm");
  return cr;
}

int main(int argc, char **argv) {
	if (argc != 2) {
		printf("Usage: prog <fileName.raw> \n"); 
		return 1;
	}
	
	char* fileName = argv[1];
	
  ImageBase originale;
  originale.load(fileName);

  ImageBase rouge = extractCanal(originale, 0);
  ImageBase vert = extractCanal(originale, 1);
  ImageBase bleu = extractCanal(originale, 2);

  rouge.save("rouge-originale.pgm");
  vert.save("vert-originale.pgm");
  bleu.save("bleu-originale.pgm");

  ImageBase r = compress(rouge);


  ImageBase v = compress(vert);
  ImageBase b = compress(bleu);

  r.save("rouge-compress.pgm");
  v.save("vert-compress.pgm");
  b.save("bleu-compress.pgm");

  ImageBase compressed = merge(rouge, v, b);
  compressed.save("compressed.pgm");

  cout << PSNR(originale, compressed) << endl;

  ImageBase y = getY(originale);
  ImageBase cr = getCr(originale, y);
  ImageBase cb = getCb(originale, y);

  ImageBase cY = compress(y);
  ImageBase cCr = compress(cr);
  ImageBase cCb = compress(cb);

cCb.save("ccb.pgm");
cCr.save("ccr.pgm");
  ImageBase ycrcb = mergeYCRCB(y, cCr, cCb);
  ycrcb.save("ycrcb.ppm");


  cout << PSNR(ycrcb, originale) << endl;
  
	return 0;
}
