#include "../lib/C++/ImageBase.h"


#include <iostream>
#include <cstdlib>
#include <ctime>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <sstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <blitz/tinymat2.h>
#include <bitset>
 


using namespace std; 
 
void genererPlaine(int* hauteur) {
  hauteur[0] = 50;
  for (int i = 1; i < 300; ++i) {
    hauteur[i] = hauteur[i-1] - 1 + rand() % 3;
  }
}
  

int main(int argc, char **argv) {

srand(time(0));

typedef struct Bloc {
  int type;
  unsigned char r, v, b;
  std::vector<double> proba;
} Bloc;


int *hauteur = new int[300];
//genererPlaine(hauteur);
  hauteur[0] = 50;
  for (int i = 1; i < 300; ++i) {
    int plus = rand() % 6;
    hauteur[i] = ((hauteur[i-1] - 1 + rand() % 6));
  }

Bloc a;
a.type = 0;
std::vector<double> voisinA;
voisinA.push_back(1.0);
voisinA.push_back(0.4);
voisinA.push_back(0.0);
voisinA.push_back(0.0);
a.proba = voisinA;
a.r = 255;
a.v = 0;
a.b = 0;

Bloc b;
b.type = 1;
std::vector<double> voisinB;
voisinB.push_back(0);
voisinB.push_back(1.5);
voisinB.push_back(0.4);
voisinB.push_back(0.01);
b.proba = voisinB;
b.r = 130;
b.v = 130;
b.b = 130;


Bloc c;
c.type = 2;
std::vector<double> voisinC;
voisinC.push_back(0);
voisinC.push_back(0.3);
voisinC.push_back(1.5);
voisinC.push_back(0.01);
c.proba = voisinC;
c.r = 220;
c.v = 180;
c.b = 220;

Bloc d;
d.type = 3;
std::vector<double> voisinD;
voisinD.push_back(0);
voisinD.push_back(0);
voisinD.push_back(0);
voisinD.push_back(2);
d.proba = voisinD;
d.r = 0;
d.v = 0;
d.b = 255;

Bloc blocs[4];
blocs[0] = a;
blocs[1] = b;
blocs[2] = c;
blocs[3] = d;


Bloc x;
d.type = 5;
std::vector<double> voisinX;
voisinX.push_back(0);
voisinX.push_back(0);
voisinX.push_back(0);
voisinX.push_back(0);
x.proba = voisinX;
x.r = 0;
x.v = 0;
x.b = 0;


ImageBase img (300, 100, true);
Bloc mat[100][300];

for (int i=0; i < 100; ++i) {
  for (int j=0; j < 300; ++j) {
    for (int k=0; k < 4; ++k) {
      mat[i][j].proba.push_back(0);
    }
    mat[i][j] = x;
  }  
}

// Remplissage ligne basse;
for (int i=0; i < 300; ++i) {
  mat[99][i] = blocs[0];
  //mat[288][i].proba = a.proba;
}

// première passe;
for (int i=99; i > 0; --i) {
  for (int j=0; j < 300; ++j) {
    
    int numeroBlocSuppose = 0;
    if (hauteur[j] < i) {
      if (i != 99) {
        double sum = 0;
        int random = rand() % 100;
      
      
        // On normalise les coefficients de proba 
        for (int k = 0; (k < 4); ++k) {
          sum += mat[i][j].proba[k];
        }

        //sum += mat[i][j].proba[3] * (double(i) / 100);


        //double coeff = ((double (i) / 100) < 0.4) ? (double (i) / 100) : 1; 
        //sum += mat[i][j].proba[3] * coeff ;

        if (sum == 0) { 
          sum = 1.0;
        }
        double proba [4];
        for (int k = 0; (k < 4); ++k) {
          proba[k] = mat[i][j].proba[k] / sum;
        }
        sum = 0;

        // On donne un type de bloc probable
        for (int k = 0; (k < 4) && (numeroBlocSuppose == 0); ++k) {
          sum += proba[k] * 100;
          if (random < sum) {
            numeroBlocSuppose = k;
            break;
          }
        }  
      }

      // diffusion
      if (i >= 1) {
        mat[i - 1][j].proba[0] += blocs[numeroBlocSuppose].proba[0];
        mat[i - 1][j].proba[1] += blocs[numeroBlocSuppose].proba[1];
        mat[i - 1][j].proba[2] += blocs[numeroBlocSuppose].proba[2];
        mat[i - 1][j].proba[3] += blocs[numeroBlocSuppose].proba[3];

        mat[i - 1][(j + 1) % 300].proba[1] += blocs[numeroBlocSuppose].proba[1];
        mat[i - 1][(j + 1) % 300].proba[2] += blocs[numeroBlocSuppose].proba[2];
        mat[i - 1][(j + 1) % 300].proba[3] += blocs[numeroBlocSuppose].proba[3];
    
        mat[i - 1][(j - 1) % 300].proba[1] += blocs[numeroBlocSuppose].proba[1];
        mat[i - 1][(j - 1) % 300].proba[2] += blocs[numeroBlocSuppose].proba[2];
        mat[i - 1][(j - 1) % 300].proba[3] += blocs[numeroBlocSuppose].proba[3];

        mat[i][(j + 1) % 300].proba[0] += blocs[numeroBlocSuppose].proba[0];
        mat[i][(j + 1) % 300].proba[1] += blocs[numeroBlocSuppose].proba[1];
        mat[i][(j + 1) % 300].proba[2] += blocs[numeroBlocSuppose].proba[2];
        mat[i][(j + 1) % 300].proba[3] += blocs[numeroBlocSuppose].proba[3];
        
        mat[i][(j - 1) % 300].proba[0] += blocs[numeroBlocSuppose].proba[0];
        mat[i][(j - 1) % 300].proba[1] += blocs[numeroBlocSuppose].proba[1];
        mat[i][(j - 1) % 300].proba[2] += blocs[numeroBlocSuppose].proba[2];
        mat[i][(j - 1) % 300].proba[3] += blocs[numeroBlocSuppose].proba[3];
      }

      if (i < 99) {
        mat[(i + 1) % 100][j].proba[1] += blocs[numeroBlocSuppose].proba[1] / 2;
        mat[(i + 1) % 100][j].proba[2] += blocs[numeroBlocSuppose].proba[2] / 2;
        mat[(i + 1) % 100][j].proba[3] += blocs[numeroBlocSuppose].proba[3] / 2;


        mat[(i + 1) % 100][(j + 1) % 300].proba[1] += blocs[numeroBlocSuppose].proba[1] / 2;
        mat[(i + 1) % 100][(j + 1) % 300].proba[2] += blocs[numeroBlocSuppose].proba[2] / 2;
        mat[(i + 1) % 100][(j + 1) % 300].proba[3] += blocs[numeroBlocSuppose].proba[3] / 2;
        
        mat[(i + 1) % 100][(j - 1) % 300].proba[1] += blocs[numeroBlocSuppose].proba[1] / 2;
        mat[(i + 1) % 100][(j - 1) % 300].proba[2] += blocs[numeroBlocSuppose].proba[2] / 2;
        mat[(i + 1) % 100][(j - 1) % 300].proba[3] += blocs[numeroBlocSuppose].proba[3] / 2;
      }
    } else {
      numeroBlocSuppose = 3;
    }


    
    img[i * 3][j * 3] = blocs[numeroBlocSuppose].r;
    img[i * 3][j * 3 + 1] = blocs[numeroBlocSuppose].v;
    img[i * 3][j * 3 + 2] = blocs[numeroBlocSuppose].b;
  }
}

img.save("1-passe.ppm");
/*
// 2nd passe;
for (int i=99; i > 0; --i) {
  for (int j=299; j > 0; --j) {
    
    int numeroBlocSuppose = 0;
    if (i != 99) {
      double sum = 0;
      int random = rand() % 100;
    
    
      // On normalise les coefficients de proba 
      for (int k = 0; (k < 4); ++k) {
        sum += mat[i][j].proba[k];
      }


      if (sum == 0) { 
        sum = 1.0;
      }
      //cout << sum << endl;

      double proba[4];
      for (int k = 0; (k < 4); ++k) {
      
          proba[k] = mat[i][j].proba[k] / sum;
        //  cout << mat[i][j].proba[k] << " ";
     
      }
      //cout << endl;

      sum = 0;

      // On donne un type de bloc probable
      for (int k = 0; (k < 4) && (numeroBlocSuppose == 0); ++k) {
        sum += proba[k] * 100;
        if (random < sum) {
          numeroBlocSuppose = k;
          break;
        }
      }  
    }


    
    img[i * 3][j * 3] = blocs[numeroBlocSuppose].r;
    img[i * 3][j * 3 + 1] = blocs[numeroBlocSuppose].v;
    img[i * 3][j * 3 + 2] = blocs[numeroBlocSuppose].b;

    // diffusion
    if (i >= 1) {
      //cout << "type du bloc : " << mat[i][j].type << endl;
      //cout << " mes probas : " << mat[i][j].proba << endl;     
      

      mat[i][(j + 1) % 300].proba[0] += blocs[numeroBlocSuppose].proba[0];
      mat[i][(j + 1) % 300].proba[1] += blocs[numeroBlocSuppose].proba[1];
      mat[i][(j + 1) % 300].proba[2] += blocs[numeroBlocSuppose].proba[2];
      mat[i][(j + 1) % 300].proba[3] += blocs[numeroBlocSuppose].proba[3];
      
      mat[i][(j - 1) % 300].proba[0] += blocs[numeroBlocSuppose].proba[0];
      mat[i][(j - 1) % 300].proba[1] += blocs[numeroBlocSuppose].proba[1];
      mat[i][(j - 1) % 300].proba[2] += blocs[numeroBlocSuppose].proba[2];
      mat[i][(j - 1) % 300].proba[3] += blocs[numeroBlocSuppose].proba[3];

//
  
    }
  }
}
*/

// On fixe pour voir 
/*
for (int j=0; j < 300; ++j) {
  for (int i=0; i < 100; ++i) {
    int numeroBlocSuppose = 0;
    double sum = 0;
    int random = rand() % 100;


      // On normalise les coefficients de proba 
      for (int k = 0; (k < 4); ++k) {
        sum += mat[i][j].proba[k];
      }

      //sum += mat[i][j].proba[3] * i /100;

      if (sum == 0) { 
        sum = 1.0;
      }
      //cout << sum << endl;
      for (int k = 0; (k < 4); ++k) {
      
          mat[i][j].proba[k] /= sum;
        //  cout << mat[i][j].proba[k] << " ";
     
      }
      //cout << endl;

    sum = 0;

      // On donne un type de bloc probable
      for (int k = 0; (k < 4) && (numeroBlocSuppose == 0); ++k) {
        sum += mat[i][j].proba[k] * 100;
        if (random < sum) {
          numeroBlocSuppose = k;
          break;
        }
      }
      mat[i][j].type = numeroBlocSuppose;
    img[i * 3][j * 3] = blocs[numeroBlocSuppose].r;
    img[i * 3][j * 3 + 1] = blocs[numeroBlocSuppose].v;
    img[i * 3][j * 3 + 2] = blocs[numeroBlocSuppose].b;    
  }
}

  img.save("2-passe.ppm");
  */

/*
ImageBase originale;
originale.copy(img);


// erosion 
char **mask = new char *[100];

for (int i=0; i < 100; ++i) {
  mask[i] = new char[300];

  for (int j=0; j < 300; ++j) {
   mask[i][j] = (img[i * 3][j * 3 + 2] == 255);
  }
}

// première passe;
for (int i=99; i > 0; --i) {
  for (int j=0; j < 300; ++j) {
    if (mask[i][j]) {
      mat[i][j+1].type = 3;
      img[i * 3][((j + 1) % 300) * 3] = 0;
      img[i * 3][((j + 1) % 300) * 3 + 1] = 0;
      img[i * 3][((j + 1) % 300) * 3 + 2] = 255;


if (j > 1) {

      mat[i][j-1].type = 3;
      img[i * 3][((j - 1) % 300) * 3] = 0;
      img[i * 3][((j - 1) % 300) * 3 + 1] = 0;
      img[i * 3][((j - 1) % 300) * 3 + 2] = 255;

}
  
if (i > 1) {

      mat[i-1][j].type = 3;
      img[((i - 1) % 100) * 3][j * 3] = 0;
      img[((i - 1) % 100) * 3][j * 3 + 1] = 0;
      img[((i - 1) % 100) * 3][j * 3 + 2] = 255;
}
  

      mat[i+1][j].type = 3;
      img[((i + 1) % 100) * 3][j * 3] = 0;
      img[((i + 1) % 100) * 3][j * 3 + 1] = 0;
      img[((i + 1) % 100) * 3][j * 3 + 2] = 255;


    }
  }
}

  img.save("2-passe-1-erosion.ppm");
*/
/*
// erosion 2
for (int i=0; i < 100; ++i) {
  for (int j=0; j < 300; ++j) {
   //mask[i][j] = (mat[i][j].type == 3);
   mask[i][j] = (img[i * 3][j * 3 + 2] == 255);
  }
}

// première passe;
for (int i=99; i > 0; --i) {
  for (int j=0; j < 300; ++j) {
    if (mask[i][j]) {
      
      img[i * 3][((j + 1) % 300) * 3] = 0;
      img[i * 3][((j + 1) % 300) * 3 + 1] = 0;
      img[i * 3][((j + 1) % 300) * 3 + 2] = 255;


if (j > 1) {
      img[i * 3][((j - 1) % 300) * 3] = 0;
      img[i * 3][((j - 1) % 300) * 3 + 1] = 0;
      img[i * 3][((j - 1) % 300) * 3 + 2] = 255;

}
  
if (i > 1) {
      img[((i - 1) % 100) * 3][j * 3] = 0;
      img[((i - 1) % 100) * 3][j * 3 + 1] = 0;
      img[((i - 1) % 100) * 3][j * 3 + 2] = 255;
}
  
      img[((i + 1) % 100) * 3][j * 3] = 0;
      img[((i + 1) % 100) * 3][j * 3 + 1] = 0;
      img[((i + 1) % 100) * 3][j * 3 + 2] = 255;


    }
  }
}

  img.save("2-passe-2-erosion.ppm");
*/
  // On applatie
  /*
  for (int i=0; i < 99; i++) {
    for (int j=1; j < 300; ++j) {
     if (img[i * 3][j * 3 + 2] == 255) {
        if ((img[i * 3][(j-1) * 3 + 2] != 255) && (img[(i+1) * 3][j * 3 + 2] != 255) && ((rand() % 100) < 90)) {
          img[i * 3][j * 3] = img[i * 3][(j-1) * 3];
          img[i * 3][j * 3 + 1] = img[i * 3][(j-1) * 3 + 1];
          img[i * 3][j * 3 + 2] = img[i * 3][(j-1) * 3 + 2]; 
        } 
      }
    }
  }

   img.save("applatie.ppm");

   ImageBase diff(100, 300, false);

  for (int i=0; i < 100; ++i) {
    for (int j=0; j < 100; ++j) {
      bool same = (originale[i*3][j*3] == img[i*3][j*3]) && (originale[i*3][j*3 + 1] == img[i*3][j*3 + 1]) && (originale[i*3][j*3 + 2] == img[i*3][j*3 + 2]);
      diff[i][j] = same ? 0 : 255;   
    }    
  }

  diff.save("diff.pgm");
  */

  delete[] hauteur;
  return 0;
}


///
  
  
  

