#include "../lib/C++/ImageBase.h"


#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <sstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <fstream>

using namespace std;

unsigned char charte [72] = {
  115, 82, 68,
  194,150,130,
  98,122,157,
  87, 108, 67,
  133, 128, 177,
  103, 189, 170,
  214, 126, 44,
  80, 91, 166,
  193, 90, 99,
  94, 60 ,108,
  157, 188, 64,
  224, 163, 46,
  56, 61, 150,
  70, 148, 73,
  175, 54, 60,
  231, 199, 31,
  187, 86, 149,
  8, 133, 161,
  43, 243, 243,
  200, 200, 200,
  160, 160, 160,
  122, 122, 121,
  85, 85, 85,
  52, 52, 52
};



ImageBase fillChart( std::string path ){
  std::vector<unsigned char> buffer;
  ImageBase chart = ImageBase(6, 4, true);
  ifstream stream (path.c_str());
  std::string line = "";

  if(stream.is_open()){
    while (getline(stream, line)) {
      buffer.push_back((unsigned char) atoi(line.c_str()));
    } 
  }

  unsigned char elt;
  unsigned int x = 0;
  unsigned int y = 0;
  unsigned int offset = 0;
  const unsigned int bufferLength = buffer.size();

  for (unsigned int i = 0; i < bufferLength; ++i) {
    elt = buffer.at(i);
    chart[x*3][y*3 + offset] = elt;
    //std::cout << offset << " " << y << " " << x << std::endl;
    ++offset;
    
    if (offset == 3) {
      offset = 0;
      ++y;
    }
    
    if(y == 6) {
      y = 0;
      ++x;
    }
  }
  
  chart.save("testChart.ppm");
  return chart;
}

// Image couleur seulement
int* diffMoy(ImageBase& org, ImageBase& comp){
  int* avg = new int[3]();

  avg[0] = 0;
  avg[1] = 0;
  avg[2] = 0;

  const unsigned int height = org.getHeight();
  const unsigned int width = org.getWidth();
  const unsigned int nbElt = height * width;


  for (unsigned int x = 0; x < height; ++x) {
    for (unsigned int y = 0; y < width; ++y) {
      avg[0] += org[x*3][y*3+0] - comp[x*3][y*3+0];
      avg[1] += org[x*3][y*3+1] - comp[x*3][y*3+1];
      avg[2] += org[x*3][y*3+2] - comp[x*3][y*3+2];
    }
  }


  avg[0] /= (int) nbElt;
  avg[1] /= (int) nbElt;
  avg[2] /= (int) nbElt;


  //  std::cout << avg[0] << " " << avg[1] << " " << avg[2] << std::endl;
  return avg;
}


void appliquerDiff(ImageBase& img, int* diff){
  int R = 0;
  int G = 0;
  int B = 0;

  const unsigned int height = img.getHeight();
  const unsigned int width = img.getWidth();
  
  for (unsigned int x = 0; x < height; ++x ){
    for (unsigned int y = 0; y < width; ++y ){
      R = (int) img[x*3][y*3+0] + diff[0];
      G = (int) img[x*3][y*3+1] + diff[1];
      B = (int) img[x*3][y*3+2] + diff[2];
      
      img[x*3][y*3+0] = std::max(0, std::min(255, R));
      img[x*3][y*3+1] = std::max(0, std::min(255, G));
      img[x*3][y*3+2] = std::max(0, std::min(255, B));
    }
  }

  img.save("appDiff.ppm");
}



float getEntropy(float* proba){
  float res = 0.0f;

  for (unsigned int i = 0; i < 256; ++i) {
    res -= proba[i] * (std::log(proba[i]) / std::log(2));
  }

  return res;
}


float* proba(int* histo, size_t w, size_t h){
  float* res = new float[256]();
  for( size_t i = 0; i < 256; ++i ){
    res[i] = (float)histo[i] / (float)(w*h);
  }
  return res;
}

unsigned int* histogramme(int** img, size_t w, size_t h){
  unsigned int * res = new unsigned int[256]();
  for(size_t x = 0 ; x < h ; ++x){
    for(size_t y = 0 ; y < w ; ++y){
      ++res[ img[x][y] ];
    }
  }
  return res;
}



void histowrite(unsigned int* histo,const char* nom){
  std::ofstream out;
  out.open(nom);
  for(size_t i = 0 ; i < 256 ; ++i){
    out << i << " " <<histo[ i ] << std::endl;
  }
  out.close();
}

void freeMemory(unsigned char** composante, size_t height){
  for( size_t i = 0; i < height; ++i ){ delete[] composante[i]; }
  delete[] composante;
}



void PSNR(ImageBase& org, ImageBase& res){
  size_t N = org.getHeight() * org.getWidth();
  float EQMR = 0.0f;
  float EQMG = 0.0f;
  float EQMB = 0.0f;
  float EQM_global = 0.0f;
  
  for( size_t x = 0; x < (size_t)org.getHeight(); ++x ){
    for( size_t y = 0; y < (size_t)org.getWidth(); ++y ){
      EQMR += pow( org[x*3][y*3+0] - res[x*3][y*3+0], 2);
      EQMG += pow( org[x*3][y*3+1] - res[x*3][y*3+1], 2);
      EQMB += pow( org[x*3][y*3+2] - res[x*3][y*3+2], 2);
    }
  }
  EQM_global = EQMR + EQMG + EQMB;
  EQM_global /= 3*N;
  EQMR /= N;
  EQMG /= N;
  EQMB /= N;
  
  
  float PSNR_R = 10 * std::log( (255.0f * 255.0f) / EQMR );
  float PSNR_G = 10 * std::log( (255.0f * 255.0f) / EQMG );
  float PSNR_B = 10 * std::log( (255.0f * 255.0f) / EQMB );
  float PSNR_global = 10 * std::log( (255.0f * 255.0f) / EQM_global );
  
  std::cout << "\e[1;31m" << PSNR_R << " " 
            << "\e[1;32m" << PSNR_G << " " 
            << "\e[1;34m" << PSNR_B << " "
            << "\e[0m" << PSNR_global << std::endl;
}




void RGBtoYCrCb(ImageBase& img){
  unsigned char Y = 0;
  unsigned char Cr = 0;
  unsigned char Cb = 0;
  for( size_t x = 0; x < (size_t)img.getHeight(); ++x ){
    for( size_t y = 0; y < (size_t)img.getWidth(); ++y ){
      Y = 0.299f*img[x*3][y*3+0] + 0.587f*img[x*3][y*3+1] + 0.114*img[x*3][y*3+2];
      Cr = 0.5f*(float)img[x*3][y*3+0] - 0.4187f*(float)img[x*3][y*3+1] - 0.0813f*(float)img[x*3][y*3+2] + 128.0f;
      Cb = -0.1687f*img[x*3][y*3+0] - 0.3313f*(float)img[x*3][y*3+1] + 0.5f*(float)img[x*3][y*3+2] + 128.0f;

      img[x*3][y*3+0] = Y;
      img[x*3][y*3+1] = Cr;
      img[x*3][y*3+2] = Cb;
    }
  }
}


void YCrCbtoRGB(ImageBase& img){
  int R = 0;
  int G = 0;
  int B = 0;
  double Y = 0.0;
  double Cb = 0.0;
  double Cr = 0.0;
  for( size_t x = 0; x < (size_t)img.getHeight(); ++x ){
    for( size_t y = 0; y < (size_t)img.getWidth(); ++y ){
      Y = (double)img[x*3][y*3+0]; Cb = (double)img[x*3][y*3+2]; Cr = (double)img[x*3][y*3+1];
      
      R = (int) (Y + 1.40200 * (Cr - 0x80));
      G = (int) (Y - 0.34414 * (Cb - 0x80) - 0.71414 * (Cr - 0x80));
      B = (int) (Y + 1.77200 * (Cb - 0x80));
      
      img[x*3][y*3+0] = std::max(0, std::min(255,R));
      img[x*3][y*3+1] = std::max(0, std::min(255,G));
      img[x*3][y*3+2] = std::max(0, std::min(255,B));
    }
  }
}


void Core(ImageBase& img,bool RGB){
  int** NG = NULL;
  unsigned char** R = NULL; unsigned char** G = NULL; unsigned char**B = NULL; 
  unsigned char** tR = NULL; unsigned char** tG = NULL; unsigned char** tB = NULL;
  
  
  
  //Allocation memoire
  if(RGB){
    ImageBase copie;
    copie.copy(img);
    R = new unsigned char*[img.getHeight()]();
    G = new unsigned char*[img.getHeight()]();
    B = new unsigned char*[img.getHeight()]();
    
    for( size_t x = 0; x < (size_t)img.getHeight(); ++x ){
      R[x] = new unsigned char[ img.getWidth()]();
      G[x] = new unsigned char[ img.getWidth()]();
      B[x] = new unsigned char[ img.getWidth()]();
    }

    //Copie des composantes de l'image en mémoire
    for( size_t x = 0; x < (size_t)img.getHeight(); ++x ){
      for( size_t y = 0; y < (size_t)img.getWidth(); ++y ){
        R[x][y] = copie[x*3][y*3+0];
        G[x][y] = copie[x*3][y*3+1];
        B[x][y] = copie[x*3][y*3+2];
      }
    }
    
    
    //Traitement des composantes
    ImageBase resR = ImageBase( img.getWidth(), img.getHeight(), false );
    
    
    freeMemory(R, img.getHeight());
    freeMemory(G, img.getHeight());
    freeMemory(B, img.getHeight());
  }
  else{
    ImageBase copie;
    copie.copy(img);
    
    NG = new int*[img.getHeight()]();
    for( size_t x = 0; x < (size_t)img.getHeight(); ++x ){
      NG[x] = new int[ img.getWidth()]();
    }
    
    for( size_t x = 0; x < (size_t)img.getHeight(); ++x ){
      for( size_t y = 0; y < (size_t)img.getWidth(); ++y ){
        NG[x][y] = img[x][y];
      }
    }

    //Traitement des composantes
    unsigned int* histoblar = histogramme(NG, (size_t)img.getWidth(), (size_t)img.getHeight());
    histowrite(histoblar, "fu.dat");
    
    for( size_t x = 0; x < (size_t)img.getHeight(); ++x ){
      for( size_t y = 0; y < (size_t)img.getWidth(); ++y ){
        copie[x][y] = NG[x][y];
      }
    }

    //freeMemory(NG, img.getHeight());
  }
}

void diffMap(ImageBase& img){
  ImageBase res;
  res.copy(img);

  for( int x = 0; x < img.getHeight(); ++x ){
    for( int y = 1; y < img.getWidth(); y++ ){
      res[x][y] = img[x][y] - img[x][y-1] + 128; 
    }
  }
  //Core(res,false);
  res.save("res.pgm");
}

/*
int main(int argc, char** argv){
  const char* nomImg = "TP4_Mandrill_1.ppm";
  ImageBase img = ImageBase();
  if( argc > 1 ){ img.load(argv[1]); }
  else{ img.load(nomImg); }
  //Core(img, false);
  //diffMap(img);
  

  
  
  
  return 0;
}
*/

ImageBase distanceCouleur (ImageBase& a, ImageBase& b) {
  const unsigned int width = a.getWidth();
  const unsigned int height = a.getHeight();
  unsigned int distance;

  if ((width == b.getWidth()) && (height == b.getHeight())) {
    ImageBase map (width, height, false);

    for (unsigned int i = 0; i < height; ++i) {
      for (unsigned int j = 0; j < width; ++j) {
        distance = abs(a[i * 3][j * 3] - b[i * 3][j * 3]);
        distance += abs(a[i * 3][j * 3] - b[i * 3][j * 3]);
        distance += abs(a[i * 3][j * 3] - b[i * 3][j * 3]);

        // clamp
        distance = distance > 255 ? 255 : distance;
        map[i][j] = distance;
      }
    }

    map.save("map.pgm");
  } else {
    cerr << "les tailles ne correspondent pas" << width << "*" << height << "  " << b.getWidth() << "*" << b.getHeight() << endl;
  }
}

ImageBase distanceLuminance (ImageBase& a, ImageBase& b) {
  const unsigned int width = a.getWidth();
  const unsigned int height = a.getHeight();
  unsigned int distance;

  if ((width == b.getWidth()) && (height == b.getHeight())) {
    ImageBase map (width, height, false);

    for (unsigned int i = 0; i < height; ++i) {
      for (unsigned int j = 0; j < width; ++j) {
        map[i][j] = abs(a[i][j] - b[i][j]);
      }
    }

    map.save("mapLum.pgm");
  } else {
    cerr << "les tailles ne correspondent pas" << endl;
  }
}

int main(int argc, char **argv) {
	if (argc != 2) {
		printf("Usage: prog <fileName> \n"); 
		return 1;
	}
	
	std::string fileName (argv[1]);
	
  ImageBase originale;
  originale.load(fileName);

  originale.save("originale.ppm");
  /*
  const unsigned char nombreCouleur = 24;
  const unsigned int width = originale.getWidth();
  const unsigned int height = originale.getHeight();

  unsigned int x, y, z;
  double n, gainX, gainY, gainZ, distance;
  for (unsigned int i = 0; i < height; ++i) {
    for (unsigned int j = 0; j < width; ++j) {
      gainX = 0;
      gainY = 0;
      gainZ = 0;
      for (unsigned int u = 0; u < nombreCouleur; ++u) {
        x = charte[u * 3];
        y = charte[(u + 1) * 3];
        z = charte[(u + 2) * 3];

        distance = sqrt((x - originale[i * 3][j * 3]) * (x - originale[i * 3][j * 3]) + (y - originale[i * 3][j * 3 + 1]) * (y - originale[i * 3][j * 3 + 1]) + (z - originale[i * 3][j * 3 + 2]) * (z - originale[i * 3][j * 3 + 2])); 
        //cout << "distance :" << distance << endl;
        if (distance < 200 && distance > 10) {
          //cout << "distance" << distance << endl; 
          gainX += (distance ? x : x / ( 2 * distance * distance));
          gainY += (distance ? y : y / ( 2 *distance * distance));
          gainZ += (distance ? z : z / (2 * distance * distance)); 
        }
      }
      
      //gainX += originale[i * 3][j * 3];
      //gainY += originale[i * 3][j * 3 + 1];
      //gainZ += originale[i * 3][j * 3 + 2];

      n = sqrt(gainX*gainX + gainY*gainY + gainZ*gainZ);
      
      gainX /= n;
      gainY /= n;
      gainZ /= n;

      originale[i * 3][j * 3] = gainX * 255;
      originale[i * 3][j * 3 + 1] = gainY * 255;
      originale[i * 3][j * 3 + 2] = gainZ * 255;
      cout << gainX << " " << gainY << " " << gainZ << endl;
    } 
  }


    originale.save("copy.ppm");
*/

  // Chargement des chartes
  std::string chartPath = "./chart1";
  ImageBase chart1;
  chart1.copy(fillChart(chartPath));
  chart1.save("_chart1.ppm");

  chartPath = "./chart2";
  ImageBase chart2;
  chart2.copy(fillChart(chartPath));
  chart2.save("_chart2.ppm");

  chartPath = "./chartOrg";
  ImageBase chartOrg;
  chartOrg.copy(fillChart(chartPath));


  chartPath = "./postAvgChart1";
  ImageBase postchart1;
  postchart1.copy(fillChart(chartPath));
  postchart1.save("_chart1.ppm");
  
  chartPath = "./postAvgChart2";
  ImageBase postchart2;
  postchart2.copy(fillChart(chartPath));
  postchart2.save("_chart2.ppm");
  

  // On calcule la différence moyenne entre les chartes
  int* diffRes = diffMoy(chartOrg, chart1);

  // On ajoute la différence moyenne à chaque pixel
  appliquerDiff(originale, diffRes);

  // On calcule la différence moyenne entre les chartes
  int* diffRes2 = diffMoy(chartOrg, postchart1);

  // On ajoute la différence moyenne à chaque pixel
  appliquerDiff(originale, diffRes2);

  distanceCouleur(chartOrg, postchart1);

  chartOrg.griser();
  chart1.griser();
  chart2.griser();

  distanceLuminance(chartOrg, postchart1);
 
	return 0;
}
