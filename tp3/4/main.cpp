#include "../../lib/C++/ImageBase.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <fstream>

using namespace std;

int* getHistogramme(ImageBase& im) {
  int* histogramme = new int[256] ();
  for (int i=0; i<255; i++) {
    histogramme[i] = 0;
  }
  // Parcours de la grille 
  int h = im.getHeight(); 
  int w = im.getWidth();

  for (int i = 0; i< h; i++) {
    for (int j = 0 ; j < w; j++ ) {
      histogramme[int(im[i][j])]++;
    }
  }
  
  return histogramme;
}

int main(int argc, char **argv) {

	if (argc != 2) {
	  printf("Usage: ImageIn.pgm \n"); 
		return 1;
	} 

	int num, isColonne;
	ofstream out;

	char nomImageIn[150] = "../../src/";
	strcat (nomImageIn, argv[1]);
	

	int* histogramme;
	int* histoLenna;

	// Chargement d'une image
	ImageBase imIn;
	ImageBase lenna;
	//const char * str = nomImageIn.c_str();
	imIn.load(nomImageIn);
	lenna.load("../../src/lena.pgm");
	lenna.save("lenna.pgm");
	imIn.save("avant.pgm");

	histogramme = getHistogramme(imIn);
	histoLenna = getHistogramme(lenna);
	
	out.open("histo.dat");
	for (int i = 0; i < 256; i++) {
	  out << i << " "<< histogramme[i] << endl;
	}
	out.close();

	out.open("histogrammeLenna.dat");
	for (int i = 0; i < 256; i++) {
	  out << i << " "<< histoLenna[i] << endl;
	}
	out.close();

	double ddp [256];
	double ddpLenna [256];
	for (int i = 0; i < 256; i++) {
	  ddp[i] = ((double)histogramme[i]) / (imIn.getWidth() * imIn.getHeight());
	  ddpLenna[i] = ((double)histoLenna[i]) / (lenna.getWidth() * lenna.getHeight());
	}

	out.open("densite.dat");
	for (int i = 0; i < 256; i++) {
	  out << i << " "<< ddp[i] << endl;
	}
	out.close();


	double F[256];
	double FLenna[256];
	F[0] = ddp[0];
	FLenna[0] = ddpLenna[0];
	for (int i=1; i < 256; i++) {
	  F[i] = F[i-1] + ddp[i];
	  FLenna[i] = FLenna[i-1] + ddpLenna[i];
	}

	out.open("repart.dat");
	for (int i = 0; i < 256; i++) {
	  out << i << " "<< F[i] << endl;
	}
	out.close();

	double T[256];
	for (int i=0; i < 256; i++) {
	  T[i] = 255 * F[i];
	}

	for (int i=0; i < imIn.getHeight(); i++) {
	  for (int j=0; j < imIn.getWidth(); j++) {
	    imIn[i][j] = 255 * F[imIn[i][j]];  
	    lenna[i][j] = 255 * FLenna[lenna[i][j]];  
	  }
	}
	
	histogramme = getHistogramme(imIn);
	out.open("newHisto.dat");
	for (int i = 0; i < 256; i++) {
	  out << i << " "<< histogramme[i] << endl;
	}
	out.close();


	imIn.save("apres.pgm");
	lenna.save("lennaEgalise.pgm");
	
	for (int i=0; i < imIn.getHeight(); i++) {
	  for (int j=0; j < imIn.getWidth(); j++) {
	    double repartition = ((double)lenna[i][j]) / 255;
	    int niveauDeGris = 0;

	    while ( ((double)FLenna[niveauDeGris]) < repartition) {
	      // cout << niveauDeGris << endl;
	      niveauDeGris++;
	    }

	    //cout << repartition << " " << FLenna[0] << " " << FLenna[niveauDeGris] << "<F(" << niveauDeGris << endl;
	    lenna[i][j] = niveauDeGris;
	    
	    repartition = ((double)imIn[i][j]) / 255;
	    niveauDeGris = 0;

	    while ( ((double)FLenna[niveauDeGris]) < repartition) {
	      // cout << niveauDeGris << endl;
	      niveauDeGris++;
	    }


	    imIn[i][j] = niveauDeGris;
	  }
	}

	histogramme = getHistogramme(imIn);
	//	histoLenna = getHistogramme(lenna);
	
	out.open("imageSpe.dat");
	for (int i = 0; i < 256; i++) {
	  out << i << " "<< histogramme[i] << " "<< histoLenna[i] << endl;
	}
	out.close();
	/*
	for (int i = 0; i < 256; i++) {
	  out << i << " "<< histoLenna[i] << endl;
	}
	out.close();
	*/

	imIn.save("compare.pgm");
	lenna.save("lennaRecons.pgm");
	  
	return 0;
}
