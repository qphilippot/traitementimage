#include "../../lib/C++/ImageBase.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <fstream>

using namespace std;

int* getHistogramme(ImageBase& im) {
  int* histogramme = new int[256] ();
  for (int i=0; i<255; i++) {
    histogramme[i] = 0;
  }
  // Parcours de la grille 
  int h = im.getHeight(); 
  int w = im.getWidth();

  for (int i = 0; i< h; i++) {
    for (int j = 0 ; j < w; j++ ) {
      histogramme[int(im[i][j])]++;
    }
  }
  
  return histogramme;
}

int main(int argc, char **argv) {

	if (argc != 2) {
	  printf("Usage: ImageIn.pgm \n"); 
		return 1;
	} 

	int num, isColonne;
	ofstream out;

	char nomImageIn[150] = "../../src/";
	strcat (nomImageIn, argv[1]);
	

	int* histogramme;
	// Chargement d'une image
	ImageBase imIn;
	//const char * str = nomImageIn.c_str();
	imIn.load(nomImageIn);
	imIn.save("avant.pgm");

	histogramme = getHistogramme(imIn);
	
	out.open("histo.dat");
	for (int i = 0; i < 256; i++) {
	  out << i << " "<< histogramme[i] << endl;
	}
	out.close();

	double ddp [256];
	for (int i = 0; i < 256; i++) {
	  ddp[i] = ((double)histogramme[i]) / (imIn.getWidth() * imIn.getHeight());
	}

	out.open("densite.dat");
	for (int i = 0; i < 256; i++) {
	  out << i << " "<< ddp[i] << endl;
	}
	out.close();


	double F[256];
	F[0] = ddp[0];
	for (int i=1; i < 256; i++) {
	  F[i] = F[i-1] + ddp[i];
	}

	out.open("repart.dat");
	for (int i = 0; i < 256; i++) {
	  out << i << " "<< F[i] << endl;
	}
	out.close();

	double T[256];
	for (int i=0; i < 256; i++) {
	  T[i] = 255 * F[i];
	}

	for (int i=0; i < imIn.getHeight(); i++) {
	  for (int j=0; j < imIn.getWidth(); j++) {
	    imIn[i][j] = 255 * F[imIn[i][j]];  
	  }
	}


	histogramme = getHistogramme(imIn);
	out.open("newHisto.dat");
	for (int i = 0; i < 256; i++) {
	  out << i << " "<< histogramme[i] << endl;
	}
	out.close();


	imIn.save("apres.pgm");
	return 0;
}
