#include "../../lib/C++/ImageBase.h"
#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

int main(int argc, char **argv) {

	if (argc != 3) {
		printf("Usage: ImageIn.pgm Seuil1 \n"); 
		return 1;
	} 

	int seuil1;
        sscanf (argv[2],"%d",&seuil1);
 
	char nomImageIn[150] = "../../src/";
	strcat (nomImageIn, argv[1]);

	char nomImageOut[150];
        sscanf (argv[1],"%s",nomImageOut); 
	
	// Chargement d'une image
	ImageBase imIn;
	imIn.load(nomImageIn);

	// Création d'une image aux dimension et de couleur de imIn 
	ImageBase imOut(imIn.getWidth(), imIn.getHeight(), imIn.getColor());
	ImageBase imTmp(imIn.getWidth(), imIn.getHeight(), imIn.getColor());

	// Seuillage de imIn dans imOut
	for (int x = 0; x < imIn.getHeight(); ++x) {
	  for(int y = 0; y < imIn.getWidth(); ++y) {
	    if (imIn[x][y] < seuil1) {
	      imOut[x][y] = 0;
	    } else {
	      imOut[x][y] = 255;
	    }
	  }
	}
	imOut.save("seuil.pgm");
	
	for (int i =0; i < imOut.getWidth();i++) {
	  for (int j=0; j < imOut.getHeight(); j++) {
	    imTmp[i][j] = imOut[i][j];
	  }
	}

	// dilatation :
	bool filtre[3][3];
	filtre[0][0] = false;
	filtre[0][1] = true;
	filtre[0][2] = false;

	filtre[1][0] = true;
	filtre[1][1] = true;
	filtre[1][2] = true;

	filtre[2][0] = false;
	filtre[2][1] = true;
	filtre[2][2] = false;
	
	// topLeft Point
	if (imOut[0][0] == 0) {
	  imTmp[0][0] = 0;
	  imTmp[0][1] = 0;
	  imTmp[1][0] = 0;
	}

	// topRight Point
	if (imOut[imOut.getWidth()-1][0] == 0) {
	  imTmp[imOut.getWidth()-1][0] = 0;
	  imTmp[imOut.getWidth()-2][0] = 0;
	  imTmp[imOut.getWidth()-2][1] = 0;
	}
	// BottomRight Point
	if (imOut[imOut.getWidth()-1][imOut.getHeight()-1] == 0) {
	  imTmp[imOut.getWidth()-1][imOut.getHeight()-1] = 0;
	  imTmp[imOut.getWidth()-2][imOut.getHeight()-1] = 0;
	  imTmp[imOut.getWidth()-1][imOut.getHeight()-2] = 0;
	}

	// BottomLeft Point
	if (imOut[0][imOut.getHeight()-1] == 0) {
	  imTmp[0][imOut.getHeight()-1] = 0;
	  imTmp[1][imOut.getHeight()-1] = 0;
	  imTmp[0][imOut.getHeight()-2] = 0;
	}

	// TopLine
	for (int i=1; i < imOut.getWidth()-1; i++) {
	  if (imOut[i][0] == 0) {
	    imTmp[i][0] = 0;
	    imTmp[i][1] = 0;
	    imTmp[i-1][0] = 0;
	    imTmp[i+1][0] = 0;
	  }
	}

	// BotLine
	for (int i = 1; i < imOut.getWidth()-1; i++) {
	  if (imOut[i][imOut.getHeight()-1] == 0) {
	    imTmp[i][imOut.getHeight()-1] = 0;
	    imTmp[i][imOut.getHeight()-2] = 0;
	    imTmp[i-1][imOut.getHeight()-1] = 0;
	    imTmp[i+1][imOut.getHeight()-1] = 0;
	  }
	}

	// LeftLine
	for (int i = 1; i < imOut.getHeight() - 1; i++) {
	  if (imOut[0][i] == 0) {
	    imTmp[0][i] = 0;
	    imTmp[0][i-1] = 0;
	    imTmp[1][i] = 0;
	    imTmp[0][i+1] = 0;
	  }
	}

	// RightLine
	for (int i = 1; i < imOut.getHeight() - 1; i++) {
	  if (imOut[imOut.getWidth()-1][i] == 0) {
	    imTmp[imOut.getWidth()-1][i] = 0;
	    imTmp[imOut.getWidth()-1][i-1] = 0;
	    imTmp[imOut.getWidth()-2][i] = 0;
	    imTmp[imOut.getWidth()-1][i+1] = 0;
	  }
	}

	// Interior	
	for (int i = 2; i < imOut.getWidth() - 2; i++) {
	  for (int j = 2; j < imOut.getWidth() - 2; j++) {
	    if (imOut[i][j] == 0) {
	    imTmp[i][j] = 0;
	    imTmp[i][j-1] = 0;
	    imTmp[i][j+1] = 0;
	 
	    imTmp[i-1][j] = 0;
	    imTmp[i+1][j] = 0;
	    }
	  }
	}
	  
	imTmp.save("dilatation.pgm");
	 

	// Serialisation de imOut
	//str = nomImageOut.c_str(); 
	imOut.save(nomImageOut);

	return 0;

}
