#include "../../../lib/C++/ImageBase.h"
#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

int main(int argc, char **argv) {
  // problème : tour de boucle : itérateur 2 ou 1 ??
	if (argc != 3) {
		printf("Usage: ImageIn.pgm Seuil1 \n"); 
		return 1;
	} 

	int seuil1;
        sscanf (argv[2],"%d",&seuil1);
 
	char nomImageIn[150] = "../../../src/";
	strcat (nomImageIn, argv[1]);

	char nomImageOut[150];
        sscanf (argv[1],"%s",nomImageOut); 
	
	// Chargement d'une image
	ImageBase imIn;
	imIn.load(nomImageIn);

	// Création d'une image aux dimension et de couleur de imIn 
	ImageBase imOut(imIn.getWidth(), imIn.getHeight(), imIn.getColor());
	ImageBase imTmp(imIn.getWidth(), imIn.getHeight(), imIn.getColor());
	ImageBase imFin(imIn.getWidth(), imIn.getHeight(), imIn.getColor());


	// Seuillage :
	for (int x = 0; x < imIn.getHeight(); ++x) {
	  for(int y = 0; y < imIn.getWidth(); ++y) {
	    if (imIn[x][y] < seuil1) {
	      imOut[x][y] = 0;
	    } else {
	      imOut[x][y] = 255;
	    }
	  }
	}
	imOut.save("seuil.pgm");
	
	// Fermeture :
	for(int x=0; x<imOut.getWidth(); x++) {
	  for (int y=0; y<imOut.getHeight(); y++) {
	    imTmp[x][y] = imOut[x][y];
	  }
	}

	// dilatation :
	// topLeft Point
	if (imOut[0][0] == 0) {
	  imTmp[0][0] = 0;
	  imTmp[0][1] = 0;
	  imTmp[1][0] = 0;
	}

	// topRight Point
	if (imOut[imOut.getWidth()-1][0] == 0) {
	  imTmp[imOut.getWidth()-1][0] = 0;
	  imTmp[imOut.getWidth()-2][0] = 0;
	  imTmp[imOut.getWidth()-2][1] = 0;
	}
	// BottomRight Point
	if (imOut[imOut.getWidth()-1][imOut.getHeight()-1] == 0) {
	  imTmp[imOut.getWidth()-1][imOut.getHeight()-1] = 0;
	  imTmp[imOut.getWidth()-2][imOut.getHeight()-1] = 0;
	  imTmp[imOut.getWidth()-1][imOut.getHeight()-2] = 0;
	}

	// BottomLeft Point
	if (imOut[0][imOut.getHeight()-1] == 0) {
	  imTmp[0][imOut.getHeight()-1] = 0;
	  imTmp[1][imOut.getHeight()-1] = 0;
	  imTmp[0][imOut.getHeight()-2] = 0;
	}

	// TopLine
	for (int i=1; i < imOut.getWidth()-1; i++) {
	  if (imOut[i][0] == 0) {
	    imTmp[i][0] = 0;
	    imTmp[i][1] = 0;
	    imTmp[i-1][0] = 0;
	    imTmp[i+1][0] = 0;
	  }
	}

	// BotLine
	for (int i = 1; i < imOut.getWidth()-1; i++) {
	  if (imOut[i][imOut.getHeight()-1] == 0) {
	    imTmp[i][imOut.getHeight()-1] = 0;
	    imTmp[i][imOut.getHeight()-2] = 0;
	    imTmp[i-1][imOut.getHeight()-1] = 0;
	    imTmp[i+1][imOut.getHeight()-1] = 0;
	  }
	}

	// LeftLine
	for (int i = 1; i < imOut.getHeight() - 1; i++) {
	  if (imOut[0][i] == 0) {
	    imTmp[0][i] = 0;
	    imTmp[0][i-1] = 0;
	    imTmp[1][i] = 0;
	    imTmp[0][i+1] = 0;
	  }
	}

	// RightLine
	for (int i = 1; i < imOut.getHeight() - 1; i++) {
	  if (imOut[imOut.getWidth()-1][i] == 0) {
	    imTmp[imOut.getWidth()-1][i] = 0;
	    imTmp[imOut.getWidth()-1][i-1] = 0;
	    imTmp[imOut.getWidth()-2][i] = 0;
	    imTmp[imOut.getWidth()-1][i+1] = 0;
	  }
	}

	// Interior	
	for (int i = 1; i < imOut.getWidth() - 1; i++) {
	  for (int j = 1; j < imOut.getWidth() - 1; j++) {
	    if (imOut[i][j] == 0) {
	    imTmp[i][j] = 0;
	    imTmp[i][j-1] = 0;
	    imTmp[i][j+1] = 0;
	 
	    imTmp[i-1][j] = 0;
	    imTmp[i+1][j] = 0;
	    }
	  }
	}
	  
	imTmp.save("fermeture-dilatation.pgm");
	
	for (int i =0; i < imOut.getWidth();i++) {
	  for (int j=0; j < imOut.getHeight(); j++) {
	    imFin[i][j] = imTmp[i][j];
	  }
	}

// erosion :
	// topLeft Point
	if (imTmp[0][0] == 0) {
	  imFin[0][0] = 255;
	  imFin[0][1] = 255;
	  imFin[1][0] = 255;
	}

	// topRight Point
	if (imTmp[imTmp.getWidth()-1][0] == 255) {
	  imFin[imFin.getWidth()-1][0] = 255;
	  imFin[imFin.getWidth()-2][0] = 255;
	  imFin[imFin.getWidth()-2][1] = 255;
	}
	// BottomRight Point
	if (imTmp[imTmp.getWidth()-1][imFin.getHeight()-1] == 255) {
	  imFin[imFin.getWidth()-1][imFin.getHeight()-1] = 255;
	  imFin[imFin.getWidth()-2][imFin.getHeight()-1] = 255;
	  imFin[imFin.getWidth()-1][imFin.getHeight()-2] = 255;
	}

	// BottomLeft Point
	if (imTmp[0][imFin.getHeight()-1] == 255) {
	  imFin[0][imFin.getHeight()-1] = 255;
	  imFin[1][imFin.getHeight()-1] = 255;
	  imFin[0][imFin.getHeight()-2] = 255;
	}

	// TopLine
	for (int i=1; i < imFin.getWidth()-1; i++) {
	  if (imTmp[i][0] == 255) {
	    imFin[i][0] = 255;
	    imFin[i][1] = 255;
	    imFin[i-1][0] = 255;
	    imFin[i+1][0] = 255;
	  }
	}

	// BotLine
	for (int i = 1; i < imFin.getWidth()-1; i++) {
	  if (imTmp[i][imFin.getHeight()-1] == 255) {
	    imFin[i][imFin.getHeight()-1] = 255;
	    imFin[i][imFin.getHeight()-2] = 255;
	    imFin[i-1][imFin.getHeight()-1] = 255;
	    imFin[i+1][imFin.getHeight()-1] = 255;
	  }
	}

	// LeftLine
	for (int i = 1; i < imFin.getHeight() - 1; i++) {
	  if (imTmp[0][i] == 255) {
	    imFin[0][i] = 255;
	    imFin[0][i-1] = 255;
	    imFin[1][i] = 0;
	    imFin[0][i+1] = 255;
	  }
	}

	// RightLine
	for (int i = 1; i < imFin.getHeight() - 1; i++) {
	  if (imTmp[imFin.getWidth()-1][i] == 255) {
	    imFin[imFin.getWidth()-1][i] = 255;
	    imFin[imFin.getWidth()-1][i-1] = 255;
	    imFin[imFin.getWidth()-2][i] = 255;
	    imFin[imFin.getWidth()-1][i+1] = 255;
	  }
	}
	
	// Interior	
	for (int i = 1; i < imFin.getWidth() - 1; i++) {
	  for (int j = 1; j < imFin.getWidth() - 1; j++) {
	    if (imTmp[i][j] == 255) {
	      imFin[i][j] = 255;
	      imFin[i][j-1] = 255;
	      imFin[i][j+1] = 255;
	      
	      imFin[i-1][j] = 255;
	      imFin[i+1][j] = 255;
	    }
	  }
	}
	
	imFin.save("fermeture-erosion.pgm");
	
	// Seuillage de imIn dans imOut
	for (int x = 0; x < imIn.getHeight(); ++x) {
	  for(int y = 0; y < imIn.getWidth(); ++y) {
	    imTmp[x][y] = imFin[x][y];
	  }
	}


	// erosion :
	// topLeft Point
	if (imFin[0][0] == 255) {
	  imTmp[0][0] = 255;
	  imTmp[0][1] = 255;
	  imTmp[1][0] = 255;
	}

	// topRight Point
	if (imFin[imOut.getWidth()-1][0] == 255) {
	  imTmp[imOut.getWidth()-1][0] = 255;
	  imTmp[imOut.getWidth()-2][0] = 255;
	  imTmp[imOut.getWidth()-2][1] = 255;
	}
	// BottomRight Point
	if (imFin[imOut.getWidth()-1][imOut.getHeight()-1] == 255) {
	  imTmp[imOut.getWidth()-1][imOut.getHeight()-1] = 255;
	  imTmp[imOut.getWidth()-2][imOut.getHeight()-1] = 255;
	  imTmp[imOut.getWidth()-1][imOut.getHeight()-2] = 255;
	}

	// BottomLeft Point
	if (imFin[0][imOut.getHeight()-1] == 255) {
	  imTmp[0][imOut.getHeight()-1] = 255;
	  imTmp[1][imOut.getHeight()-1] = 255;
	  imTmp[0][imOut.getHeight()-2] = 255;
	}

	// TopLine
	for (int i=1; i < imOut.getWidth()-1; i++) {
	  if (imFin[i][0] == 255) {
	    imTmp[i][0] = 255;
	    imTmp[i][1] = 255;
	    imTmp[i-1][0] = 255;
	    imTmp[i+1][0] = 255;
	  }
	}

	// BotLine
	for (int i = 1; i < imOut.getWidth()-1; i++) {
	  if (imFin[i][imOut.getHeight()-1] == 255) {
	    imTmp[i][imOut.getHeight()-1] = 255;
	    imTmp[i][imOut.getHeight()-2] = 255;
	    imTmp[i-1][imOut.getHeight()-1] = 255;
	    imTmp[i+1][imOut.getHeight()-1] = 255;
	  }
	}

	// LeftLine
	for (int i = 1; i < imOut.getHeight() - 1; i++) {
	  if (imFin[0][i] == 255) {
	    imTmp[0][i] = 255;
	    imTmp[0][i-1] = 255;
	    imTmp[1][i] = 255;
	    imTmp[0][i+1] = 255;
	  }
	}

	// RightLine
	for (int i = 1; i < imOut.getHeight() - 1; i++) {
	  if (imFin[imOut.getWidth()-1][i] == 255) {
	    imTmp[imOut.getWidth()-1][i] = 255;
	    imTmp[imOut.getWidth()-1][i-1] = 255;
	    imTmp[imOut.getWidth()-2][i] = 255;
	    imTmp[imOut.getWidth()-1][i+1] = 255;
	  }
	}

	// Interior	
	for (int i = 1; i < imOut.getWidth() - 1; i++) {
	  for (int j = 1; j < imOut.getWidth() - 1; j++) {
	    if (imFin[i][j] == 255) {
	    imTmp[i][j] = 255;
	    imTmp[i][j-1] = 255;
	    imTmp[i][j+1] = 255;
	 
	    imTmp[i-1][j] = 255;
	    imTmp[i+1][j] = 255;
	    }
	  }
	}
	  
	imTmp.save("ouverture-erosion.pgm");

	for (int i =0; i < imOut.getWidth();i++) {
	  for (int j=0; j < imOut.getHeight(); j++) {
	    imFin[i][j] = imTmp[i][j];
	  }
	}

	// dilatation :
	// topLeft Point
	if (imTmp[0][0] == 0) {
	  imFin[0][0] = 0;
	  imFin[0][1] = 0;
	  imFin[1][0] = 0;
	}

	// topRight Point
	if (imTmp[imTmp.getWidth()-1][0] == 0) {
	  imFin[imFin.getWidth()-1][0] = 0;
	  imFin[imFin.getWidth()-2][0] = 0;
	  imFin[imFin.getWidth()-2][1] = 0;
	}
	// BottomRight Point
	if (imTmp[imTmp.getWidth()-1][imFin.getHeight()-1] == 0) {
	  imFin[imFin.getWidth()-1][imFin.getHeight()-1] = 0;
	  imFin[imFin.getWidth()-2][imFin.getHeight()-1] = 0;
	  imFin[imFin.getWidth()-1][imFin.getHeight()-2] = 0;
	}

	// BottomLeft Point
	if (imTmp[0][imFin.getHeight()-1] == 0) {
	  imFin[0][imFin.getHeight()-1] = 0;
	  imFin[1][imFin.getHeight()-1] = 0;
	  imFin[0][imFin.getHeight()-2] = 0;
	}

	// TopLine
	for (int i=1; i < imFin.getWidth()-1; i++) {
	  if (imTmp[i][0] == 0) {
	    imFin[i][0] = 0;
	    imFin[i][1] = 0;
	    imFin[i-1][0] = 0;
	    imFin[i+1][0] = 0;
	  }
	}

	// BotLine
	for (int i = 1; i < imFin.getWidth()-1; i++) {
	  if (imTmp[i][imFin.getHeight()-1] == 0) {
	    imFin[i][imFin.getHeight()-1] = 0;
	    imFin[i][imFin.getHeight()-2] = 0;
	    imFin[i-1][imFin.getHeight()-1] = 0;
	    imFin[i+1][imFin.getHeight()-1] = 0;
	  }
	}

	// LeftLine
	for (int i = 1; i < imFin.getHeight() - 1; i++) {
	  if (imTmp[0][i] == 0) {
	    imFin[0][i] = 0;
	    imFin[0][i-1] = 0;
	    imFin[1][i] = 0;
	    imFin[0][i+1] = 0;
	  }
	}

	// RightLine
	for (int i = 1; i < imFin.getHeight() - 1; i++) {
	  if (imTmp[imFin.getWidth()-1][i] == 0) {
	    imFin[imFin.getWidth()-1][i] = 0;
	    imFin[imFin.getWidth()-1][i-1] = 0;
	    imFin[imFin.getWidth()-2][i] = 0;
	    imFin[imFin.getWidth()-1][i+1] = 0;
	  }
	}

	// Interior	
	for (int i = 1; i < imFin.getWidth() - 1; i++) {
	  for (int j = 1; j < imFin.getWidth() - 1; j++) {
	    if (imTmp[i][j] == 0) {
	    imFin[i][j] = 0;
	    imFin[i][j-1] = 0;
	    imFin[i][j+1] = 0;
	 
	    imFin[i-1][j] = 0;
	    imFin[i+1][j] = 0;
	    }
	  }
	}
	  
	imFin.save("fermeture-ouverture.pgm");
	 
	return 0;

}
