#include "../../../../../lib/C++/ImageBase.h"
#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

void seuiller1 (ImageBase* imIn, int seuil) {
  for (int x = 0; x < imIn->getHeight(); ++x) {
    for(int y = 0; y < imIn->getWidth(); ++y) {
      if ((*imIn)[x][y] < seuil) {
	(*imIn)[x][y] = 0;
      } else {
     	(*imIn)[x][y] = 255;
      }
    }
  }
}

void copy (ImageBase* imIn, ImageBase* imOut) {
  for (int i =0; i < imIn->getWidth();i++) {
    for (int j=0; j < imIn->getHeight(); j++) {
      (*imOut)[i][j] = (*imIn)[i][j];
    }
  } 
}

void eroder(ImageBase* imIn) {
  ImageBase imOut(imIn->getWidth(), imIn->getHeight(), imIn->getColor());
  copy(imIn, &imOut);
  
  // topLeft Point
  if ( (*imIn) [0][0] == 255) {
    imOut[0][0] = 255;
    imOut[0][1] = 255;
    imOut[1][0] = 255;
  }
  
  // topRight Point
  if ( (*imIn) [imOut.getWidth()-1][0] == 255) {
    imOut[imOut.getWidth()-1][0] = 255;
    imOut[imOut.getWidth()-2][0] = 255;
    imOut[imOut.getWidth()-2][1] = 255;
  }
  // BottomRight Point
  if ( (*imIn) [imOut.getWidth()-1][imOut.getHeight()-1] == 255) {
    imOut[imOut.getWidth()-1][imOut.getHeight()-1] = 255;
    imOut[imOut.getWidth()-2][imOut.getHeight()-1] = 255;
    imOut[imOut.getWidth()-1][imOut.getHeight()-2] = 255;
  }
  
  // BottomLeft Point
  if ( (*imIn)[0][imOut.getHeight()-1] == 255) {
    imOut[0][imOut.getHeight()-1] = 255;
    imOut[1][imOut.getHeight()-1] = 255;
    imOut[0][imOut.getHeight()-2] = 255;
  }
  
  // TopLine
  for (int i=1; i < imOut.getWidth()-1; i++) {
    if ( (*imIn)[i][0] == 255) {
      imOut[i][0] = 255;
      imOut[i][1] = 255;
      imOut[i-1][0] = 255;
      imOut[i+1][0] = 255;
    }
  }
  
  // BotLine
  for (int i = 1; i < imOut.getWidth()-1; i++) {
    if ( (*imIn) [i][imOut.getHeight()-1] == 255) {
      imOut[i][imOut.getHeight()-1] = 255;
      imOut[i][imOut.getHeight()-2] = 255;
      imOut[i-1][imOut.getHeight()-1] = 255;
      imOut[i+1][imOut.getHeight()-1] = 255;
    }
  }
  
  // LeftLine
  for (int i = 1; i < imOut.getHeight() - 1; i++) {
    if ( (*imIn) [0][i] == 255) {
      imOut[0][i] = 255;
      imOut[0][i-1] = 255;
      imOut[1][i] = 255;
      imOut[0][i+1] = 255;
    }
  }
  
  // RightLine
  for (int i = 1; i < imOut.getHeight() - 1; i++) {
    if ( (*imIn) [imOut.getWidth()-1][i] == 255) {
      imOut[imOut.getWidth()-1][i] = 255;
      imOut[imOut.getWidth()-1][i-1] = 255;
      imOut[imOut.getWidth()-2][i] = 255;
      imOut[imOut.getWidth()-1][i+1] = 255;
    }
  }
  
  // Interior	
  for (int i = 1; i < imOut.getWidth() - 1; i++) {
    for (int j = 1; j < imOut.getWidth() - 1; j++) {
      if ( (*imIn) [i][j] == 255) {
	imOut[i][j] = 255;
	imOut[i][j-1] = 255;
	imOut[i][j+1] = 255;
	
	imOut[i-1][j] = 255;
	imOut[i+1][j] = 255;
      }
    }
  }
  copy(&imOut, imIn);
}

void dilater(ImageBase* imIn) {
  ImageBase imOut(imIn->getWidth(), imIn->getHeight(), imIn->getColor());
  copy(imIn, &imOut);
  
  // topLeft Point
  if ( (*imIn) [0][0] == 0) {
    imOut[0][0] = 0;
    imOut[0][1] = 0;
    imOut[1][0] = 0;
  }
  
  // topRight Point
  if ( (*imIn) [imOut.getWidth()-1][0] == 0) {
    imOut[imOut.getWidth()-1][0] = 0;
    imOut[imOut.getWidth()-2][0] = 0;
    imOut[imOut.getWidth()-2][1] = 0;
  }
  // BottomRight Point
  if ( (*imIn) [imOut.getWidth()-1][imOut.getHeight()-1] == 0) {
    imOut[imOut.getWidth()-1][imOut.getHeight()-1] = 0;
    imOut[imOut.getWidth()-2][imOut.getHeight()-1] = 0;
    imOut[imOut.getWidth()-1][imOut.getHeight()-2] = 0;
  }
  
  // BottomLeft Point
  if ( (*imIn)[0][imOut.getHeight()-1] == 0) {
    imOut[0][imOut.getHeight()-1] = 0;
    imOut[1][imOut.getHeight()-1] = 0;
    imOut[0][imOut.getHeight()-2] = 0;
  }
  
  // TopLine
  for (int i=1; i < imOut.getWidth()-1; i++) {
    if ( (*imIn)[i][0] == 0) {
      imOut[i][0] = 0;
      imOut[i][1] = 0;
      imOut[i-1][0] = 0;
      imOut[i+1][0] = 0;
    }
  }
  
  // BotLine
  for (int i = 1; i < imOut.getWidth()-1; i++) {
    if ( (*imIn) [i][imOut.getHeight()-1] == 0) {
      imOut[i][imOut.getHeight()-1] = 0;
      imOut[i][imOut.getHeight()-2] = 0;
      imOut[i-1][imOut.getHeight()-1] = 0;
      imOut[i+1][imOut.getHeight()-1] = 0;
    }
  }
  
  // LeftLine
  for (int i = 1; i < imOut.getHeight() - 1; i++) {
    if ( (*imIn) [0][i] == 0) {
      imOut[0][i] = 0;
      imOut[0][i-1] = 0;
      imOut[1][i] = 0;
      imOut[0][i+1] = 0;
    }
  }
  
  // RightLine
  for (int i = 1; i < imOut.getHeight() - 1; i++) {
    if ( (*imIn) [imOut.getWidth()-1][i] == 0) {
      imOut[imOut.getWidth()-1][i] = 0;
      imOut[imOut.getWidth()-1][i-1] = 0;
      imOut[imOut.getWidth()-2][i] = 0;
      imOut[imOut.getWidth()-1][i+1] = 0;
    }
  }
  
  // Interior	
  for (int i = 1; i < imOut.getWidth() - 1; i++) {
    for (int j = 1; j < imOut.getWidth() - 1; j++) {
      if ( (*imIn) [i][j] == 0) {
	imOut[i][j] = 0;
	imOut[i][j-1] = 0;
	imOut[i][j+1] = 0;
	
	imOut[i-1][j] = 0;
	imOut[i+1][j] = 0;
      }
    }
  }
  copy(&imOut, imIn);
}

int main(int argc, char **argv) {
  
  if (argc != 3) {
    printf("Usage: ImageIn.pgm Seuil1 \n"); 
    return 1;
  } 
  
  int seuil;
  sscanf (argv[2],"%d",&seuil);
  
  char nomImageIn[150] = "../../../../../src/";
  strcat (nomImageIn, argv[1]);
  
  char nomImageOut[150];
  sscanf (argv[1],"%s",nomImageOut); 
  
  // Chargement d'une image
  ImageBase imIn;
  imIn.load(nomImageIn);
 
  seuiller1(&imIn, seuil);

  for (int i =0; i < 3;i++) {
      dilater(&imIn);  
  } 
  
      imIn.save("3d.pgm");
  for (int i =0; i < 6;i++) {
      eroder(&imIn);  
  } 
  
      imIn.save("3d6e.pgm");
  for (int i =0; i < 3;i++) {
      dilater(&imIn);  
  } 

      imIn.save("3d6e3d.pgm");
  
  return 0;

}
