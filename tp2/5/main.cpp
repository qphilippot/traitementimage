#include "../../lib/C++/ImageBase.h"
#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

void seuiller1 (ImageBase* imIn, int seuil) {
  for (int x = 0; x < imIn->getHeight(); ++x) {
    for(int y = 0; y < imIn->getWidth(); ++y) {
      if ((*imIn)[x][y] < seuil) {
	(*imIn)[x][y] = 0;
      } else {
     	(*imIn)[x][y] = 255;
      }
    }
  }
}

void copy (ImageBase* imIn, ImageBase* imOut) {
  for (int i =0; i < imIn->getWidth();i++) {
    for (int j=0; j < imIn->getHeight(); j++) {
      (*imOut)[i][j] = (*imIn)[i][j];
    }
  } 
}

void eroder(ImageBase* imIn) {
  ImageBase imOut(imIn->getWidth(), imIn->getHeight(), imIn->getColor());
  copy(imIn, &imOut);
  
  if(imIn->getColor()) {
    // En couleur
  } else {

    // topLeft Point
    if ( (*imIn) [0][0] > imOut[0][0]) {
      imOut[0][0] += ((*imIn)[0][0] - imOut[0][0]) / 2;
    } 
    if ( (*imIn) [0][0] > imOut[1][0]) {
      imOut[1][0] += ((*imIn)[0][0] - imOut[1][0]) / 2;
    } 
    if ( (*imIn) [0][0] > imOut[0][1]) {
      imOut[0][1] += ((*imIn)[0][0] - imOut[0][1]) / 2;
    }
    // topRight Point
    if ( (*imIn) [imOut.getWidth()-1][0] > imOut [imOut.getWidth()-1][0]) {
      imOut[imOut.getWidth()-1][0] += ((*imIn)[imOut.getWidth()-1][0] - imOut[imOut.getWidth()-1][0]) / 2;
    } 
    if ( (*imIn) [imOut.getWidth()-1][0] > imOut [imOut.getWidth()-2][0]) {
      imOut[imOut.getWidth()-2][0] += ((*imIn)[imOut.getWidth()-1][0] - imOut[imOut.getWidth()-2][0]) / 2;
    }
    if ( (*imIn) [imOut.getWidth()-1][0] > imOut [imOut.getWidth()-1][1]) {
      imOut[imOut.getWidth()-1][1] += ((*imIn)[imOut.getWidth()-1][0] - imOut[imOut.getWidth()-1][1]) / 2;
    }
    // bottomRight Point
    if ( (*imIn) [imOut.getWidth()-1][imOut.getHeight()-1] > imOut [imOut.getWidth()-1][imOut.getHeight()-1]) {
      imOut[imOut.getWidth()-1][imOut.getHeight()-1] += ((*imIn)[imOut.getWidth()-1][imOut.getHeight()-1] - imOut[imOut.getWidth()-1][imOut.getHeight()-1]) / 2;
    }
    if ( (*imIn) [imOut.getWidth()-1][imOut.getHeight()-1] >  imOut[imOut.getWidth()-2][imOut.getHeight()-1]) {
      imOut[imOut.getWidth()-2][imOut.getHeight()-1] += ( (*imIn)[imOut.getWidth()-1][imOut.getHeight()-1] - imOut[imOut.getWidth()-2][imOut.getHeight()-1]) / 2;
    }
    if ( (*imIn) [imOut.getWidth()-1][imOut.getHeight()-1] > imOut[imOut.getWidth()-1][imOut.getHeight()-2]) {
      imOut[imOut.getWidth()-1][imOut.getHeight()-2] += ((*imIn) [imOut.getWidth()-1][imOut.getHeight()-1] - imOut[imOut.getWidth()-1][imOut.getHeight()-2]) / 2;
    }
    // bottomLeft Point 
    if ( (*imIn) [0][imOut.getHeight()-1] > imOut [0][imOut.getHeight()-1] ) {
      imOut [0][imOut.getHeight()-1] += ((*imIn) [0][imOut.getHeight()-1] - imOut [0][imOut.getHeight()-1]) / 2;
    }
    if ( (*imIn) [0][imOut.getHeight()-1] >  imOut [1][imOut.getHeight()-1]) {
      imOut[1][imOut.getHeight()-1] += ( (*imIn) [0][imOut.getHeight()-1] - imOut[1][imOut.getHeight()-1]) / 2;
    }
    if ( (*imIn) [0][imOut.getHeight()-1] >  imOut[0][imOut.getHeight()-2]) {
      imOut[0][imOut.getHeight()-2] += ((*imIn) [0][imOut.getHeight()-1] - imOut[0][imOut.getHeight()-2]) / 2;
    }
    // topLine
    for (int i = 1; i < imOut.getWidth()-2; i++) {
      if ( (*imIn) [i][0] > imOut [i][0] ) {
	imOut [i][0] += ((*imIn) [i][0] - imOut [i][0]) / 2;
      }
      if ( (*imIn) [i][0] >  imOut [i][1]) {
	imOut[i][1] += ( (*imIn) [i][0] - imOut[i][1]) / 2;
      }
      if ( (*imIn) [i][0] >  imOut [i-1][0]) {
	imOut[i-1][0] += ((*imIn) [i][0] - imOut[i-1][0]) / 2;
      }
      if ( (*imIn) [i][0] >  imOut [i+1][0]) {
	imOut[i+1][0] += ((*imIn) [i][0] - imOut[i+1][0]) / 2;
      }
    }
    // botLine
    for (int i = 1; i < imOut.getWidth()-2; i++) {
      if ( (*imIn) [i][imOut.getHeight()-1] > imOut [i][imOut.getHeight()-1] ) {
	imOut [i][imOut.getHeight()-1] += ((*imIn) [i][imOut.getHeight()-1] - imOut [i][imOut.getHeight()-1]) / 2;
      }
      if ( (*imIn) [i][imOut.getHeight()-1] >  imOut [i][imOut.getHeight()-2]) {
	imOut[i][imOut.getHeight()-2] += ( (*imIn) [i][imOut.getHeight()-1] - imOut[i][imOut.getHeight()-2]) / 2;
      }
      if ( (*imIn) [i][imOut.getHeight()-1] >  imOut [i-1][imOut.getHeight()-1]) {
	imOut[i-1][imOut.getHeight()-1] += ((*imIn) [i][imOut.getHeight()-1] - imOut[i-1][imOut.getHeight()-1]) / 2;
      }
      if ( (*imIn) [i][imOut.getHeight()-1] >  imOut [i+1][imOut.getHeight()-1]) {
	imOut[i+1][imOut.getHeight()-1] += ((*imIn) [i][imOut.getHeight()-1] - imOut[i+1][imOut.getHeight()-1]) / 2;
      }
    }
    // leftLine
    for (int i = 1; i < imOut.getHeight()-2; i++) {
      if ( (*imIn) [0][i] > imOut [0][i] ) {
	imOut [0][i] += ((*imIn) [0][i] - imOut [0][i]) / 2;
      }
      if ( (*imIn) [0][i] >  imOut [0][i-1]) {
	imOut[0][i-1] += ( (*imIn) [0][i] - imOut[0][i-1]) / 2;
      }
      if ( (*imIn) [0][i] >  imOut [1][i]) {
	imOut[1][i] += ((*imIn) [0][i] - imOut[1][i]) / 2;
      }
      if ( (*imIn) [i][0] >  imOut [0][i+1]) {
	imOut[0][i+1] += ((*imIn) [0][i] - imOut[0][i+1]) / 2;
      }
    }
    // rightLine
    for (int i = 1; i < imOut.getHeight()-2; i++) {
      if ( (*imIn) [imOut.getWidth()-1][i] > imOut [imOut.getWidth()-1][i] ) {
	imOut [imOut.getWidth()-1][i] += ((*imIn) [imOut.getWidth()-1][i] - imOut [imOut.getWidth()-1][i]) / 2;
      }
      if ( (*imIn) [imOut.getWidth()-1][i] >  imOut [imOut.getWidth()-1][i-1]) {
	imOut[imOut.getWidth()-1][i-1] += ( (*imIn) [imOut.getWidth()-1][i] - imOut[imOut.getWidth()-1][i-1]) / 2;
      }
      if ( (*imIn) [imOut.getWidth()-2][i] >  imOut [imOut.getWidth()-2][i]) {
	imOut[imOut.getWidth()-2][i] += ((*imIn) [imOut.getWidth()-1][i] - imOut[imOut.getWidth()-2][i]) / 2;
      }
      if ( (*imIn) [i][imOut.getWidth()-1] >  imOut [imOut.getWidth()-1][i+1]) {
	imOut[imOut.getWidth()-1][i+1] += ((*imIn) [imOut.getWidth()-1][i] - imOut[imOut.getWidth()-1][i+1]) / 2;
      }
    }
    // Interior	
    for (int i = 1; i < imOut.getWidth() - 1; i++) {
      for (int j = 1; j < imOut.getWidth() - 1; j++) {
	if ( (*imIn) [i][j] < imOut[i][j]) {
	  imOut [i][j] += ((*imIn) [i][j] - imOut [i][j]) / 2;
	}
	if ( (*imIn) [i][j] >  imOut [i][j-1]) {
	  imOut[i][j-1] += ( (*imIn) [i][j] - imOut[i][j-1]) / 2;
	}
	if ( (*imIn) [i][j] >  imOut [i][j+1]) {
	  imOut[i][j+1] += ((*imIn) [i][j] - imOut[i][j+1]) / 2;
	}
	if ( (*imIn) [i][j] >  imOut [i+1][j]) {
	  imOut[i+1][j] += ((*imIn) [i][j] - imOut[i+1][j]) / 2;
	}
	if ( (*imIn) [i][j] >  imOut [i-1][j]) {
	  imOut[i-1][j] += ((*imIn) [i][j] - imOut[i-1][j]) / 2;
	}
      }
    }
  }
  copy(&imOut, imIn);
}

void dilater(ImageBase* imIn) {
  ImageBase imOut(imIn->getWidth(), imIn->getHeight(), imIn->getColor());
  copy(imIn, &imOut);
 
  if(imIn->getColor()) {
    // En couleur
  } else {
    // En noir et blanc
    // On éclaircie les voisins pour qu'ils nous ressemblent :
    // topLeft Point
    if ( (*imIn) [0][0] < imOut[0][0]) {
      imOut[0][0] -= (imOut[0][0] - (*imIn)[0][0]) / 2;
    } 
    if ( (*imIn) [0][0] < imOut[1][0]) {
      imOut[1][0] -= (imOut[1][0] - (*imIn)[0][0]) / 2;
    } 
    if ( (*imIn) [0][0] < imOut[0][1]) {
      imOut[0][1] -= (imOut[0][1] - (*imIn)[0][0]) / 2;
    } 
 
    
    // topRight Point
    if ( (*imIn) [imOut.getWidth()-1][0] < imOut [imOut.getWidth()-1][0]) {
      imOut[imOut.getWidth()-1][0] -= (imOut[imOut.getWidth()-1][0] - (*imIn) [imOut.getWidth()-1][0]) / 2;
    } 
    if ( (*imIn) [imOut.getWidth()-1][0] < imOut [imOut.getWidth()-2][0]) {
      imOut[imOut.getWidth()-2][0] -= (imOut[imOut.getWidth()-2][0] - (*imIn) [imOut.getWidth()-1][0]) / 2;
    }
    if ( (*imIn) [imOut.getWidth()-1][0] < imOut [imOut.getWidth()-1][1]) {
      imOut[imOut.getWidth()-1][1] -= (imOut[imOut.getWidth()-1][1] - (*imIn) [imOut.getWidth()-1][0]) / 2;
    }
    
    // bottomRight Point
    if ( (*imIn) [imOut.getWidth()-1][imOut.getHeight()-1] < imOut [imOut.getWidth()-1][imOut.getHeight()-1]) {
      imOut[imOut.getWidth()-1][imOut.getHeight()-1] -= (imOut[imOut.getWidth()-1][imOut.getHeight()-1] - (*imIn) [imOut.getWidth()-1][imOut.getHeight()-1]) / 2;
    }
    if ( (*imIn) [imOut.getWidth()-1][imOut.getHeight()-1] <  imOut[imOut.getWidth()-2][imOut.getHeight()-1]) {
      imOut[imOut.getWidth()-2][imOut.getHeight()-1] -= (imOut[imOut.getWidth()-2][imOut.getHeight()-1] - (*imIn) [imOut.getWidth()-1][imOut.getHeight()-1]) / 2;
    }
    if ( (*imIn) [imOut.getWidth()-1][imOut.getHeight()-1] < imOut[imOut.getWidth()-1][imOut.getHeight()-2]) {
      imOut[imOut.getWidth()-1][imOut.getHeight()-2] -= (imOut[imOut.getWidth()-1][imOut.getHeight()-2] - (*imIn) [imOut.getWidth()-1][imOut.getHeight()-1]) / 2;
    }

    // bottomLeft Point 
    if ( (*imIn) [0][imOut.getHeight()-1] < imOut [0][imOut.getHeight()-1] ) {
      imOut [0][imOut.getHeight()-1] -= (imOut [0][imOut.getHeight()-1] - (*imIn) [0][imOut.getHeight()-1]) / 2;
    }
    if ( (*imIn) [0][imOut.getHeight()-1] <  imOut [1][imOut.getHeight()-1]) {
      imOut[1][imOut.getHeight()-1] -= (imOut[1][imOut.getHeight()-1] - (*imIn) [0][imOut.getHeight()-1]) / 2;
    }
    if ( (*imIn) [0][imOut.getHeight()-1] <  imOut[0][imOut.getHeight()-2]) {
      imOut[0][imOut.getHeight()-2] -= (imOut[0][imOut.getHeight()-2] - (*imIn) [0][imOut.getHeight()-1]) / 2;
    }


    // topLine
    for (int i = 1; i < imOut.getWidth()-2; i++) {
      if ( (*imIn) [i][0] < imOut [i][0] ) {
	imOut [i][0] -= (imOut [i][0] - (*imIn) [i][0]) / 2;
      }
      if ( (*imIn) [i][0] <  imOut [i][1]) {
	imOut[i][0] -= (imOut[i][1] - (*imIn) [i][0]) / 2;
      }
      if ( (*imIn) [i][0] <  imOut [i-1][0]) {
	imOut[i-1][0] -= (imOut[i-1][0] - (*imIn) [i][0]) / 2;
      }
      if ( (*imIn) [i][0] <  imOut [i+1][0]) {
	imOut[i+1][0] -= (imOut[i+1][0] - (*imIn) [i][0]) / 2;
      }
    }


    // botLine
    for (int i = 1; i < imOut.getWidth()-2; i++) {
      if ( (*imIn) [i][imOut.getHeight()-1] < imOut [i][imOut.getHeight()-1] ) {
	imOut [i][imOut.getHeight()-1] -= (imOut [i][imOut.getHeight()-1] - (*imIn) [i][imOut.getHeight()-1]) / 2;
      }
      if ( (*imIn) [i][imOut.getHeight()-1] <  imOut [i][imOut.getHeight()-2]) {
	imOut[i][imOut.getHeight()-2] -= (imOut[i][imOut.getHeight()-2] - (*imIn) [i][imOut.getHeight()-1]) / 2;
      }
      if ( (*imIn) [i][imOut.getHeight()-1] <  imOut [i-1][imOut.getHeight()-1]) {
	imOut[i-1][imOut.getHeight()-1] -= (imOut[i-1][imOut.getHeight()-1] - (*imIn) [i][imOut.getHeight()-1]) / 2;
      }
      if ( (*imIn) [i][imOut.getHeight()-1] <  imOut [i+1][imOut.getHeight()-1]) {
	imOut[i+1][imOut.getHeight()-1] -= (imOut[i+1][imOut.getHeight()-1] - (*imIn) [i][imOut.getHeight()-1]) / 2;
      }
    }

	
    // leftLine
    for (int i = 1; i < imOut.getHeight()-2; i++) {
      if ( (*imIn) [0][i] < imOut [0][i] ) {
	imOut [0][i] -= (imOut [0][i] - (*imIn) [0][i]) / 2;
      }
      if ( (*imIn) [0][i] <  imOut [0][i-1]) {
	imOut[0][i-1] -= (imOut[0][i-1] - (*imIn) [0][i]) / 2;
      }
      if ( (*imIn) [0][i] <  imOut [1][i]) {
	imOut[1][i] -= (imOut[1][i] - (*imIn) [0][i]) / 2;
      }
      if ( (*imIn) [i][0] <  imOut [0][i+1]) {
	imOut[0][i+1] -= (imOut[0][i+1] - (*imIn) [0][i]) / 2;
      }
    }

    // rightLine
    for (int i = 1; i < imOut.getHeight()-2; i++) {
      if ( (*imIn) [imOut.getWidth()-1][i] < imOut [imOut.getWidth()-1][i] ) {
	imOut [imOut.getWidth()-1][i] -= (imOut [imOut.getWidth()-1][i] - (*imIn) [imOut.getWidth()-1][i]) / 2;
      }
      if ( (*imIn) [imOut.getWidth()-1][i] <  imOut [imOut.getWidth()-1][i-1]) {
	imOut[imOut.getWidth()-1][i-1] -= (imOut[imOut.getWidth()-1][i-1] - (*imIn) [imOut.getWidth()-1][i]) / 2;
      }
      if ( (*imIn) [imOut.getWidth()-2][i] <  imOut [imOut.getWidth()-2][i]) {
	imOut[imOut.getWidth()-2][i] -= (imOut[imOut.getWidth()-2][i] - (*imIn) [imOut.getWidth()-1][i]) / 2;
      }
      if ( (*imIn) [i][imOut.getWidth()-1] <  imOut [imOut.getWidth()-1][i+1]) {
	imOut[imOut.getWidth()-1][i+1] -= (imOut[imOut.getWidth()-1][i+1] - (*imIn) [imOut.getWidth()-1][i]) / 2;
      }
    }
    
    // Interior	
    for (int i = 1; i < imOut.getWidth() - 1; i++) {
      for (int j = 1; j < imOut.getWidth() - 1; j++) {
	if ( (*imIn) [i][j] < imOut [i][j] ) {
	  imOut [i][j] -= (imOut [i][j] - (*imIn) [i][j]) / 2;
	}
	if ( (*imIn) [i][j] <  imOut [i][j-1]) {
	  imOut[i][j-1] -= (imOut[i][j-1] - (*imIn) [i][j]) / 2;
	}
	if ( (*imIn) [i][0] <  imOut [i][j+1]) {
	  imOut[i][j+1] -= (imOut[i][j+1] - (*imIn) [i][j]) / 2;
	}
	if ( (*imIn) [i][0] <  imOut [i+1][j]) {
	  imOut[i+1][j] -= (imOut[i+1][j] - (*imIn) [i][j]) / 2;
	}
	if ( (*imIn) [i][0] <  imOut [i-1][j]) {
	  imOut[i-1][j] -= (imOut[i-1][j] - (*imIn) [i][j]) / 2;
	}
      }
    } 
  }
  copy(&imOut, imIn);
}

int main(int argc, char **argv) {
  
  if (argc != 3) {
    printf("Usage: ImageIn.pgm Seuil1 \n"); 
    return 1;
  } 
  
  int seuil;
  sscanf (argv[2],"%d",&seuil);
  
  char nomImageIn[150] = "../../src/";
  strcat (nomImageIn, argv[1]);
  
  char nomImageOut[150];
  sscanf (argv[1],"%s",nomImageOut); 
  
  // Chargement d'une image
  ImageBase imIn;
  imIn.load(nomImageIn);
 
  /* 
  ImageBase imDil (imSeuil.getWidth(), imSeuil.getHeight(), imSeuil.getColor());
  seuiller1(&imSeuil, seuil);
  copy(&imSeuil, &imDil);

 

  imSeuil.save("seuil.pgm");
  

  dilater(&imDil);   
  imDil.save("dilatation.pgm");

  for (int x = 0; x < imSeuil.getWidth(); x++) {
    for (int y = 0; y < imSeuil.getHeight(); y++) {
      if ( (imSeuil[x][y] - imDil[x][y]) == 0 ) {
	imSeuil[x][y] = 255;
      } else {
	imSeuil[x][y] = 0;
      }
    }
  }
  */
  cout << "avant" << endl;
  for (int i =0; i < 1; i++) {    
    eroder(&imIn);
    dilater(&imIn);

    dilater(&imIn);
    eroder(&imIn);
  }
  cout << "apres" << endl;
  imIn.save("ouverture-fermeture.pgm");
  
  return 0;

}
