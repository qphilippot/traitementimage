#include "../lib/C++/ImageBase.h"
#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <sstream>
#include <string>
#include <cmath>

using namespace std;

vector<string> split(string str, char delimiter) {
  vector<string> internal;
  stringstream ss(str); // Turn the string into a stream.
  string tok;
  
  while(getline(ss, tok, delimiter)) {
    internal.push_back(tok);
  }
  
  return internal;
}

void seuiller (ImageBase& img, unsigned char seuil) {
  for (int x = 0; x < img.getHeight(); ++x) {
    for(int y = 0; y < img.getWidth(); ++y) {
      img[x][y] = (img[x][y] < seuil) ? 0 : 255;
    }
  }
} 

unsigned char winCalculerSeuilMinMax (ImageBase& img, int iBegin, int jBegin, int iEnd, int jEnd) {
    if (iBegin < 0) {
    iBegin = 0;
  }

  if (iEnd >= img.getHeight()) {
    iEnd = img.getHeight() -1;
  }

  if (jBegin < 0) {
    jBegin = 0;
  }

  if (jEnd >= img.getHeight()) {
    jEnd = img.getWidth() -1;
  }

  unsigned char max = 0;
  unsigned char min = 255;
  
  for (int x = iBegin; x < iEnd; ++x) {
    for(int y = jBegin; y < jEnd; ++y) {

      if (img[x][y] < min) {
        min = img[x][y];
      }
     
      if (img[x][y] > max) {
        max = img[x][y];
      }  
    }
  }
  return (max - min) / 2;  
}

unsigned char calculerSeuilMinMax (ImageBase& img) {
  unsigned char max = 0;
  unsigned char min = 255;
  
  for (int x = 0; x < img.getHeight(); ++x) {
    for(int y = 0; y < img.getWidth(); ++y) {

      if (img[x][y] < min) {
        min = img[x][y];
      }
     
      if (img[x][y] > max) {
        max = img[x][y];
      }  
    }
  }
  return (max - min) / 2;  
}

// Prend en entrée une image en ndg 
void binariserMinMax (ImageBase& img) {
  unsigned char seuil = calculerSeuilMinMax(img);
  seuiller(img, seuil);
  img.save("imageBinMinMax.pgm");
}

ImageBase extractCanal(ImageBase& img, const unsigned char canal) {
  const unsigned int width = img.getWidth();
  const unsigned int height = img.getHeight();

  ImageBase extracted (width, height, false);
  for (int i = 0; i < height; ++i) {
    for (int j = 0; j < width; ++j) {
      extracted[i][j] = img[i * 3][j * 3 + canal];
    }
  }
  return extracted;
}

// Prend en entrée une image avec 3 canaux
void binariserVoteMaj (ImageBase& img) {
  unsigned int moduleSize = 5; // sous-entendu 20*20
  unsigned int votingSize = 3; // on regarde que 15*15
  unsigned int nbVotingPixel = votingSize * votingSize;

  const unsigned char seuil = calculerSeuilMinMax(img);
  
  const unsigned int w = img.getWidth();
  const unsigned int h = img.getHeight();

  // Itération sur les modules
  for (int i = 0; i < h; i += moduleSize) {

    
    for (int j = 0; j < w; j += moduleSize) {
      unsigned int lastI = i + moduleSize;
      unsigned int lastJ = j + moduleSize;
      // Si le bloc ne sort pas de l'image
      if ((lastI < h) && (lastJ < w)) {
        // On effectue le vote
        unsigned int lastX = i + votingSize;
        unsigned int lastY = j + votingSize;
        unsigned int nbBlack = 0;
        for (int x = i; x < lastX; ++x) {
          for (int y = j; y < lastY; ++y) {
            if (img[x][y] < seuil) {
              ++nbBlack;
            }
          }
        }
        // résolution 
        unsigned char color = (nbBlack >= (votingSize / 2)) ? 0 : 255;

        // binarisation du module
        for (int px = i; px < lastI; ++px) {
          for (int py = j; py < lastJ; ++py) {
            img[px][py] = color;
          } 
        }
      }     
    }
  }

  img.save("imageVoteMaj.pgm");
}

 double winVariance (ImageBase& img, int iBegin, int jBegin, int iEnd, int jEnd, double average) {
  if (iBegin < 0) {
    iBegin = 0;
  }

  if (iEnd >= img.getHeight()) {
    iEnd = img.getHeight() -1;
  }

  if (jBegin < 0) {
    jBegin = 0;
  }

  if (jEnd >= img.getHeight()) {
    jEnd = img.getWidth() -1;
  }

  double sum = 0;
  for (int i=iBegin; i < iEnd; ++i) {
    for (int j=jBegin; j < jEnd; ++j) {
      sum += (img[i][j] - average) * (img[i][j] - average);
    }
  }

  return sum / (((iEnd - iBegin) * (jEnd - jBegin)));
}

double winEcartType (ImageBase& img, int iBegin, int jBegin, int iEnd, int jEnd, double average) {
  return sqrt(winVariance(img, iBegin, jBegin, iEnd, jEnd, average));
}

double winAverage (ImageBase& img, int iBegin, int jBegin, int iEnd, int jEnd) {
  if (iBegin < 0) {
    iBegin = 0;
  }

  if (iEnd >= img.getHeight()) {
    iEnd = img.getHeight() -1;
  }

  if (jBegin < 0) {
    jBegin = 0;
  }

  if (jEnd >= img.getHeight()) {
    jEnd = img.getWidth() -1;
  }


  double sum = 0;
  for (int i=iBegin; i < iEnd; ++i) {
    for (int j=jBegin; j < jEnd; ++j) {
      //cout << i << j << endl;
      sum += img[i][j];
    }
  }

  return sum / (((iEnd - iBegin) * (jEnd - jBegin)));
}

void binariserBiModal(ImageBase& img) {
  const unsigned char modeGlobal = calculerSeuilMinMax(img);

  // Dimensions de l'image
  const unsigned int iWidth = img.getWidth();
  const unsigned int iHeight = img.getHeight();
  
  // demi - dimensions de la fenetre (ex : 5*5 -> 10*10)
  const unsigned int wWidth = 10;
  const unsigned int wHeight = 10;

  for (int i=0; i < iHeight; ++i) {
    for (int j=0; j < iWidth; ++j) {
      //cout << "_" << i << j << endl;
      const unsigned char modeLocal = winCalculerSeuilMinMax(img,  i - wHeight, j - wWidth, i + wHeight, j + wWidth);
      const unsigned char seuil = (modeGlobal + modeLocal) / 2;
      img[i][j] = (img[i][j] >= seuil) ? 255 : 0;
    }
  }
  img.save("bimod.pgm");
}

void niblack(ImageBase& img) {
  // Dimensions de l'image
  const unsigned int iWidth = img.getWidth();
  const unsigned int iHeight = img.getHeight();
  
  // demi - dimensions de la fenetre (ex : 5*5 -> 10*10)
  const unsigned int wWidth = 10;
  const unsigned int wHeight = 10;

  for (int i=0; i < iHeight; ++i) {
    for (int j=0; j < iWidth; ++j) {
      //cout << "_" << i << j << endl;
      double average = winAverage(img, i - wHeight, j - wWidth, i + wHeight, j + wWidth);
      double ecartType = winEcartType(img, i - wHeight, j - wWidth, i + wHeight, j + wWidth, average);
      const unsigned char seuil = ecartType + 0.2 * average;
      img[i][j] = (img[i][j] >= seuil) ? 255 : 0;
    }
  }
  img.save("niblack");
}

ImageBase rawToPGM (const unsigned char* data, int nbColonne, int nbLigne) {
  ImageBase img (nbColonne, nbLigne, false);
  for (int i = 0; i < nbLigne; ++i) {
    for (int j=0; j < nbColonne; ++j) {
      img[i][j] = data[(i * nbColonne) + j];
    }
  }
  img.save("image.pgm");
  return img;
}

ImageBase rawToPPM (const unsigned char* data, int nbColonne, int nbLigne) {
  ImageBase img (nbColonne, nbLigne, true);
  for (int i = 0; i < nbLigne; ++i) {
    for (int j=0; j < nbColonne; ++j) {
      img[i *3][j * 3] = data[i * 3 * nbColonne + j * 3];
      img[i *3][j * 3 + 1] = data[i * 3 * nbColonne + j * 3 + 1];
      img[i *3][j * 3 + 2] = data[i * 3 * nbColonne + j * 3 + 2];
    }
  }
  img.save("image.ppm");
  return img;
}

ImageBase copyImage (const unsigned char* data, int nbColonne, int nbLigne, int nbCanal) {
  if (nbCanal == 1) {
    return rawToPGM(data, nbColonne, nbLigne);
  } else {
    return rawToPPM(data, nbColonne, nbLigne);
  }
}

ImageBase griser (ImageBase& color) {
  const unsigned int w = color.getWidth();
  const unsigned int h = color.getHeight();
  
  ImageBase ndg (w, h, false);
  for (int i = 0; i < h; ++i) {
    for (int j = 0; j < w; ++j) {
      double r = color[i*3][j*3 + 0];
      double g = color[i*3][j*3 + 1];
      double b = color[i*3][j*3 + 2];

      // Calcul de la luminance
      ndg[i][j] = int(0.299 * r + 0.587 * g + 0.114 * b); 
    }
  }

  return ndg;
}
 
void question2 (ImageBase& img) {
  if (img.getColor()) {
    img.copy(griser(img));
    img.save("imgNdg.pgm");
  }

  binariserMinMax(img);
}

void question4 (ImageBase& img) {
  if (img.getColor()) {
    img.copy(griser(img));
    img.save("imgNdg.pgm");
  }
  binariserVoteMaj(img);
} 

void question6 (ImageBase& img) {
  if (img.getColor()) {
    img.copy(griser(img));
    img.save("imgNdg.pgm");
  }
  niblack(img);
} 


void question7 (ImageBase& img) {
  if (img.getColor()) {
    img.copy(griser(img));
    img.save("imgNdg.pgm");
  }
  binariserBiModal(img);
} 

int main(int argc, char **argv) {
	if (argc != 2) {
		printf("Usage: prog <fileName.raw> \n"); 
		return 1;
	}
	
	char* fileName = argv[1];
	
	vector<string> data = split (string(fileName),'.');
	
	// data[0] is the image name
	const unsigned int nbColonne = atoi(data[1].c_str());
	const unsigned int nbLigne = atoi(data[2].c_str());
	const unsigned int nbCanal = atoi(data[3].c_str());
	const unsigned int bufferSize = nbColonne * nbLigne * nbCanal;

	unsigned char* buffer = new unsigned char [bufferSize];
	FILE* f = fopen(fileName, "rb");
	fread(buffer, 1, bufferSize, f);
	fclose(f);

	ImageBase img = copyImage(buffer, nbColonne, nbLigne, nbCanal);
  
  // Début des questions du tp :
  //question2(img);
  //question4(img);
  //question6(img);
  question7(img);

  delete[] buffer;
	
	return 0;

}
