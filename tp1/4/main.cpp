#include "../../lib/C++/ImageBase.h"
#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

int main(int argc, char **argv) {

	if (argc != 2) {
	  printf("Usage: ImageIn.pgm \n"); 
		return 1;
	} 

	int num, isColonne;
	int* occurence  = new int[256]();

	char nomImageIn[150] = "../../src/";
	strcat (nomImageIn, argv[1]); 

       
	// Chargement d'une image
	ImageBase imIn;
	//const char * str = nomImageIn.c_str();
	imIn.load(nomImageIn);


	// Parcours de la grille 
	int h = imIn.getHeight(); 
	int w = imIn.getWidth();
	for (int i = 0; i< w; i++) {
	  for (int j = 0 ; j < h; j++ ) {
	    occurence[(int)imIn[i][j]]++;
	  }
	}
	
	for (int i = 0; i < 256; i++) {
	  cout << i << " "<< occurence[i] << endl;
	}
	
	return 0;
}
