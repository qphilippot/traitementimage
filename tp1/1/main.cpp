#include "../../lib/C++/ImageBase.h"
#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

int main(int argc, char **argv) {

	if (argc != 4) {
		printf("Usage: ImageIn.pgm ImageOut.pgm Seuil \n"); 
		return 1;
	} 
	int seuil;
	char nomImageIn[150] = "../../src/";
	char nomImageOut[150];
	strcat (nomImageIn, argv[1]);
        sscanf (argv[2],"%s",nomImageOut); 
        sscanf (argv[3],"%d",&seuil); 
	
	cout << nomImageIn << endl;
	// Chargement d'une image
	ImageBase imIn;
	//const char * str = nomImageIn.c_str();
	imIn.load(nomImageIn);

	// Création d'une image aux dimension et de couleur de imIn 
	ImageBase imOut(imIn.getWidth(), imIn.getHeight(), imIn.getColor());

	// Seuillage de imIn dans imOut
	for (int x = 0; x < imIn.getHeight(); ++x) {
		for(int y = 0; y < imIn.getWidth(); ++y) {
			if (imIn[x][y] < seuil) 
				imOut[x][y] = 0;
			else imOut[x][y] = 255;
		}
	}
		
	// Serialisation de imOut
	//str = nomImageOut.c_str(); 
	imOut.save(nomImageOut);

	return 0;
}
