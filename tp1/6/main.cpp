#include "../../lib/C++/ImageBase.h"
#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

int main(int argc, char **argv) {

	if (argc != 6) {
		printf("Usage: ImageIn.pgm ImageOut.pgm seuilR seuilV seuilB \n"); 
		return 1;
	} 

	int seuil1, seuil2, seuil3;
	char nomImageIn[150] = "../../src/";
	char nomImageOut[150];
	strcat (nomImageIn, argv[1]);
        sscanf (argv[2],"%s",nomImageOut); 
        sscanf (argv[3],"%d",&seuil1); 
        sscanf (argv[4],"%d",&seuil2); 
        sscanf (argv[5],"%d",&seuil3); 
	
	cout << nomImageIn << endl;
	// Chargement d'une image
	ImageBase imIn;
	//const char * str = nomImageIn.c_str();
	imIn.load(nomImageIn);

	// Création d'une image aux dimension et de couleur de imIn 
	ImageBase imOut(imIn.getWidth(), imIn.getHeight(), imIn.getColor());
	
	// Seuillage de imIn dans imOut
	for (int x = 0; x < imIn.getHeight(); ++x) {
	  for(int y = 0; y < imIn.getWidth(); ++y) {
	    if (imIn[x*3][y*3 +0] < seuil1) {
	      // Rouge
	      imOut[x*3][y*3+0] = 0;
	    } else {
	      imOut[x*3][y*3+0] = 255;
	    }
	    // Vert
	    if (imIn[x*3][y*3 +0] < seuil2) {
	      imOut[x*3][y*3+1] = 0;
	    } else {
	      imOut[x*3][y*3+1] = 255;
	    }
	    // Bleu
	    if (imIn[x*3][y*3 +0] < seuil3) {
	      imOut[x*3][y*3+2] = 0;
	    } else {
	      imOut[x*3][y*3+2] = 255;
	    }
	  }
	}
	// Serialisation de imOut
	  //str = nomImageOut.c_str(); 
	imOut.save(nomImageOut);

	return 0;
}
