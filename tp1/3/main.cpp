#include "../../lib/C++/ImageBase.h"
#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

int main(int argc, char **argv) {

	if (argc != 4) {
		printf("Usage: ImageIn.pgm isColonne numero \n"); 
		return 1;
	} 

	int num, isColonne;
	char nomImageIn[150] = "../../src/";
	strcat (nomImageIn, argv[1]); 
        sscanf (argv[2],"%d",&isColonne); 
        sscanf (argv[3],"%d",&num); 
        
	// Chargement d'une image
	ImageBase imIn;
	//const char * str = nomImageIn.c_str();
	imIn.load(nomImageIn);

	if (isColonne) {
	  size_t size = imIn.getHeight();
	  for (int i = 0; i < size; i++) {
	    cout << i << " "<< (int)imIn[num][i] << endl;
	  }
	} else {
	  size_t size = imIn.getHeight();
	  for (int i = 0; i < size; i++) {
	    cout << i << " " << (int)imIn[i][num] << endl;
	  }
	}

	return 0;
}
