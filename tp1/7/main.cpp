#include "../../lib/C++/ImageBase.h"
#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

int main(int argc, char **argv) {

	if (argc != 2) {
	  printf("Usage: ImageIn.pgm \n"); 
		return 1;
	} 

	int* occurence  = new int[256]();

	char nomImageIn[150] = "../../src/";
	strcat (nomImageIn, argv[1]); 

       
	// Chargement d'une image
	ImageBase imIn;
	//const char * str = nomImageIn.c_str();
	imIn.load(nomImageIn);

	ImageBase imOut(imIn.getWidth(), imIn.getHeight(), imIn.getColor());


	// Parcours de la grille : Comptage des occurences
	int h = imIn.getHeight(); 
	int w = imIn.getWidth();
	for (int i = 0; i< w; i++) {
	  for (int j = 0 ; j < h; j++ ) {
	    occurence[(int)imIn[i][j]]++;
	  }
	}
	
	
	/*
	// Recherche de l'occurence Max
	// Seuillage au max
	int indexOccurenceMax = 0;
	int occurenceMax = 0;
	for (int i = 0; i < 256; i++) {
	if (occurence[i] > occurenceMax) {
	occurenceMax = occurence[i];
	indexOccurenceMax = i;
	}
	}
	
	
	// Seuillage par rapport à l'occurence Max :
	for (int i = 0; i< w; i++) {
	for (int j = 0 ; j < h; j++ ) {
	if (imIn[i][j] < indexOccurenceMax) {
	imOut[i][j] = 0;
	} else {
	imOut[i][j] = 255;
	}
	}
	}
	*/

	// seuillage moyen
	int indexOccurenceMax = 0;
	int occurenceMax = 0;
	double moyenne = 0;
	double totalWeight = 0;
	for (int i = 0; i < 256; i++) {
	  moyenne += i * occurence[i];
	  totalWeight += occurence[i];
	}

	moyenne /= totalWeight;

	


	// Seuillage par rapport à l'occurence Max :
        for (int i = 0; i< w; i++) {
	  for (int j = 0 ; j < h; j++ ) {
	    if (imIn[i][j] < moyenne) {
	      imOut[i][j] = 0;
	    } else {
	      imOut[i][j] = 255;
	    }
	  }
	}


	imOut.save(argv[1]);
	
	return 0;
}
