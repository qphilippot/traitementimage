#include "../lib/C++/ImageBase.h"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>

/**
 * 1) Changement d'espace couleur
 * 2) Transformée en ondelettes en choisissant le nombre N de décompositions N [1;6] -- Ajouter 128 pour stocker l'image
 * 3) Quantification Q = 1 coefficient par sous bandes -- Retirer 128 pour quantifier
 * 4) Faire varier N et/ou Q afin de tracer une courbe débit/distorsion bpp/PSNR
 * Utiliser de préférence une image carrée ppm en entrée 
 */

using namespace std;

struct QuantInfos{
	int Approx;
	int detailsVert;
	int detailsHrz;
	int HF;

	QuantInfos():Approx(1),detailsVert(1),detailsHrz(1),HF(1){}
};

struct NoyauQuant{
	QuantInfos infos[3];

	
};

template<typename T>
unsigned char clamp(T& val){
	return std::max(0, std::min(255, val) );
}

void freeMemory(unsigned char** composante, size_t height){
	for( size_t i = 0; i < height; ++i ){ delete[] composante[i]; }
	delete[] composante;
}


void PSNR(ImageBase& org, ImageBase& res){
	size_t N = org.getHeight() * org.getWidth();
	float EQMR = 0.0f;
	float EQMG = 0.0f;
	float EQMB = 0.0f;
	float EQM_global = 0.0f;
	
	for( size_t x = 0; x < (size_t)org.getHeight(); ++x ){
		for( size_t y = 0; y < (size_t)org.getWidth(); ++y ){
			EQMR += pow( org[x*3][y*3+0] - res[x*3][y*3+0], 2);
			EQMG += pow( org[x*3][y*3+1] - res[x*3][y*3+1], 2);
			EQMB += pow( org[x*3][y*3+2] - res[x*3][y*3+2], 2);
		}
	}
	EQM_global = EQMR + EQMG + EQMB;
	EQM_global /= 3*N;
	EQMR /= N;
	EQMG /= N;
	EQMB /= N;
	
	
	float PSNR_R = 10 * std::log( (255.0f * 255.0f) / EQMR );
	float PSNR_G = 10 * std::log( (255.0f * 255.0f) / EQMG );
	float PSNR_B = 10 * std::log( (255.0f * 255.0f) / EQMB );
	float PSNR_global = 10 * std::log( (255.0f * 255.0f) / EQM_global );
	
	std::cout << "\e[1;31m" << PSNR_R << " " 
	          << "\e[1;32m" << PSNR_G << " " 
	          << "\e[1;34m" << PSNR_B << " "
	          << "\e[0m" << PSNR_global << std::endl;
}




void RGBtoYCrCb(ImageBase& img){
	unsigned char Y = 0;
	unsigned char Cr = 0;
	unsigned char Cb = 0;
	for( size_t x = 0; x < (size_t)img.getHeight(); ++x ){
		for( size_t y = 0; y < (size_t)img.getWidth(); ++y ){
			Y = 0.299f*img[x*3][y*3+0] + 0.587f*img[x*3][y*3+1] + 0.114*img[x*3][y*3+2];
			Cr = 0.5f*(float)img[x*3][y*3+0] - 0.4187f*(float)img[x*3][y*3+1] - 0.0813f*(float)img[x*3][y*3+2] + 128.0f;
			Cb = -0.1687f*img[x*3][y*3+0] - 0.3313f*(float)img[x*3][y*3+1] + 0.5f*(float)img[x*3][y*3+2] + 128.0f;

			img[x*3][y*3+0] = Y;
			img[x*3][y*3+1] = Cr;
			img[x*3][y*3+2] = Cb;
		}
	}
}


void YCrCbtoRGB(ImageBase& img){
	int R = 0;
	int G = 0;
	int B = 0;
	double Y = 0.0;
	double Cb = 0.0;
	double Cr = 0.0;
	for( size_t x = 0; x < (size_t)img.getHeight(); ++x ){
		for( size_t y = 0; y < (size_t)img.getWidth(); ++y ){
			Y = (double)img[x*3][y*3+0]; Cb = (double)img[x*3][y*3+2]; Cr = (double)img[x*3][y*3+1];
			
			R = (int) (Y + 1.40200 * (Cr - 0x80));
			G = (int) (Y - 0.34414 * (Cb - 0x80) - 0.71414 * (Cr - 0x80));
			B = (int) (Y + 1.77200 * (Cb - 0x80));
			
			img[x*3][y*3+0] = std::max(0, std::min(255,R));
			img[x*3][y*3+1] = std::max(0, std::min(255,G));
			img[x*3][y*3+2] = std::max(0, std::min(255,B));
		}
	}
}

void Ondelette(ImageBase& ref, int width, int height){
	int newWidth = width / 2;
	int newHeight = height / 2;

	ImageBase Approx(newWidth, newHeight, true);
	ImageBase detailsVert(newWidth, newHeight, true);
	ImageBase detailsHrz(newWidth, newHeight, true);
	ImageBase HF(newWidth, newHeight, true);

	int X;
	int Y;
	int K;
	int L;

	
	size_t itX = 0;
	size_t itY = 0;
	//Iteration par blocs de 4
	for( size_t x = 0; x < (size_t)height; x+=2 ){
		for( size_t y = 0; y < (size_t)width; y+=2 ){
			for( int i = 0; i < 3; ++i ){
				X = (ref[x*3][y*3+i] + ref[x*3][(y+1)*3+i]) / 2;
				Y = (ref[(x+1)*3][y*3+i] + ref[(x+1)*3][(y+1)*3+i]) / 2;
				K = ref[x*3][y*3+i] - ref[x*3][(y+1)*3+i];
				L = ref[(x+1)*3][y*3+i] - ref[(x+1)*3][(y+1)*3+i];

				Approx[itX*3][itY*3+i]      = (unsigned char)((X + Y) / 2);
				detailsVert[itX*3][itY*3+i] = (unsigned char)((K + L) / 2 + 128);
				detailsHrz[itX*3][itY*3+i]  = (unsigned char)(X - Y + 128);
				HF[itX*3][itY*3+i]          = (unsigned char)(K - L + 128);
			}
			++itY;
		}
		itY = 0;
		++itX;
	}
	
	//Sauvegarde de chaque composante
	for( int x = 0; x < newHeight; ++x ){
		for( int y = 0; y < newWidth; ++y ){
			for( int i = 0; i < 3; ++i ){
				ref[x*3][y*3+i]                        = Approx[x*3][y*3+i];
				ref[(x+newHeight)*3][y*3+i]            = detailsHrz[x*3][y*3+i];
				ref[(x+newHeight)*3][(y+newWidth)*3+i] = HF[x*3][y*3+i];
				ref[x*3][(y+newWidth)*3+i]             = detailsVert[x*3][y*3+i];
			}
		}
	}
}

void Reconstruction(ImageBase& ref, int width, int height){
	int newWidth = width * 2;
	int newHeight = height * 2;

	ImageBase rec(newWidth, newHeight, true);
	int X; int Y;
	int K; int L;
	
	int A; int B;
	int C; int D;

	int Approx; int dH;
	int dV;     int HF;

	for( size_t x = 0; x < (size_t)height; ++x ){
		for( size_t y = 0; y < (size_t)width; ++y ){
			for( int i = 0; i < 3; ++i ){
				Approx = ref[x*3][y*3+i];
				dH = ref[(x+height)*3][y*3+i]; dV -= 128;
				HF = ref[(x+height)*3][(y+width)*3+i]; HF -= 128;
				dV = ref[x*3][(y+width)*3+i]; dH -= 128;
				
				Y = Approx - dH/2;
				L = dV - HF/2;
				X = dH + Y;
				K = HF + L;

				B = X - K/2;
				D = Y - L/2;
				A = K + B;
				C = L + D;

				A = std::max(0, std::min(255,A));
				B = std::max(0, std::min(255,B));
				C = std::max(0, std::min(255,C));
				D = std::max(0, std::min(255,D));
				
				rec[x*2*3][y*2*3+i]         = (unsigned char)A;
				rec[x*2*3][(y*2+1)*3+i]     = (unsigned char)B;
				rec[(x*2+1)*3][y*2*3+i]     = (unsigned char)C;
				rec[(x*2+1)*3][(y*2+1)*3+i] = (unsigned char)D;	

			//	cout << A << " " << B << " " << C << " " << D << endl;
			
			}
		}
	}
	rec.save("test.ppm");

	
	for( int x = 0; x < newHeight; ++x ){
		for( int y = 0; y < newWidth; ++y ){
			for( int i = 0; i < 3; ++i ){
				ref[x*3][y*3+i] = rec[x*3][y*3+i];
			}
		}
	}
}




int main(int argc, char** argv){
	const char* nomImg = "pepper.ppm";
	ImageBase img = ImageBase();
	if( argc > 1 ){ img.load(argv[1]); }
	else{ img.load(nomImg); }
	//RGBtoYCrCb(img);

	int nbDecomp = 2;
	int newWidth = img.getWidth();
	int newHeight = img.getHeight();
	for( int i = 0; i < nbDecomp; ++i ){
		Ondelette(img, newWidth, newHeight);
		newWidth /= 2;
		newHeight /= 2;
		std::cout << newWidth << " " << newHeight << std::endl;
	}
	img.save("inter.ppm");
	for( int i = 0; i < nbDecomp; ++i ){
		Reconstruction(img, newWidth, newHeight);
		newWidth *= 2;
		newHeight *= 2;
		std::cout << newWidth << " " << newHeight << std::endl;
		}
	//YCrCbtoRGB(img);
	img.save("res.ppm");
	return 0;
}
