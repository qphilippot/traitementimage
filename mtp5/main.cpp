#include "../lib/C++/ImageBase.h"
#include "../lib/C++/huffman.h"

#include <iostream>
#include <cstdlib>
#include <vector>
#include <sstream>
#include <string>
#include <cmath>
#include <fstream>


using namespace std;

void PSNR(ImageBase& org, ImageBase& res){
	size_t N = org.getHeight() * org.getWidth();
	float EQMR = 0.0f;
	float EQMG = 0.0f;
	float EQMB = 0.0f;
	float EQM_global = 0.0f;
	
	for( size_t x = 0; x < (size_t)org.getHeight(); ++x ){
		for( size_t y = 0; y < (size_t)org.getWidth(); ++y ){
			EQMR += pow( org[x*3][y*3+0] - res[x*3][y*3+0], 2);
			EQMG += pow( org[x*3][y*3+1] - res[x*3][y*3+1], 2);
			EQMB += pow( org[x*3][y*3+2] - res[x*3][y*3+2], 2);
		}
	}
	EQM_global = EQMR + EQMG + EQMB;
	EQM_global /= 3*N;
	EQMR /= N;
	EQMG /= N;
	EQMB /= N;
	
	
	float PSNR_R = 10 * std::log( (255.0f * 255.0f) / EQMR );
	float PSNR_G = 10 * std::log( (255.0f * 255.0f) / EQMG );
	float PSNR_B = 10 * std::log( (255.0f * 255.0f) / EQMB );
	float PSNR_global = 10 * std::log( (255.0f * 255.0f) / EQM_global );
	
	std::cout << "\e[1;31m" << PSNR_R << " " 
	          << "\e[1;32m" << PSNR_G << " " 
	          << "\e[1;34m" << PSNR_B << " "
	          << "\e[0m" << PSNR_global << std::endl;
}

double EQM (ImageBase& originale, ImageBase& compressed) {
  const unsigned int w = originale.getWidth();
  const unsigned int h = originale.getHeight();
  const unsigned int N = h*w;

  unsigned int sum = 0;
  for (int i = 0; i < h; ++i) {
    for (int j = 0; j < w; ++j) {
      sum += (originale[i * 3][j * 3] - compressed[i * 3][j * 3]) * (originale[i * 3][j * 3] - compressed[i * 3][j * 3]);
      sum += (originale[i * 3][j * 3 + 1] - compressed[i * 3][j * 3 + 1]) * (originale[i * 3][j * 3 +1] - compressed[i * 3][j * 3 + 1]);
      sum += (originale[i * 3][j * 3 + 2] - compressed[i * 3][j * 3 + 2]) * (originale[i * 3][j * 3 +2] - compressed[i * 3][j * 3 + 2]);
    }
  }

  return double(sum) / (3 * N); 
}


double PSNR2 (ImageBase& originale, ImageBase& compressed) {
  double eqm = EQM(originale, compressed);
  return 10 * log(double(255*255) / eqm);
}

unsigned char clamp (int n) {
	if (n < 0) {
		return 0;
	} else if (n > 255) {
		return 255;
	} else {
		return n;
	}
}

void compresserHuffman (vector<unsigned char> img) {
	
   FILE *src, *dst, *frq, *rest;
   ofstream fichier("rle.txt", ios::out | ios::trunc);

	/* Ouverture du fichier source en lecture */
   if (fichier) {
	   for(int i = 0; i < img.size() - 1; i++) {
	   		fichier << img.at(i);
	    }

	    fichier.close();   	
   }


   /* Ouverture du fichier source en lecture */
   if ((src=fopen("rle.txt", "rb"))==NULL)
   {
      perror("fopen");

   }


   /* Ouverture du fichier source en lecture */
   if ((rest=fopen("restore.txt", "wb"))==NULL)
   {
      perror("fopen tralala");
    }

   /* Ouverture du fichier cible en écriture */
   if ((dst=fopen("compressed.txt", "wb"))==NULL)
   {
      perror("fopen");

   }

   
    frq=NULL;
   /* Allocation mémoire pour les données diverses necessaires à la construction de l'arbre */
   if ((arbre_d=(struct arbre_data *)malloc(512*sizeof(struct arbre_data)))==NULL)
   {
      perror("malloc");

   }

   /* Allocation d'une zone mémoire pour l'arbre */
   if ((arbre=(struct arbre *)malloc(512*sizeof(struct arbre)))==NULL)
   {
      free(arbre_d);

   }


  // Parcours de la grille
  unsigned int occurence[256] = {0}; 

  int size = img.size() - 1; 
  for (int i = 0; i < size; i++) {
    occurence[img.at(i)]++;
  }

  


   
  //huffman_creer_fichier_frequences(src, frq);
   huffman_compacter(src, dst, frq);
   //huffman_compacter(diff, rest, NULL);
   //huffman_decompacter(dst, rest, frq);
/*
   if (*argv[1]=='c')
      huffman_compacter(src, dst, frq);
   else if (*argv[1]=='d')
      huffman_decompacter(src, dst, frq);
   else if (*argv[1]=='f')
      huffman_creer_fichier_frequences(src, dst);
*/
   /* Libération de la mémoire */
   free(arbre_d);
   free(arbre);

   /* Fermeture des fichiers */
   fclose(src);
   fclose(dst);
   fclose(rest);
   if (frq!=NULL)
      fclose(frq);

  #ifdef LINUX_COMPIL
     printf("Linux rules!\n");
  #endif

}


void initMatrix(int *** matrix, unsigned int size) {
	matrix = new int**[size];
	for (unsigned int i = 0; i < size; ++i) {
		matrix[i] = new int* [size];
		for (unsigned int j = 0; j < size; ++j) {
			matrix[i][j] = new int [3];
			matrix[i][j][0] = 0;
			matrix[i][j][1] = 0;
			matrix[i][j][2] = 0;
		}	
	}
}

void deleteMatrix(int *** matrix, unsigned int size) {
	for (unsigned int i = 0; i < size; ++i) {
		for (unsigned int j = 0; j < size; ++j) {
			delete[] matrix[i][j];	
		}

		delete[] matrix[i];
	}

	delete[] matrix;
}


vector<unsigned char> codageRLE (int ***matrix, unsigned int size) {
	vector<unsigned char> rle;
	int nb = 1;
	for (unsigned int k = 0; k < 3; ++k) {
		for (unsigned int i = 0; i < size; ++i){
			for (unsigned int j = 0; j < size; ++j) {
				//rle.push_back(matrix[i][j][k]);
				 
				if (j == size-1 ) {
					if ( i < size -1){
						if (matrix [i][j][k] == matrix[i+1][0][k] ){
							nb++;
						}
					}
				}
				else if (matrix[i][j][k] == matrix[i][j+1][k] ){
					nb ++;
				}
				else {
					rle.push_back(nb);
					rle.push_back(matrix[i][j][k]);
					nb = 1;
				}
			}	
		}
		rle.push_back(nb);
		rle.push_back(matrix[size-1][size-1][k]);
		nb =1;
	}

	return rle;
}


int*** decodageRLE (vector<unsigned char> rle, unsigned int size){
	int ***img;
	img = new int**[size];
	for (unsigned int i = 0; i < size; ++i) {
		img[i] = new int* [size];
		for (unsigned int j = 0; j < size; ++j) {
			img[i][j] = new int[3];
			img[i][j][0] = 0.0;
			img[i][j][1] = 0.0;
			img[i][j][2] = 0.0;
		}	
	}
	int x =0;
	int y =0; 
	int z =0;
	for (int i = 0; i < rle.size(); i++){
		for (int nb = 0;  nb < rle.at(i); nb++ ){
			img[y][x][z] = rle.at(i+1);
			x++;
			if (x == size ){
				x = 0;
				y ++;
				if (y == size){
					y = 0;
					z ++;
				}
			}	

		}
		i++;
	}	
	for (unsigned int k = 0; k < 3; ++k) {
	for (unsigned int i =0; i < size; ++i) {
			for (unsigned int j =0; j < size; ++j) {
					cout << (unsigned char) img [i][j][k] << " ";
				}cout << endl;	
			}
			cout << "***************" <<endl;
		}

	return img;

}

void flouter (ImageBase& couleur) {
   int w = couleur.getWidth();
   int h = couleur.getHeight();
   
   ImageBase copie (w,h,true);

   for (int i = 0; i < h; i++) {
     for (int j = 0; j < w; j++) {
       copie[i*3][j*3 + 0] = couleur[i*3][j*3 + 0];
       copie[i*3][j*3 + 1] = couleur[i*3][j*3 + 1];
       copie[i*3][j*3 + 2] = couleur[i*3][j*3 + 2];
     }
   }
   h--;
   w--;
   for (int i = 1; i < h; i++) {
     for (int j = 1; j < w; j++) {
       for (int k = 0; k < 3; k++) {
	 double p1 = copie[(i-1)*3][(j-1)*3 + k];
	 double p2 = copie[(i-1)*3][j*3+k];
	 double p3 = copie[(i-1)*3][(j+1)*3 + k];
	 double p4 = copie[i*3][(j-1)*3 + k];
	 double p5 = copie[i*3][(j+1)*3 + k];
	 double p6 = copie[(i+1)*3][(j-1)*3 + k];
	 double p7 = copie[(i+1)*3][j*3 + k];
	 double p8 = copie[(i+1)*3][(j+1)*3 + k];


	 int pixel =  (1.0/8) * (p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8);
	 couleur[i*3][j*3 + k] = pixel;//(couleur[i*3][j*3 + k] + pixel) / 2;
       }
     }
   }
}
		
int main(int argc, char **argv) {
	
	ImageBase image;
	image.load("./pepper.ppm");

	image.rgbToYCrCb();


	const unsigned int height = image.getHeight();
	ImageBase ondelette (height, height, true);
	ondelette.copy(image);
	ImageBase previous (height, height, true);
	previous.copy(image);
	//ImageBase ondelette (height, height, true);

	int*** lastIteration;
	int*** currentImage;
	int*** currentRecons;

	double crcbMultiplier = 4;

	unsigned int quart = height;

	// On va parcourir l'image par bloc de 4 pixels

	int **bloc = new int *[4];
	for (unsigned int k = 0; k < 4; ++k) {
		bloc[k] = new int [3];
	}

	int *x = new int [3];
	int *y = new int [3]; 
	int *K = new int [3]; 
	int *l = new int [3]; 

	for (unsigned int k = 0; k < 3; ++k) {
		x[k] = 0;
		y[k] = 0;
		K[k] = 0;
		l[k] = 0;
	}



	unsigned int nbDecomposition = 2;

	double *coeff = new double [4];
	coeff [0] =  1;//2 * (nbDecomposition -1)* 1;
	coeff [1] =  2;//2 * (nbDecomposition -1) * 3;
	coeff [2] =  2;//2 * (nbDecomposition -1) * 3;
	coeff [3] =  4;//2 * (nbDecomposition -1) * 3;

	currentImage = new int**[height];
	for (unsigned int i = 0; i < height; ++i) {
		currentImage[i] = new int* [height];
		for (unsigned int j = 0; j < height; ++j) {
			currentImage[i][j] = new int [3];
			currentImage[i][j][0] = 0;
			currentImage[i][j][1] = 0;
			currentImage[i][j][2] = 0;
		}	
	}
	
	for (unsigned int i = 0; i < height; ++i) {
		for (unsigned int j = 0; j < height; ++j) {
			for (unsigned int k = 0; k < 3; ++k) {
				currentImage[i][j][k] = ondelette[i * 3][j * 3 + k];
			}
		}
	}
	
	for (int n = 0; n < nbDecomposition; ++n) {
		
		for (int k = 1; k < 4; ++k) {
			coeff[k] =  coeff[k] / 2;
		}

	
		lastIteration = new int**[quart];
		for (unsigned int i = 0; i < quart; ++i) {
			lastIteration[i] = new int* [quart];
			for (unsigned int j = 0; j < quart; ++j) {
				lastIteration[i][j] = new int [3];
				lastIteration[i][j][0] = 0;
				lastIteration[i][j][1] = 0;
				lastIteration[i][j][2] = 0;
			}	
		}
	

		for (unsigned int i =0; i < quart; ++i) {
			for (unsigned int j =0; j < quart; ++j) {
				for (unsigned int k = 0; k < 3; ++k) {
					previous[i * 3][j * 3 + k] = ondelette [i * 3] [j * 3 + k];
					lastIteration[i][j][k] = currentImage[i][j][k];
				}
			}	
		}


		quart /= 2;

		for (unsigned int i = 0; i < (2 * quart); i += 2) {
			for (unsigned int j = 0; j < (2 * quart); j += 2) {
				
				for (unsigned int k = 0; k < 3; ++k) {
					
					bloc[0][k] = lastIteration[i] [j] [k];
					bloc[1][k] = lastIteration[i] [j + 1] [k];
					bloc[2][k] = lastIteration[i] [j] [k];
					bloc[3][k] = lastIteration[i + 1] [j + 1] [k];
				}


				for (unsigned int k = 0; k < 3; ++k) {
					// On fait l'approx
					x[k] = (bloc[0][k] + bloc[1][k]) / 2;
					y[k] = (bloc[2][k] + bloc[3][k]) / 2;
					/*
					ondelette[(i/2) * 3][(j/2) * 3 + k] = ((x[k] + y[k]) / 2);
					currentImage[i/2][j/2][k] = ((x[k] + y[k]) / 2);
*/
					// On fait les détails verticaux
					K[k] = (bloc[0][k] - bloc[1][k]);
					l[k] = (bloc[2][k] - bloc[3][k]);
					if (k == 0) {

						ondelette[(i/2) * 3][(j/2) * 3 + k] = ((x[k] + y[k]) / 2);
						currentImage[i/2][j/2][k] = ((x[k] + y[k]) / 2);
						
						ondelette[(i/2) * 3][(quart + (j/2)) * 3 + k] = ((K[k] + l[k]) / 2) / coeff[1] + 128;
						currentImage[i/2][quart + (j/2)][k] = ((K[k] + l[k]) / 2) / coeff[1] + 128;
		
						// On fait les détails horizontaux
						ondelette[(quart + (i/2)) * 3][(j/2) * 3 + k] = (x[k] - y[k]) / coeff[2] + 128;//  + 128f / coeff[2];
						currentImage[quart + (i/2)][j/2][k] = (x[k] - y[k]) / coeff[2] + 128;

						// On fait les hautes fréquences
						ondelette[(quart + (i/2)) * 3][(quart + (j/2)) * 3 + k] = (K[k] - l[k]) / coeff[3] + 128;//; + 128f / coeff[3];
						currentImage[quart + (i/2)][quart + (j/2)][k] = (K[k] - l[k]) / coeff[3] + 128;
					
					} else {
						ondelette[(i/2) * 3][(j/2) * 3 + k] = ((x[k] + y[k]) / 2) / crcbMultiplier;
						currentImage[i/2][j/2][k] = ((x[k] + y[k]) / 2) / crcbMultiplier;

						ondelette[(i/2) * 3][(quart + (j/2)) * 3 + k] = ((K[k] + l[k]) / 2) / (coeff[1] * crcbMultiplier)+ 128;
						currentImage[i/2][quart + (j/2)][k] = ((K[k] + l[k]) / 2) / (coeff[1] * crcbMultiplier) + 128;
		
						// On fait les détails horizontaux
						ondelette[(quart + (i/2)) * 3][(j/2) * 3 + k] = (x[k] - y[k]) / (coeff[2] * crcbMultiplier) + 128;//  + 128f / coeff[2];
						currentImage[quart + (i/2)][j/2][k] = (x[k] - y[k]) / (coeff[2]  * crcbMultiplier) + 128;

						// On fait les hautes fréquences
						ondelette[(quart + (i/2)) * 3][(quart + (j/2)) * 3 + k] = (K[k] - l[k]) / (coeff[3] * crcbMultiplier) + 128;//; + 128f / coeff[3];
						currentImage[quart + (i/2)][quart + (j/2)][k] = (K[k] - l[k]) / (coeff[3] * crcbMultiplier)  + 128;
					}
					
				}
			}
		}

		deleteMatrix(lastIteration, quart);

		//std::string s = std::to_string(n);

		//ondelette.save("ondelette.ppm");
	}
	

	//ondelette.yCrCbToRGB();

	for (unsigned int i = 0; i < height; ++i) {
		for (unsigned int j = 0; j < height; ++j) {
			for (unsigned int k = 0; k < 3; ++k) {
				ondelette[i * 3][j * 3 + k] = currentImage[i][j][k];
			}
		}
	}
	
	ondelette.save("ondelette.ppm");

	ImageBase ndg (height, height, false);
	for (int k = 0; k < 3; ++k) {
		for (unsigned int i = 0; i < height; ++i) {
			for (unsigned int j = 0; j < height; ++j) {
				ndg[i][j] = ondelette[i * 3][j * 3 + k];
			}
		}
		string name = "aaa" + k;
		ndg.save(name);
	}

	std::vector<unsigned char> rle = codageRLE(currentImage, height);
	cout << "size rle :" << rle.size() << endl;

	compresserHuffman(rle);

	// Reconstruction
	ImageBase recons (height, height, true);
	recons.copy(ondelette);

	//initMatrix(currentRecons, height);
	currentRecons = new int**[height];
	for (unsigned int i = 0; i < height; ++i) {
		currentRecons[i] = new int* [height];
		for (unsigned int j = 0; j < height; ++j) {
			currentRecons[i][j] = new int [3];
			currentRecons[i][j][0] = 0;
			currentRecons[i][j][1] = 0;
			currentRecons[i][j][2] = 0;
		}	
	}
	
	for (unsigned int i = 0; i < height; ++i) {
		for (unsigned int j = 0; j < height; ++j) {
			for (unsigned int k = 0; k < 3; ++k) {
				currentRecons[i][j][k] = currentImage[i][j][k];
			}
		}
	}


	for (int n = nbDecomposition; n > 0; --n) {

		quart *= 2;	
		//initMatrix(lastIteration, quart);
		
		lastIteration = new int**[quart];
		for (unsigned int i = 0; i < quart; ++i) {
			lastIteration[i] = new int* [quart];
			for (unsigned int j = 0; j < quart; ++j) {
				lastIteration[i][j] = new int [3];
				lastIteration[i][j][0] = 0;
				lastIteration[i][j][1] = 0;
				lastIteration[i][j][2] = 0;
			}	
		}
	

		for (unsigned int i =0; i < quart; ++i) {
			for (unsigned int j =0; j < quart; ++j) {
				for (unsigned int k = 0; k < 3; ++k) {
					lastIteration[i][j][k] = currentImage[i][j][k];
				}
			}	
		}

		for (unsigned int i =0; i <= quart/2 ; ++i) {
			for (unsigned int j =0; j < quart/2 ; ++j) {
				for (unsigned int k = 0; k < 3; ++k) {
					lastIteration[i][j][k] = currentRecons[i][j][k];
				}
			}	
		}
	

		for (unsigned int i = 0; i < (quart / 2); ++i) { 
			for (unsigned int j = 0; j < (quart / 2); ++j) {
				//cout << "(" << i <<","<<j<<")" << endl;
				/*
				bloc[0][0] = previous[i * 3][j * 3] * coeff[0];
				bloc[0][1] = previous[i * 3][j * 3 + 1] * coeff[0];
				bloc[0][2] = previous[i * 3][j * 3 + 2]  * coeff[0];

				bloc[1][0] = previous[i * 3][(j + quart/2) * 3] * coeff[1];
				bloc[1][1] = previous[i * 3][(j + quart/2) * 3 + 1] * coeff[1];
				bloc[1][2] = previous[i * 3][(j + quart/2) * 3 + 2] * coeff[1];

				bloc[2][0] = previous[(i + quart/2) * 3][j * 3]  * coeff[2];
				bloc[2][1] = previous[(i + quart/2) * 3][j * 3 + 1]  * coeff[2];
				bloc[2][2] = previous[(i + quart/2) * 3][j * 3 + 2] * coeff[2];

				bloc[3][0] = previous[(i + quart/2) * 3][(j + quart/2) * 3] * coeff[3];
				bloc[3][1] = previous[(i + quart/2) * 3][(j + quart/2) * 3 + 1] * coeff[3];
				bloc[3][2] = previous[(i + quart/2) * 3][(j + quart/2) * 3 + 2] * coeff[3];
*/

				for (unsigned int k = 0; k < 3; ++k) {
					if (k > 0) {
						bloc[0][k] = clamp(lastIteration[i][j][k]) * crcbMultiplier;
					
						bloc[1][k] = clamp(lastIteration[i][(j + quart/2)][k]  - 128) * coeff[1] * crcbMultiplier;//* coeff[1];
						
						bloc[2][k] = clamp((lastIteration[i + quart/2][j][k]) - 128) * coeff[2] * crcbMultiplier;// * coeff[2] - 128 * coeff[2];
						
						bloc[3][k] = clamp((lastIteration[(i + quart/2)][(j + quart/2)][k]) - 128) * coeff[3] * crcbMultiplier; // * coeff[3] - 128 * coeff[2];
						
					} else {
						bloc[0][k] = clamp(lastIteration[i][j][k]);
					
						bloc[1][k] = clamp(lastIteration[i][(j + quart/2)][k]  - 128) * coeff[1];//* coeff[1];
						
						bloc[2][k] = clamp((lastIteration[i + quart/2][j][k]) - 128) * coeff[2];// * coeff[2] - 128 * coeff[2];
						
						bloc[3][k] = clamp((lastIteration[(i + quart/2)][(j + quart/2)][k]) - 128) * coeff[3];// * coeff[3] - 128 * coeff[2];
						
					}
					
				}


				for (unsigned int k = 0; k < 3; ++k) {
					int Y = bloc[0][k] - bloc[2][k] / 2;
					int L = bloc[1][k] - bloc[3][k] / 2;
					int X = bloc[2][k] + Y;
					int K = bloc[3][k] + L;

					int B = X - K / 2;
					int D = Y - L / 2;
					int A = K + B;
					int C = L + D;
					 // calcul 2
					//double D = bloc[0][k] - bloc[1][k] * 0.5 - bloc[2][k] * 0.5 + bloc[3][k] * 0.5;
					recons[(2 * i + 1) * 3][(2 * j + 1) * 3 + k] = clamp(D); //(D <= 0) ? 0 : D;
					currentRecons[2*i + 1][2*j + 1][k] = D;
					//recons[(2 * i + 1) * 3][(2 * j + 1) * 3 + k] = 0;
					
					//double C = bloc[1][k] - bloc[3][k] * 0.5 + D;
					//recons[(2 * i) * 3][((2 * j + 1) * 3) + k] =  C; //(C <= 0) ? 0 : C;
					recons[(2 * i + 1) * 3][((2 * j) * 3) + k] = clamp(C);

					currentRecons[2*i + 1][2*j][k] = C;

					//double B = bloc[2][k] - bloc[3][k] * 0.5 + D;
					//recons[(2 * i + 1)* 3][(2 * j) * 3 + k] = 128 + B;//(B <= 0) ? 0 : B;
					recons[(2 * i)* 3][(2 * j + 1) * 3 + k] = clamp(B);
					currentRecons[2*i][2*j + 1][k] = B;
					//double A = bloc[3][k] + B + C - D;
					//recons[2* i * 3][2 * j * 3 + k] = 128 + A;//(A <= 0) ? 0 : A;
					recons[2* i * 3][2 * j * 3 + k] = clamp(A);//(A <= 0) ? 0 : A;
					currentRecons[2*i][2*j][k] = A;

						//cout << A << " " << B << " " << C << " " << D << endl;
				}
					// On fait les détails verticaux
					
					// On fait les détails horizontaux
			
					// On fait les hautes fréquences
						
				

			}
		}

		
		for (int k = 1; k < 4; ++k) {
			coeff[k] *= 2;
		}

		
		//string namr = "aaaarecons" + n;
		//recons.save(namr);

		//quart *= 2;

		deleteMatrix(lastIteration, quart);
		//std::string s = std::to_string(n);

		//ondelette.save("ondelette.ppm");
	}

	for (int w = 0; w < height; ++w) {
		for (unsigned int j = 0; j < height; ++j) {
			for (unsigned int k = 0; k < 3; ++k) {
				recons[w * 3][j * 3 + k] = (unsigned char) clamp(currentRecons[w][j][k]);
			}
		}
	}

	image.yCrCbToRGB();

	recons.yCrCbToRGB();
	recons.save("reconsPas	.ppm");
	//cout << rle.size() * 8 << endl; 
	PSNR(image, recons);
	flouter(recons);
	recons.save("recons.ppm");


	for (int k = 0; k < 3; ++k) {
		for (unsigned int i = 0; i < height; ++i) {
			for (unsigned int j = 0; j < height; ++j) {
				ndg[i][j] = recons[i * 3][j * 3 + k];
			}
		}
		string name = "bbb" + k;
		ndg.save(name);
	}

	PSNR(image, recons);
	//cout << "psnr:"<< PSNR2(image, recons)<< endl;

	for (unsigned int k = 0; k < 4; ++k) {
		delete[] bloc[k];
	}

	delete[] bloc;
	delete[] x;
	delete[] y;
	delete[] K;
	delete[] l;
	deleteMatrix(currentRecons, height);
	deleteMatrix(currentImage, height);
	return 0;
}
