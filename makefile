CFLAGS=-Wall -Werror -ansi -pedantic

all: prog

prog: main.o ~/TraitementImage/lib/C++/ImageBase.o
	g++ main.o  ~/TraitementImage/lib/C++/ImageBase.o -o prog $(CFLAGS)

clean :
	rm -f *.o

%.o : %.c
	g++ -o $@ -c $< $(CFLAGS)
