/******************************************************************************
* ICAR_Library
*
* Fichier : ImageBase.cpp
*
* Description : Voir le fichier .h
*
* Auteur : Mickael Pinto
*
* Mail : mickael.pinto@live.fr
*
* Date : Octobre 2012
*
*******************************************************************************/

#include "ImageBase.h"
#include "image_ppm.h"



ImageBase::ImageBase(void) {
	isValid = false;
	init();
}

ImageBase::ImageBase(int imWidth, int imHeight, bool isColor) {
	isValid = false;
	init();

	color = isColor;
	height = imHeight;
	width = imWidth;
	nTaille = height * width * (color ? 3 : 1);
	
	if(nTaille == 0)
		return;
	
	allocation_tableau(data, OCTET, nTaille);
	dataD = (double*)malloc(sizeof(double) * nTaille);
	isValid = true;
}


ImageBase::~ImageBase(void) {
	reset();
}

void ImageBase::init() {
	if(isValid) {
		free(data);
		free(dataD);
	}

	data = 0;
	dataD = 0;
	height = width = nTaille = 0;
	isValid = false;
}

void ImageBase::reset() {
	if(isValid) {
		free(data);
		free(dataD);
	}
	isValid = false;
}

void ImageBase::load(const char *filename) {
	init();

	int l = strlen(filename);

	// Le fichier ne peut pas etre que ".pgm" ou ".ppm"
	if(l <= 4) {
		printf("Chargement de l'image impossible : Le nom de fichier n'est pas conforme, il doit comporter l'extension, et celle ci ne peut �tre que '.pgm' ou '.ppm'");
		exit(0);
	}

	int nbPixel = 0;

	// L'image est en niveau de gris
	if( strcmp(filename + l - 3, "pgm") == 0) {
		color = false;
		lire_nb_lignes_colonnes_image_pgm(filename, &height, &width);
		nbPixel = height * width;
  
		nTaille = nbPixel;
		allocation_tableau(data, OCTET, nTaille);
		lire_image_pgm(filename, data, nbPixel);
	} else if ( strcmp(filename + l - 3, "ppm") == 0) {
	  // L'image est en couleur
		color = true;
		lire_nb_lignes_colonnes_image_ppm(filename, &height, &width);
		nbPixel = height * width;
  
		nTaille = nbPixel * 3;
		allocation_tableau(data, OCTET, nTaille);
		lire_image_ppm(filename, data, nbPixel);
	} else {
		printf("Chargement de l'image impossible : Le nom de fichier n'est pas conforme, il doit comporter l'extension, et celle ci ne peut �tre que .pgm ou .ppm");
		exit(0);
	}
	
	dataD = (double*)malloc(sizeof(double) * nTaille);

	isValid = true;
}

void ImageBase::load(std::string& filename) {
	load(filename.c_str());
}

bool ImageBase::save(const char *filename) {
	if(!isValid) {
		printf("Sauvegarde de l'image impossible : L'image courante n'est pas valide");
		exit(0);
	}

	if(color)
		ecrire_image_ppm(filename, data,  height, width);
	else
		ecrire_image_pgm(filename, data,  height, width);

	return true;
}

bool ImageBase::save(std::string& filename) {
	return save(filename.c_str());
}
/*
ImageBase *ImageBase::getPlan(PLAN plan) {
	if( !isValid || !color )
		return 0;

	ImageBase *greyIm = new ImageBase(width, height, false);
	
	switch (plan) {
	case PLAN_R:
		planR(greyIm->data, data, height * width);
		break;
	case PLAN_G:
		planV(greyIm->data, data, height * width);
		break;
	case PLAN_B:
		planB(greyIm->data, data, height * width);
		break;
	default:
		printf("Il n'y a que 3 plans, les valeurs possibles ne sont donc que 'PLAN_R', 'PLAN_G', et 'PLAN_B'");
		exit(0);
		break;
	}

	return greyIm;
}
*/
void ImageBase::copy(const ImageBase &copy) {
	reset();
	
	isValid = false;
	init();
	
	color = copy.color;
	height = copy.height;
	width = copy.width;
	nTaille = copy.nTaille;
	isValid = copy.isValid;
	
	if(nTaille == 0)
		return;
	
	allocation_tableau(data, OCTET, nTaille);
	dataD = (double*)malloc(sizeof(double) * nTaille);
	isValid = true;

	for(int i = 0; i < nTaille; ++i) {
		data[i] = copy.data[i];
		dataD[i] = copy.dataD[i];
	}

}

unsigned char *ImageBase::operator[](int l) {
	if(!isValid) {
		printf("L'image courante n'est pas valide");
		exit(0);
	}
	
	if((!color && l >= height) || (color && l >= height*3)) {
		printf("L'indice se trouve en dehors des limites de l'image");
		exit(0);
	}
	
	return data+l*width;
}

ImageBase ImageBase::griser(ImageBase& color) {
  const unsigned int w = color.getWidth();
  const unsigned int h = color.getHeight();
  
  ImageBase ndg (w, h, false);
  for (unsigned int i = 0; i < h; ++i) {
    for (unsigned int j = 0; j < w; ++j) {
      double r = color[i*3][j*3 + 0];
      double g = color[i*3][j*3 + 1];
      double b = color[i*3][j*3 + 2];

      // Calcul de la luminance
      ndg[i][j] = int(0.299 * r + 0.587 * g + 0.114 * b); 
    }
  }

  return ndg;
}

void ImageBase::griser() {
	copy(ImageBase::griser(*(this)));
}

void ImageBase::rgbToYCrCb() {
	ImageBase ycrcb;
	ycrcb.copy((*this));

	const unsigned int w = getWidth();
  	const unsigned int h = getHeight();


  	for (unsigned int i = 0; i < h; ++i) {
	    for (unsigned int j = 0; j < w; ++j) {
			ycrcb[i * 3][j * 3] = double(0.299 * (*this)[i * 3][j * 3]) + double(0.587 * (*this)[i*3][j*3 + 1]) + double(0.114 * (*this)[i*3][j*3 + 2]);
	      	ycrcb[i * 3][j * 3 + 1] = double(0.499 * (*this)[i * 3][j * 3]) - double(0.418 * (*this)[i*3][j*3 + 1]) - double(0.0813 * (*this)[i*3][j*3 + 2]) + 128;
	      	ycrcb[i * 3][j * 3 + 2] = double(-0.1687 * (*this)[i * 3][j * 3]) - double(0.331 * (*this)[i*3][j*3 + 1]) + double(0.499 * (*this)[i*3][j*3 + 2]) + 128;
	    }
  	}

  	copy(ycrcb);
}

void ImageBase::yCrCbToRGB() {
	ImageBase rgb;
	rgb.copy((*this));

	const unsigned int w = getWidth();
  	const unsigned int h = getHeight();

  	for (unsigned int i=0; i < h; ++i) {
    for (unsigned int j=0; j < w; ++j) {
	    rgb[i * 3][j * 3] = std::max(0, std::min(255, int(double((*this)[i * 3][j * 3]) + double(1.402 * char((*this)[i * 3][j * 3 + 1] - 128)))));
	    rgb[i * 3][j * 3 + 1] = std::max(0, std::min(255,int(double((*this)[i * 3][j * 3]) - double(0.344 * char((*this)[i * 3][j * 3 + 2] - 128)) - double(0.714 * char((*this)[i * 3][j * 3 + 1] - 128)))));
	    rgb[i * 3][j * 3 + 2] = std::max(0, std::min(255,int(double((*this)[i * 3][j * 3]) + double(1.772 * char((*this)[i * 3][j * 3 + 2] - 128)))));
    }   
  }

  	copy(rgb);
}
