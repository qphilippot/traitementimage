#include "../lib/C++/ImageBase.h"


#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <sstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <blitz/tinymat2.h>
#include <bitset>
 


using namespace std; 
 

void exo1 () {
  bool generatrice [7][4];

  generatrice[0][0] = 1; generatrice[0][1] = 1; generatrice[0][2] = 0;  generatrice[0][3] = 1;
  generatrice[1][0] = 1; generatrice[1][1] = 0; generatrice[1][2] = 1;  generatrice[1][3] = 1;
  generatrice[2][0] = 1; generatrice[2][1] = 0; generatrice[2][2] = 0;  generatrice[2][3] = 0;
  generatrice[3][0] = 0; generatrice[3][1] = 1; generatrice[3][2] = 1;  generatrice[3][3] = 1;
  generatrice[4][0] = 0; generatrice[4][1] = 1; generatrice[4][2] = 0;  generatrice[4][3] = 0;
  generatrice[5][0] = 0; generatrice[5][1] = 0; generatrice[5][2] = 1;  generatrice[5][3] = 0;
  generatrice[6][0] = 0; generatrice[6][1] = 0; generatrice[6][2] = 0;  generatrice[6][3] = 1;

  bitset<8> mot [11];
  bitset<8> c [11];

  string INE = "1105031393e";

  for (std::size_t i = 0; i < INE.size(); ++i) {
    mot[i] = bitset<8> ((INE.c_str()[i]));
  }



  unsigned int n = 7;
  unsigned int w = 22; // nombre de mots * 2

  std::vector<unsigned char> m;

  for (unsigned int j = 0; j < w; j++) {
    for (unsigned int i = 0; i < n; i++) {

      bool sum = false;
      for (unsigned int k = 0; k < 4; k++) {
        int p = k;
        if ((j % 2 == 0)) { p += 4; }
        sum = (sum + (generatrice[i%7][k] && mot[j % 11][p])) % 2;
      }
      m.push_back((sum ? '1' : '0'));
    }
  }

  unsigned int size = m.size();
  for (unsigned int i=0; i < size; ++i) {
    if (i % 7 == 0) { cout << endl; }
    cout << m[i];
  }
}

vector<string> split(string str, char delimiter) {
  vector<string> internal;
  stringstream ss(str); // Turn the string into a stream.
  string tok;
  
  while(getline(ss, tok, delimiter)) {
    internal.push_back(tok);
  }
  
  return internal;
}

ImageBase rawToPGM (const unsigned char* data, int nbColonne, int nbLigne) {
  ImageBase img (nbColonne, nbLigne, false);
  for (int i = 0; i < nbLigne; ++i) {
    for (int j=0; j < nbColonne; ++j) {
      img[i][j] = data[(i * nbColonne) + j];
    }
  }
  img.save("image.pgm");
  return img;
}

ImageBase rawToPPM (const unsigned char* data, int nbColonne, int nbLigne) {
  ImageBase img (nbColonne, nbLigne, true);
  for (int i = 0; i < nbLigne; ++i) {
    for (int j=0; j < nbColonne; ++j) {
      img[i *3][j * 3] = data[i * 3 * nbColonne + j * 3];
      img[i *3][j * 3 + 1] = data[i * 3 * nbColonne + j * 3 + 1];
      img[i *3][j * 3 + 2] = data[i * 3 * nbColonne + j * 3 + 2];
    }
  }
  img.save("image.ppm");
  return img;
}

ImageBase rawAlphaToPPM (const unsigned char* data, int nbColonne, int nbLigne) {
  ImageBase img (nbColonne, nbLigne, true);
  for (int i = 0; i < nbLigne; ++i) {
    for (int j=0; j < nbColonne; ++j) {
      img[i *3][j * 3] = data[i * 4 * nbColonne + j * 4];
      img[i *3][j * 3 + 1] = data[i * 4 * nbColonne + j * 4 + 1];
      img[i *3][j * 3 + 2] = data[i * 4 * nbColonne + j * 4 + 2];
    }
  }
  img.save("image.ppm");
  return img;
}

ImageBase copyImage (const unsigned char* data, int nbColonne, int nbLigne, int nbCanal) {
  if (nbCanal == 1) {
    return rawToPGM(data, nbColonne, nbLigne);
  } else if (nbCanal == 3) {
    return rawToPPM(data, nbColonne, nbLigne);
  } else {
    return rawAlphaToPPM(data, nbColonne, nbLigne);
  }
}


void exo3() {

  // Chargement du logo UM
  vector<string> data = split ("logo.804.805.4.raw",'.');

  unsigned int nbColonne = atoi(data[1].c_str());
  unsigned int nbLigne = atoi(data[2].c_str());
  unsigned int nbCanal = atoi(data[3].c_str());
  unsigned int bufferSize = nbColonne * nbLigne * nbCanal;

  unsigned char* buffer = new unsigned char [bufferSize];
  FILE* f = fopen("./logo.804.805.4.raw", "rb");
  fread(buffer, 1, bufferSize, f);
  fclose(f);
  // Fin chargemnt


  ImageBase logo = copyImage(buffer, nbColonne, nbLigne, nbCanal);
  logo.save("logo.ppm");

  delete buffer;
  data.clear();

  // Chargement du premier qr
  data = split ("QrCode.300.300.1.raw",'.');

  nbColonne = atoi(data[1].c_str());
  nbLigne = atoi(data[2].c_str());
  nbCanal = atoi(data[3].c_str());
  bufferSize = nbColonne * nbLigne * nbCanal;

  buffer = new unsigned char [bufferSize];
  f = fopen("./QrCode.300.300.1.raw", "rb");
  fread(buffer, 1, bufferSize, f);
  fclose(f);

  ImageBase qr1 = copyImage(buffer, nbColonne, nbLigne, nbCanal);
  qr1.save("qr1.pgm");
 
  delete buffer;
  data.clear();
  // fin chargement premier qr

  // Chargement du second qr
  data = split ("QrCode_v35.1320.1320.1.raw",'.');

  nbColonne = atoi(data[1].c_str());
  nbLigne = atoi(data[2].c_str());
  nbCanal = atoi(data[3].c_str());
  bufferSize = nbColonne * nbLigne * nbCanal;

  buffer = new unsigned char [bufferSize];
  f = fopen("./QrCode_v35.1320.1320.1.raw", "rb");
  fread(buffer, 1, bufferSize, f);
  fclose(f);

  ImageBase qr2 = copyImage(buffer, nbColonne, nbLigne, nbCanal);
  qr2.save("qr2.pgm");
 
  delete buffer;
  data.clear();
  // fin chargement 2nd qr



  const unsigned int w = qr1.getWidth();
  const unsigned int h = qr1.getHeight();
  
  // incruster logo dans qrcode
  ImageBase logoResizedColor (w, h, true);
  
  ImageBase logoUbuntu;
  logoUbuntu.load("./ubuntu.ppm");
  
  ImageBase logoUbuntuResizedColor (w, h, true);


  double ratioW = (double(logo.getWidth()) / w);
  double ratioH = (double(logo.getHeight()) / h);

  double ratioW2 = (double(logoUbuntu.getWidth()) / w);
  double ratioH2 = (double(logoUbuntu.getHeight()) / h);

  for (unsigned int i = 0; i < h; ++i) {
    for (unsigned int j = 0; j < w; ++j) {
      //cout << "i = " << i << "  j = " << j << endl;
      logoResizedColor[i * 3][j * 3] = logo [ (unsigned int)(i * ratioH) * 3] [(unsigned int)(j * ratioW) * 3];
      logoResizedColor[i * 3][j * 3 + 1] = logo [ (unsigned int)(i * ratioH) * 3] [(unsigned int)(j * ratioW) * 3 + 1];
      logoResizedColor[i * 3][j * 3 + 2] = logo [ (unsigned int)(i * ratioH) * 3] [(unsigned int)(j * ratioW) * 3 + 2];


      logoUbuntuResizedColor[i * 3][j * 3] = logoUbuntu [ (unsigned int)(i * ratioH2) * 3] [(unsigned int)(j * ratioW2) * 3];
      logoUbuntuResizedColor[i * 3][j * 3 + 1] = logoUbuntu [ (unsigned int)(i * ratioH2) * 3] [(unsigned int)(j * ratioW2) * 3 + 1];
      logoUbuntuResizedColor[i * 3][j * 3 + 2] = logoUbuntu [ (unsigned int)(i * ratioH2) * 3] [(unsigned int)(j * ratioW2) * 3 + 2];
    }    
  }

  logoResizedColor.save("logoResizedColor.ppm");
  logoUbuntuResizedColor.save("logoUbuntuResizedColor.ppm");


  ImageBase BColor (w, h, true);
  ImageBase BUbuntuColor (w, h, true);

  for (unsigned int i = 0; i < h; ++i) {
    for (unsigned int j = 0; j < w; ++j) {
      //cout << "i = " << i << "  j = " << j << endl;
      BColor[i * 3][j * 3] = (qr1[i][j] == 255) ? std::max(logoResizedColor[i * 3][j * 3], (unsigned char)180) : std::min(logoResizedColor[i * 3][j * 3], (unsigned char)50);
      BColor[i * 3][j * 3 + 1] = (qr1[i][j] == 255) ? std::max(logoResizedColor[i * 3][j * 3 + 1], (unsigned char)180) : std::min(logoResizedColor[i * 3][j * 3 + 1], (unsigned char)50);
      BColor[i * 3][j * 3 + 2] = (qr1[i][j] == 255) ? std::max(logoResizedColor[i * 3][j * 3 + 2], (unsigned char)180) : std::min(logoResizedColor[i * 3][j * 3 + 2], (unsigned char)50);

      BUbuntuColor[i * 3][j * 3] = (qr1[i][j] == 255) ? std::max(logoUbuntuResizedColor[i * 3][j * 3], (unsigned char)200) : std::min(logoUbuntuResizedColor[i * 3][j * 3], (unsigned char)20);
      BUbuntuColor[i * 3][j * 3 + 1] = (qr1[i][j] == 255) ? std::max(logoUbuntuResizedColor[i * 3][j * 3 + 1], (unsigned char)200) : std::min(logoUbuntuResizedColor[i * 3][j * 3 + 1], (unsigned char)20);
      BUbuntuColor[i * 3][j * 3 + 2] = (qr1[i][j] == 255) ? std::max(logoUbuntuResizedColor[i * 3][j * 3 + 2], (unsigned char)200) : std::min(logoUbuntuResizedColor[i * 3][j * 3 + 2], (unsigned char)20);
    }    
  }

  BColor.save("fusionColor.ppm");
  BUbuntuColor.save("fusionUbuntuColor.ppm");


  logo.griser(); logo.save("ndg-logo.pgm");
  logoUbuntuResizedColor.griser(); 


  ImageBase logoResized (w, h, false);
  //double ratioW = (double(logo.getWidth()) / w);
  //double ratioH = (double(logo.getHeight()) / h);

  for (unsigned int i = 0; i < h; ++i) {
    for (unsigned int j = 0; j < w; ++j) {
      //cout << "i = " << i << "  j = " << j << endl;
      logoResized[i][j] = logo [ (unsigned int)(i * ratioH)] [(unsigned int)(j * ratioW)];
    }    
  }

  logoResized.save("logoResized.pgm");


  ImageBase B (w, h, false);
  ImageBase BUbuntu (w, h, false);

  for (unsigned int i = 0; i < h; ++i) {
    for (unsigned int j = 0; j < w; ++j) {
      //cout << "i = " << i << "  j = " << j << endl;
      B[i][j] = (qr1[i][j] == 255) ? std::max(logoResized[i][j], (unsigned char)200) : std::min(logoResized[i][j], (unsigned char)20);
      BUbuntu[i][j] = (qr1[i][j] == 255) ? std::max(logoUbuntuResizedColor[i][j], (unsigned char)200) : std::min(logoUbuntuResizedColor[i][j], (unsigned char)20);
    }    
  }

  B.save("fusion.pgm");
  BUbuntu.save("fusionUbuntu.pgm");
}

void exo2() {
  vector<string> data = split ("QrCode_v35.1320.1320.1.raw",'.');

  unsigned int nbColonne = atoi(data[1].c_str());
  unsigned int nbLigne = atoi(data[2].c_str());
  unsigned int nbCanal = atoi(data[3].c_str());
  unsigned int bufferSize = nbColonne * nbLigne * nbCanal;

  unsigned char* buffer = new unsigned char [bufferSize];
  FILE* f = fopen("./QrCode_v35.1320.1320.1.raw", "rb");
  fread(buffer, 1, bufferSize, f);
  fclose(f);
  // Fin chargemnt


  ImageBase qr = copyImage(buffer, nbColonne, nbLigne, nbCanal);
  qr.save("qr.pgm");

  delete buffer;
  data.clear();

  const unsigned int w = qr.getWidth();
  const unsigned int h = qr.getHeight();

  bitset<154> bit;
  for (unsigned int i = 0; i < 154; ++i) {
    bit[i] = rand() % 2;
    cout << bit[i];
  }
  cout << endl << endl;
  unsigned int k = 0;
  for (unsigned int i = 0; i < h; i += 4) {
    for (unsigned int j = 0; j < w; j += 4) {
      
      if ((qr[i][j] == 0) && (k < 154)) {
        if (bit[k] == 1) {
          qr[i+1][j+1] = 255;
          qr[i+2][j+2] = 255;
          qr[i+1][j+2] = 255;
          qr[i+2][j+1] = 255;
        }
        ++k;
      }
    
    }
  }

  qr.save("2-2.pgm");


  for (unsigned int i = 0; i < h; i += 4) {
    for (unsigned int j = 0; j < w; j += 4) {
      if ((qr[i][j] == 0)) {
        qr[i+1][j+1] = 255;
        qr[i+2][j+2] = 255;
        qr[i+1][j+2] = 255;
        qr[i+2][j+1] = 255;
      }
    }
  }

  qr.save("2-1.pgm");
}

int main(int argc, char **argv) {
	
  //exo1();
  exo2();
  //exo3();

	return 0;
}
