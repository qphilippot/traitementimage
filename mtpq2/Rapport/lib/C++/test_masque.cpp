#include "ImageBase.h"
#include "Masque.h"
#include <iostream>
#include <vector>
#include <string>

using namespace std;


int main(int argc, char **argv) {

    if (argc != 2) {
    cerr << "Usage: prog <fileName>" << endl; 
    return -1;
  }
  
  char* fileName = argv[1];
  
  ImageBase originale;
  originale.load(fileName);


  originale.save("originale.pgm");
  
  if (originale.getColor()) {
    originale.griser();
    originale.save("grise.pgm");
  }

  Masque mask(3, 3);
  vector<double> data;

  data.push_back(0); data.push_back(0); data.push_back(0);
  data.push_back(0); data.push_back(1); data.push_back(0);
  data.push_back(0); data.push_back(0); data.push_back(0);
  
  mask.setData(data);

//  mask.apply(originale);

  originale.save("masked");

	return 0;
}
