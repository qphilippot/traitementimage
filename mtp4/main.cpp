#include "../lib/C++/ImageBase.h"


#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <sstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <fstream>

using namespace std;

unsigned char charte [72] = {
  115, 82, 68,
  194,150,130,
  98,122,157,
  87, 108, 67,
  133, 128, 177,
  103, 189, 170,
  214, 126, 44,
  80, 91, 166,
  193, 90, 99,
  94, 60 ,108,
  157, 188, 64,
  224, 163, 46,
  56, 61, 150,
  70, 148, 73,
  175, 54, 60,
  231, 199, 31,
  187, 86, 149,
  8, 133, 161,
  43, 243, 243,
  200, 200, 200,
  160, 160, 160,
  122, 122, 121,
  85, 85, 85,
  52, 52, 52
};



ImageBase fillChart( std::string path ){
  std::vector<unsigned char> buffer;
  ImageBase chart = ImageBase(6, 4, true);
  ifstream stream (path.c_str());
  std::string line = "";

  if(stream.is_open()){
    while (getline(stream, line)) {
      buffer.push_back((unsigned char) atoi(line.c_str()));
    } 
  }

  unsigned char elt;
  unsigned int x = 0;
  unsigned int y = 0;
  unsigned int offset = 0;
  const unsigned int bufferLength = buffer.size();

  for (unsigned int i = 0; i < bufferLength; ++i) {
    elt = buffer.at(i);
    chart[x*3][y*3 + offset] = elt;
    //std::cout << offset << " " << y << " " << x << std::endl;
    ++offset;
    
    if (offset == 3) {
      offset = 0;
      ++y;
    }
    
    if(y == 6) {
      y = 0;
      ++x;
    }
  }
  
  chart.save("testChart.ppm");
  return chart;
}

// Image couleur seulement
int* diffMoy(ImageBase& org, ImageBase& comp){
  int* avg = new int[3]();

  avg[0] = 0;
  avg[1] = 0;
  avg[2] = 0;

  const unsigned int height = org.getHeight();
  const unsigned int width = org.getWidth();
  const unsigned int nbElt = height * width;


  for (unsigned int x = 0; x < height; ++x) {
    for (unsigned int y = 0; y < width; ++y) {
      avg[0] += org[x*3][y*3+0] - comp[x*3][y*3+0];
      avg[1] += org[x*3][y*3+1] - comp[x*3][y*3+1];
      avg[2] += org[x*3][y*3+2] - comp[x*3][y*3+2];
    }
  }


  avg[0] /= (int) nbElt;
  avg[1] /= (int) nbElt;
  avg[2] /= (int) nbElt;


  //  std::cout << avg[0] << " " << avg[1] << " " << avg[2] << std::endl;
  return avg;
}// Image couleur seulement

int diffLumi(ImageBase& org, ImageBase& comp){
  int avg = 0;

  const unsigned int height = org.getHeight();
  const unsigned int width = org.getWidth();
  const unsigned int nbElt = height * width;


  for (unsigned int x = 0; x < height; ++x) {
    for (unsigned int y = 0; y < width; ++y) {
      avg += org[x*3][y*3+0] - comp[x*3][y*3+0];
    }
  }


  avg /= (int) nbElt;

  //  std::cout << avg[0] << " " << avg[1] << " " << avg[2] << std::endl;
  return avg;
}


void appliquerDiff(ImageBase& img, int* diff){
  int R = 0;
  int G = 0;
  int B = 0;

  const unsigned int height = img.getHeight();
  const unsigned int width = img.getWidth();
  
  for (unsigned int x = 0; x < height; ++x ){
    for (unsigned int y = 0; y < width; ++y ){
      R = (int) img[x*3][y*3+0] + diff[0];
      G = (int) img[x*3][y*3+1] + diff[1];
      B = (int) img[x*3][y*3+2] + diff[2];
      
      img[x*3][y*3+0] = std::max(0, std::min(255, R));
      img[x*3][y*3+1] = std::max(0, std::min(255, G));
      img[x*3][y*3+2] = std::max(0, std::min(255, B));
    }
  }

  img.save("appDiff.ppm");
}

void appliquerLum(ImageBase& img, int diff){
  
  int lum;
  const unsigned int height = img.getHeight();
  const unsigned int width = img.getWidth();
  
  for (unsigned int x = 0; x < height; ++x ){
    for (unsigned int y = 0; y < width; ++y ){
      lum = (int) img[x*3][y*3+0] + diff;
      
      img[x*3][y*3+0] = std::max(0, std::min(255, lum));
    }
  }
}



float getEntropy(float* proba){
  float res = 0.0f;

  for (unsigned int i = 0; i < 256; ++i) {
    res -= proba[i] * (std::log(proba[i]) / std::log(2));
  }

  return res;
}


void histowrite(unsigned int* histo,const char* nom){
  std::ofstream out;
  out.open(nom);
  for(size_t i = 0 ; i < 256 ; ++i){
    out << i << " " <<histo[ i ] << std::endl;
  }
  out.close();
}


void diffMap(ImageBase& img){
  ImageBase res;
  res.copy(img);

  for( int x = 0; x < img.getHeight(); ++x ){
    for( int y = 1; y < img.getWidth(); y++ ){
      res[x][y] = img[x][y] - img[x][y-1] + 128; 
    }
  }
  //Core(res,false);
  res.save("res.pgm");
}

ImageBase distanceCouleur (ImageBase& a, ImageBase& b) {
  const unsigned int width = a.getWidth();
  const unsigned int height = a.getHeight();
  unsigned int distance;
   ImageBase map (width, height, false);

  if ((width == b.getWidth()) && (height == b.getHeight())) {
   

    for (unsigned int i = 0; i < height; ++i) {
      for (unsigned int j = 0; j < width; ++j) {
        distance = abs(a[i * 3][j * 3] - b[i * 3][j * 3]);
        distance += abs(a[i * 3][j * 3] - b[i * 3][j * 3]);
        distance += abs(a[i * 3][j * 3] - b[i * 3][j * 3]);

        // clamp
        distance = distance > 255 ? 255 : distance;
        map[i][j] = distance;
      }
    }

    map.save("map.pgm");
  } else {
    cerr << "les tailles ne correspondent pas" << width << "*" << height << "  " << b.getWidth() << "*" << b.getHeight() << endl;
  }
  return map;
}

ImageBase distanceLuminance (ImageBase& a, ImageBase& b) {
  const unsigned int width = a.getWidth();
  const unsigned int height = a.getHeight();
  unsigned int distance;
  ImageBase map (width, height, false);
  
  if ((width == b.getWidth()) && (height == b.getHeight())) {
   

    for (unsigned int i = 0; i < height; ++i) {
      for (unsigned int j = 0; j < width; ++j) {
        map[i][j] = abs(a[i][j] - b[i][j]);
      }
    }

    map.save("mapLum.pgm");
  } else {
    cerr << "les tailles ne correspondent pas" << endl;
  }

  return map;
}

ImageBase griser(ImageBase& color) {
  const unsigned int w = color.getWidth();
  const unsigned int h = color.getHeight();
  
  ImageBase ndg (w, h, false);
  for (unsigned int i = 0; i < h; ++i) {
    for (unsigned int j = 0; j < w; ++j) {
      double r = color[i*3][j*3 + 0];
      double g = color[i*3][j*3 + 1];
      double b = color[i*3][j*3 + 2];

      // Calcul de la luminance
      ndg[i][j] = int(0.299 * r + 0.587 * g + 0.114 * b); 
    }
  }

  return ndg;
}
int main(int argc, char **argv) {
	/*
	if (argc != 2) {
		printf("Usage: prog <fileName> \n"); 
		return 1;
	}
	*/

	//std::string fileName (argv[1]);
	
  ImageBase originale1;
  originale1.load("./1.ppm");
  originale1.save("1.ppm");

  ImageBase originale2;
  originale2.load("./2.ppm");
  originale2.save("2.ppm");


  /*
  const unsigned char nombreCouleur = 24;
  const unsigned int width = originale.getWidth();
  const unsigned int height = originale.getHeight();

  unsigned int x, y, z;
  double n, gainX, gainY, gainZ, distance;
  for (unsigned int i = 0; i < height; ++i) {
    for (unsigned int j = 0; j < width; ++j) {
      gainX = 0;
      gainY = 0;
      gainZ = 0;
      for (unsigned int u = 0; u < nombreCouleur; ++u) {
        x = charte[u * 3];
        y = charte[(u + 1) * 3];
        z = charte[(u + 2) * 3];

        distance = sqrt((x - originale[i * 3][j * 3]) * (x - originale[i * 3][j * 3]) + (y - originale[i * 3][j * 3 + 1]) * (y - originale[i * 3][j * 3 + 1]) + (z - originale[i * 3][j * 3 + 2]) * (z - originale[i * 3][j * 3 + 2])); 
        //cout << "distance :" << distance << endl;
        if (distance < 200 && distance > 10) {
          //cout << "distance" << distance << endl; 
          gainX += (distance ? x : x / ( 2 * distance * distance));
          gainY += (distance ? y : y / ( 2 *distance * distance));
          gainZ += (distance ? z : z / (2 * distance * distance)); 
        }
      }
      
      //gainX += originale[i * 3][j * 3];
      //gainY += originale[i * 3][j * 3 + 1];
      //gainZ += originale[i * 3][j * 3 + 2];

      n = sqrt(gainX*gainX + gainY*gainY + gainZ*gainZ);
      
      gainX /= n;
      gainY /= n;
      gainZ /= n;

      originale[i * 3][j * 3] = gainX * 255;
      originale[i * 3][j * 3 + 1] = gainY * 255;
      originale[i * 3][j * 3 + 2] = gainZ * 255;
      cout << gainX << " " << gainY << " " << gainZ << endl;
    } 
  }


    originale.save("copy.ppm");
*/

  // Chargement des chartes

  //originale.rgbToYCrCb();

  std::string chartPath = "./chart1";
  ImageBase chart1;
  chart1.copy(fillChart(chartPath));
  chart1.save("chartOrignigale1.ppm");
  
  //chart1.rgbToYCrCb();

  chartPath = "./chart2";
  ImageBase chart2;
  chart2.copy(fillChart(chartPath));
  chart2.save("charteOriginale2.ppm");
  //chart2.rgbToYCrCb();

  chartPath = "./chartOrg";
  ImageBase chartOrg;
  chartOrg.copy(fillChart(chartPath));
  //chartOrg.rgbToYCrCb();

  ImageBase distanceCouleurReferenceChart1;
  distanceCouleurReferenceChart1.copy(distanceCouleur(chartOrg, chart1));
  ImageBase distanceCouleurReferenceChart2;
  distanceCouleurReferenceChart2.copy(distanceCouleur(chartOrg, chart2));

  distanceCouleurReferenceChart1.save("distanceCouleurReferenceChart1.pgm");
  distanceCouleurReferenceChart2.save("distanceCouleurReferenceChart2.pgm");

  ImageBase chartRefNdg;
  chartRefNdg.copy(griser(chartOrg));
  //chartRefNdg.griser();

  ImageBase chart1Ndg;
  chart1Ndg.copy(griser(chart1));
  //chart1Ndg.griser();


  ImageBase chart2Ndg;
  chart2Ndg.copy(griser(chart2));
  //chart2Ndg.griser();


  ImageBase distanceNdgReferenceChart1;
  distanceNdgReferenceChart1.copy(distanceLuminance(chartRefNdg, chart1Ndg));
  ImageBase distanceNdgReferenceChart2;
  distanceNdgReferenceChart2.copy(distanceLuminance(chartRefNdg, chart2Ndg));

  distanceNdgReferenceChart1.save("distanceNdgReferenceChart1.pgm");
  distanceNdgReferenceChart2.save("distanceNdgReferenceChart2.pgm");


  int* diffRes1 = diffMoy(chartOrg, chart1);
  int* diffRes2 = diffMoy(chartOrg, chart2);

  ImageBase chart1PostAvg; chart1PostAvg.copy(chart1);
  ImageBase chart2PostAvg; chart2PostAvg.copy(chart2);

  ImageBase image1PostAvg;
  image1PostAvg.copy(originale1);
  appliquerDiff(image1PostAvg, diffRes1);
  image1PostAvg.save("1-post-avg.ppm");

  ImageBase image2PostAvg;
  image2PostAvg.copy(originale2);
  appliquerDiff(image2PostAvg, diffRes2);
  image2PostAvg.save("2-post-avg.ppm");

  appliquerDiff(chart1PostAvg, diffRes1);
  chart1PostAvg.save("chart1-post-avg.ppm");

  appliquerDiff(chart2PostAvg, diffRes1);
  chart2PostAvg.save("chart2-post-avg.ppm");


  ImageBase distanceCouleurReferenceChart1PostAvg;
  distanceCouleurReferenceChart1PostAvg.copy(distanceCouleur(chartOrg, chart1PostAvg));
  //distanceCouleurReferenceChart1.copy(chart1PostAvg);
  
  ImageBase distanceCouleurReferenceChart2PostAvg;
	distanceCouleurReferenceChart2PostAvg.copy(distanceCouleur(chartOrg, chart2PostAvg));
	//distanceCouleurReferenceChart1.copy(chart1PostAvg);
  
  distanceCouleurReferenceChart1PostAvg.save("distanceCouleurReferenceChart1-post-avg.pgm");
  distanceCouleurReferenceChart2PostAvg.save("distanceCouleurReferenceChart2-post-avg.pgm");


  ImageBase chart1PostAvgNdg; chart1PostAvgNdg.copy(griser(chart1PostAvg));
  ImageBase chart2PostAvgNdg; chart2PostAvgNdg.copy(griser(chart2PostAvg));


  ImageBase distanceNdgReferenceChart1PostAvg; distanceNdgReferenceChart1PostAvg.copy(distanceLuminance(chartRefNdg, chart1PostAvgNdg));
  ImageBase distanceNdgReferenceChart2PostAvg; distanceNdgReferenceChart2PostAvg.copy(distanceLuminance(chartRefNdg, chart2PostAvgNdg));

  distanceNdgReferenceChart1PostAvg.save("distanceNdgReferenceChart1PostAvg.pgm");
  distanceNdgReferenceChart2PostAvg.save("distanceNdgReferenceChart2PostAvg.pgm");



  chart1.rgbToYCrCb(); chart2.rgbToYCrCb();

  int diffRes3 = diffLumi(chartOrg, chart1);
  int diffRes4 = diffLumi(chartOrg, chart2);



  ImageBase image1PostLum; image1PostLum.copy(originale1);
  
  appliquerLum(image1PostLum, diffRes3);
  image1PostLum.save("1-post-lum.ppm");

  ImageBase image2PostLum; image2PostLum.copy(originale2);
  appliquerLum(image2PostLum, diffRes4);
  image2PostLum.save("2-post-lum.ppm");


  ImageBase chart1PostLum; chart1PostLum.copy(chart1);
  ImageBase chart2PostLum; chart2PostLum.copy(chart2);

  appliquerLum(chart1PostLum, diffRes3);
  chart1PostLum.yCrCbToRGB();
  chart1PostLum.save("chart1-post-lum.ppm");

  appliquerLum(chart2PostLum, diffRes4);
  chart2PostLum.yCrCbToRGB();
  chart2PostLum.save("chart2-post-lum.ppm");


  ImageBase distanceCouleurReferenceChart1PostLum; distanceCouleurReferenceChart1PostLum.copy(distanceCouleur(chartOrg, chart1PostLum));
  ImageBase distanceCouleurReferenceChart2PostLum; distanceCouleurReferenceChart2PostLum.copy(distanceCouleur(chartOrg, chart2PostLum));

  distanceCouleurReferenceChart1PostLum.save("distanceCouleurReferenceChart1-post-lum.pgm");
  distanceCouleurReferenceChart2PostLum.save("distanceCouleurReferenceChart2-post-lum.pgm");

  ImageBase chart1PostLumNdg; chart1PostLumNdg.copy(griser(chart1PostLum));
 // chart1PostLumNdg.griser();


  ImageBase chart2PostLumNdg; chart2PostLumNdg.copy(griser(chart2PostLum));
  //chart2PostLumNdg.griser();


  ImageBase distanceNdgReferenceChart1PostLum; distanceNdgReferenceChart1PostLum.copy(distanceLuminance(chartRefNdg, chart1PostLumNdg));
  ImageBase distanceNdgReferenceChart2PostLum; distanceNdgReferenceChart2PostLum.copy(distanceLuminance(chartRefNdg, chart2PostLumNdg));

  distanceNdgReferenceChart1PostLum.save("distanceNdgReferenceChart1PostLum.pgm");
  distanceNdgReferenceChart2PostLum.save("distanceNdgReferenceChart2PostLum.pgm");

delete[] diffRes1;
delete[] diffRes2;


/*
  chartPath = "./postAvgChart1";
  ImageBase postchart1;
  postchart1.copy(fillChart(chartPath));
  postchart1.save("_chart1.ppm");
  
  chartPath = "./postAvgChart2";
  ImageBase postchart2;
  postchart2.copy(fillChart(chartPath));
  postchart2.save("_chart2.ppm");
*/
  

  // On calcule la différence moyenne entre les chartes
  //int* diffRes = diffMoy(chartOrg, chart1);
/*
  int diffRes = diffLumi(chartOrg, chart1);

  // On ajoute la différence moyenne à chaque pixel
  //appliquerDiff(originale, diffRes);
  appliquerLum(chart1, diffRes);
  appliquerLum(originale, diffRes);

  originale.yCrCbToRGB();
  chart1.yCrCbToRGB();
  chart2.yCrCbToRGB();
  chartOrg.yCrCbToRGB();

  originale.save("result.ppm");
  chart1.save("post-ch1-withY.ppm");

  // On calcule la différence moyenne entre les chartes
  int* diffRes2 = diffMoy(chartOrg, chart2);

  // On ajoute la différence moyenne à chaque pixel
  appliquerDiff(chart2, diffRes2);

  distanceCouleur(chartOrg, chart2);


  chartOrg.griser();
  chart1.griser();
  chart2.griser();

  distanceLuminance(chartOrg, chart2);
 */
	return 0;
}
