#include "../../lib/C++/ImageBase.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include <cmath>

using namespace std;

int minWidth = 4;
int varianceThreshold = 20;
double rapport = 0.8;
int seuil =10;


double getAverage (ImageBase& image, int iBegin, int jBegin, int iEnd, int jEnd) {
  double sum = 0;
  for (int i=iBegin; i < iEnd; ++i) {
    for (int j=jBegin; j < jEnd; ++j) {
      sum += image[i][j];
    }
  }

  return sum / (((iEnd - iBegin) * (jEnd - jBegin)));
}

double getVariance (ImageBase& image, int iBegin, int jBegin, int iEnd, int jEnd, double average) {
  double sum = 0;
  for (int i=iBegin; i < iEnd; ++i) {
    for (int j=jBegin; j < jEnd; ++j) {
      sum += (image[i][j] - average) * (image[i][j] - average);
    }
  }

  return sum / (((iEnd - iBegin) * (jEnd - jBegin)));
}

void divideOne (ImageBase& image, int iBegin, int jBegin, int iEnd, int jEnd) {
  double average = getAverage(image, iBegin, jBegin, iEnd / 2, jEnd / 2);
  cout << "var : " << getVariance(image, iBegin, jBegin, iEnd / 2, jEnd / 2, average) << endl;
    for (int i=iBegin; i < (iEnd/2); ++i) {
      for(int j=jBegin; j < (jEnd/2); ++j) {
        image[i][j] = average;
      }
    }

  average = getAverage(image, (iEnd/2), jBegin, iEnd, jEnd/2);
  cout << "var : " << getVariance(image, (iEnd/2), jBegin, iEnd, jEnd/2, average) << endl;
  for (int i=(iEnd/2); i < iEnd; ++i) {
      for(int j=jBegin; j < (jEnd/2); ++j) {
        image[i][j] = average;
      }
  }

  average = getAverage(image, (iEnd/2), (jEnd/2), iEnd, jEnd);
  cout << "var : " << getVariance(image, (iEnd/2), (jEnd/2), iEnd, jEnd, average) << endl;
  for (int i=(iEnd/2); i < iEnd; ++i) {
      for(int j=(jEnd/2); j < jEnd; ++j) {
        image[i][j] = average;
      }
  }


  average = getAverage(image, iBegin, (jEnd/2), (iEnd/2), jEnd);
  cout << "var : " << getVariance(image, iBegin, (jEnd/2), (iEnd/2), jEnd, average) << endl;
  for (int i=iBegin; i < (iEnd/2); ++i) {
      for(int j=(jEnd/2); j < jEnd; ++j) {
        image[i][j] = average;
      }
  }
  
}
/*
  double variance = getVariance(image, iBegin, jBegin, iEnd, jEnd, average);

  cout << "iBegin :" << iBegin << "  iEnd:" << iEnd << endl;
  cout << "jBegin :" << jBegin << "  jEnd:" << jEnd << endl; 
  cout << "average :" << average << "  variance:" << variance << endl;
 
  for (int i=iBegin; i < iEnd; ++i) {
    for(int j=jBegin; j < jEnd; ++j) {
      image[i][j] = average;
    }
  }

}*/

void divideRec (ImageBase& image, int iBegin, int jBegin, int iEnd, int jEnd, ImageBase& rslt) {
  if ( ((iEnd - iBegin) <= minWidth) || (((jEnd - jBegin) <= minWidth)) ) {
    return;
  }

  double average = getAverage(image, iBegin, jBegin, iEnd, jEnd);
  double variance = getVariance(image, iBegin, jBegin, iEnd, jEnd, average);

  // Recopie de la valeur moyenne dans la région cible
  for (int i=iBegin; i < iEnd; ++i) {
    for(int j=jBegin; j < jEnd; ++j) {
      rslt[i][j] = average;
    }
  }

  if ( sqrt(variance) > varianceThreshold) {
    divideRec(image, iBegin, jBegin, iBegin + ((iEnd - iBegin) / 2), jBegin + ((jEnd - jBegin) / 2), rslt);
    divideRec(image, iBegin, jBegin + ((jEnd - jBegin) / 2) , iBegin + ((iEnd - iBegin) / 2), jEnd, rslt);
    divideRec(image, iBegin + ((iEnd - iBegin ) / 2), jBegin , iEnd, jBegin + ((jEnd - jBegin) / 2), rslt);
    divideRec(image, iBegin + ((iEnd - iBegin) / 2), jBegin + ((jEnd - jBegin) / 2), iEnd, jEnd, rslt);
  }

/*    
    // Test de voisinnage :
    int i1Begin = iBegin;
    int j1Begin = jBegin;
    int j1End = jBegin + (jEnd-jBegin)/2;
    int i1End = iBegin + (iEnd-iBegin)/2;

    int i2Begin = iBegin + (iEnd-iBegin)/2;
    int j2Begin = jBegin;
    int j2End = jBegin + (jEnd-jBegin)/2;
    int i2End = iEnd;

    int i3Begin = iBegin;
    int j3Begin = jBegin + (jEnd-jBegin)/2;
    int j3End = jEnd;
    int i3End = iBegin + (iEnd-iBegin)/2;

    int i4Begin = iBegin + (iEnd-iBegin)/2;
    int j4Begin = jBegin + (jEnd-jBegin)/2;
    int j4End = jEnd;
    int i4End = iEnd;

    double diff = abs(rslt[i1Begin][j1Begin] - rslt[i2Begin][j2Begin]);
    // 1 et 2
    if ( diff <= seuil ) {
      double moyenne = (rslt[i1Begin][j1Begin] + rslt[i2Begin][j2Begin]) / 2;
      for (int i=i1Begin; i < i1End; ++i) {
        for (int j=j1Begin; j < j1End; ++j) {
          rslt[i][j] = moyenne;
        }
      }

      for (int i=i2Begin; i < i2End; ++i) {
        for (int j=j2Begin; j < j2End; ++j) {
          rslt[i][j] = moyenne;
        }
      }
    }
  
    // 1 et 3
    diff = abs(rslt[i1Begin][j1Begin] - rslt[i3Begin][j3Begin]);
   
    if ( diff <= seuil ) {
      double moyenne = (rslt[i1Begin][j1Begin] + rslt[i3Begin][j3Begin]) / 2;
      for (int i=i1Begin; i < i1End; ++i) {
        for (int j=j1Begin; j < j1End; ++j) {
          rslt[i][j] = moyenne;
        }
      }

      for (int i=i3Begin; i < i3End; ++i) {
        for (int j=j3Begin; j < j3End; ++j) {
          rslt[i][j] = moyenne;
        }
      }
    }
  

    // 4 et 2

    diff = abs(rslt[i4Begin][j4Begin] - rslt[i2Begin][j2Begin]);
    if ( diff <= seuil ){
      double moyenne = (rslt[i4Begin][j4Begin] + rslt[i2Begin][j2Begin]) / 2;
      for (int i=i4Begin; i < i4End; ++i) {
        for (int j=j4Begin; j < j4End; ++j) {
          rslt[i][j] = moyenne;
        }
      }

      for (int i=i2Begin; i < i2End; ++i) {
        for (int j=j2Begin; j < j2End; ++j) {
          rslt[i][j] = moyenne;
        }
      }
    }
  

    // 4 et 3
    diff = abs(rslt[i4Begin][j4Begin] - rslt[i2Begin][j2Begin]);
    if ( diff <= seuil ) {
      double moyenne = (rslt[i4Begin][j4Begin] + rslt[i3Begin][j3Begin]) / 2;
      for (int i=i4Begin; i < i4End; ++i) {
        for (int j=j4Begin; j < j4End; ++j) {
          rslt[i][j] = moyenne;
        }
      }

      for (int i=i3Begin; i < i3End; ++i) {
        for (int j=j3Begin; j < j3End; ++j) {
          rslt[i][j] = moyenne;
        }
      }
    
  }*/
}

void fusion (ImageBase& rslt) {
  for (int i=1; i<rslt.getHeight() -1; ++i) {
    for (int j = 1; j <rslt.getWidth()-1; ++j) {

      if (abs(rslt[i][j] - rslt[i+1][j]) < seuil) {
        rslt[i][j] = (rslt[i][j] + rslt[i+1][j]) / 2;
        rslt[i+1][j] = (rslt[i][j] + rslt[i+1][j]) / 2;
      }

      if (abs(rslt[i][j] - rslt[i][j+1]) < seuil) {
        rslt[i][j] = (rslt[i][j] + rslt[i][j+1]) / 2;
        rslt[i][j+1] = (rslt[i][j] + rslt[i][j+1]) / 2;
      }

      if (abs(rslt[i][j] - rslt[i][j-1]) < seuil) {
        rslt[i][j] = (rslt[i][j] + rslt[i][j-1]) / 2;
        rslt[i][j-1] = (rslt[i][j] + rslt[i][j-1]) / 2;
      }

      if (abs(rslt[i][j] - rslt[i-1][j]) < seuil) {
        rslt[i][j] = (rslt[i-1][j] + rslt[i][j]) / 2;
        rslt[i-1][j] = (rslt[i-1][j] + rslt[i][j]) / 2;
      }
    }
  }
}
void divide(ImageBase& image, ImageBase& rslt) {
  divideRec(image, 0, 0, image.getHeight(), image.getWidth(), rslt);
}

void griser (ImageBase& color, ImageBase& ndg) {
  int w = color.getWidth();
  int h = color.getHeight();
  
  for (int i = 0; i < h; i++) {
    for (int j = 0; j < w; j++) {
      double r = color[i*3][j*3 + 0];
      double g = color[i*3][j*3 + 1];
      double b = color[i*3][j*3 + 2];

      ndg[i][j] = int(0.299 * r + 0.587 * g + 0.114 * b); 
    }
  }
}

int main(int argc, char **argv) {

	if (argc != 2) {
	  printf("Usage: ImageIn.pgm \n"); 
		return 1;
	} 

	int num, isColonne;
	ofstream out;

	char nomImageIn[150] = "../../src/";
	strcat (nomImageIn, argv[1]);
	

  // Chargement d'une image
  ImageBase imageCouleur;
  //const char * str = nomImageIn.c_str();
  imageCouleur.load(nomImageIn);
  imageCouleur.save("imageCouleur.ppm");

  ImageBase image(imageCouleur.getWidth(), imageCouleur.getHeight(), false);
  ImageBase rslt(imageCouleur.getWidth(), imageCouleur.getHeight(), false);

  griser(imageCouleur, image);
  image.save("imageNdg.pgm");

  double average = getAverage(image, 0, 0, image.getHeight(), image.getWidth());
  double variance = getVariance(image, 0, 0, image.getHeight(), image.getWidth(), average);
  cout << average << " " << variance << endl;

  //divideOne(image, 0, 0, image.getHeight(), image.getWidth());
  divide(image, rslt);
  fusion(rslt);
  rslt.save("d20s10.pgm");
	return 0;
}
