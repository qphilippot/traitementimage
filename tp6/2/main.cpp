#include "../../lib/C++/ImageBase.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include <cmath>

using namespace std;
double elt [8] = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
void getMask(ImageBase& original, ImageBase& image, ImageBase& mask) {
  int height = original.getHeight();
  int width = original.getWidth();
  for (int i=0; i < height; ++i) {
    for (int j=0; j < width; ++j) {
      mask[i][j] = original[i][j] - image[i][j];
    }
  }
}

void fillWithAverage (ImageBase& toFill, ImageBase& mask) {
  int height = toFill.getHeight() -1;
  int width = toFill.getWidth() -1;

  for (int i=1; i < height; ++i) {
    for (int j=1; j < width; ++j) {
      if (mask[i][j] != 0) {
        double average = 0;
        int N = 0;
        if (mask[i-1][j-1] == 0) {
          average += toFill[i-1][j-1];
          N++;
        }
        if (mask[i-1][j] == 0) {
          average += toFill[i-1][j];
          N++;
        }
        if (mask[i-1][j+1] == 0) {
          average += toFill[i-1][j+1];
          N++;
        }
        if (mask[i][j-1] == 0) {
          average += toFill[i][j-1];
          N++;
        }
        if (mask[i][j+1] == 0) {
          average += toFill[i][j+1];
          N++;
        }
        if (mask[i+1][j-1] == 0) {
          average += toFill[i+1][j-1];
          N++;
        }
        if (mask[i+1][j] == 0) {
          average += toFill[i+1][j];
          N++;
        }
        if (mask[i+1][j+1] == 0) {
          average += toFill[i+1][j+1];
          N++;
        }

        if (N > 0) {
         //cout << int(average / N) << endl;
          toFill[i][j] = int (average / N);
          mask[i][j] = 0;
        }
      }
    }
  }
}


bool fillByDilatation (ImageBase& toFill, ImageBase& mask) {
  int height = mask.getHeight() -1;
  int width = mask.getWidth() -1;

  ImageBase tmp (toFill.getWidth(), toFill.getHeight(), false);
  ImageBase tmpMask (toFill.getWidth(), toFill.getHeight(), false);

  for (int i = 0; i < height; ++i) {
    for (int j=0; j < width; ++j) {
      tmp [i][j] = toFill[i][j];
      tmpMask [i][j] = mask[i][j];
    }
  }

  // Interior 
  for (int i = 1; i < height; i++) {
    for (int j = 1; j < width; j++) {
   //   cout << i << " " << j << endl;
      if (mask[i][j] != 0) {
        //cout << 0 << endl;
        double average = 0;
        double N = 0;
        if (mask[i-1][j-1] == 0) {
          average += (elt[0] * toFill[i-1][j-1]);
          N += elt[0];
        }

        if (mask[i-1][j] == 0) {
          average += (elt[1] * toFill[i-1][j]);
          N += elt[1];
        }

        if (mask[i-1][j+1] == 0) {
          average += (elt[2] * toFill[i-1][j+1]);
          N += elt[2];
        }

        if (mask[i][j-1] == 0) {
          average += (elt[3] * toFill[i][j-1]);
          N += elt[3];
        }

        if (mask[i][j+1] == 0) {
          average += (elt[4] * toFill[i][j+1]);
          N += elt[4];
        }

        if (mask[i+1][j-1] == 0) {
          average += (elt[5] * toFill[i+1][j-1]);
          N += elt[5];
        }

        if (mask[i+1][j] == 0) {
          average += (elt[6] * toFill[i+1][j]);
          N += elt[6];
        }

        if (mask[i+1][j+1] == 0) {
          average += (elt[7] * toFill[i+1][j+1]);
          N += elt[7];
        }

        if (N > 0) {
         //cout << int(average / N) << endl;
          tmp[i][j] = int (average / N);
          tmpMask[i][j] = 0;
        }
      }
    }
  }

  for (int i = 0; i < height; ++i) {
    for (int j=0; j < width; ++j) {
      toFill [i][j] = tmp[i][j];
      mask[i][j] = tmpMask[i][j];
    }
  }

  bool isFill = true;
    for (int i=0; (i < height) && isFill; ++i) {
      for (int j=0; (j < width) && isFill; ++j) {
        if (mask[i][j] > 0) isFill = false;
      }
    }
    return isFill;
  
}

void fillByDiffusion3T (ImageBase& toFill, ImageBase& mask) {
  int height = mask.getHeight() -1;
  int width = mask.getWidth() -1;

  // Interior 
  for (int i = 3; i < height; i += 3) {
    for (int j = 3; j < width; j += 3) {
      //   1
      if (mask[i][j] != 0) {
          toFill[i][j] = toFill[i-3][j-3];
          mask[i][j] = 0;
      }
      if (mask[i][j+1] != 0) {
          toFill[i][j+1] = toFill[i-3][j-2];
          mask[i][j+1] = 0;
      }
      if (mask[i][j+2] != 0) {
          toFill[i][j+2] = toFill[i-3][j-1];
          mask[i][j+2] = 0;
      }

      // 2
      if (mask[i+1][j] != 0) {
          toFill[i+1][j] = toFill[i-2][j-3];
          mask[i+1][j] = 0;
      }

      if (mask[i+1][j+1] != 0) {
          toFill[i+1][j+1] = toFill[i-2][j-2];
          mask[i+1][j+1] = 0;
      }
      if (mask[i+1][j+2] != 0) {
          toFill[i+1][j+2] = toFill[i-2][j-1];
          mask[i+1][j+2] = 0;
      }

      //3
      if (mask[i+2][j] != 0) {
          toFill[i+2][j] = toFill[i-1][j-3];
          mask[i+2][j] = 0;
      }
      if (mask[i+2][j+1] != 0) {
          toFill[i+2][j+1] = toFill[i-1][j-2];
          mask[i+2][j+1] = 0;
      }
      if (mask[i+2][j+2] != 0) {
          toFill[i+2][j+2] = toFill[i-1][j-1];
          mask[i+2][j+2] = 0;
      }
    }
  }
}

void fillByDiffusion2L (ImageBase& toFill, ImageBase& mask) {
  int height = mask.getHeight() -1;
  int width = mask.getWidth() -1;

  // Interior 
  for (int i = 0; i < height; ++i) {
    for (int j = 2; j < width; j += 2) {
      //   1
      if (mask[i][j] != 0) {
          toFill[i][j] = toFill[i][j-3];
          mask[i][j] = 0;
      }
      if (mask[i][j+1] != 0) {
          toFill[i][j+1] = toFill[i][j-2];
          mask[i][j+1] = 0;
      }
    }
  }
}
void fillByDiffusion5L (ImageBase& toFill, ImageBase& mask) {
  int height = mask.getHeight() -1;
  int width = mask.getWidth() -1;

  // Interior 
  for (int i = 0; i < height; ++i) {
    for (int j = 5; j < width; j += 5) {
      //   1
      if (mask[i][j] != 0) {
          toFill[i][j] = toFill[i][j-5];
          mask[i][j] = 0;
      }
      if (mask[i][j+1] != 0) {
          toFill[i][j+1] = toFill[i][j-4];
          mask[i][j+1] = 0;
      }
      if (mask[i][j+2] != 0) {
          toFill[i][j+2] = toFill[i][j-3];
          mask[i][j+2] = 0;
      }

      if (mask[i][j+3] != 0) {
          toFill[i][j+3] = toFill[i][j-2];
          mask[i][j+3] = 0;
      }


      if (mask[i][j+4] != 0) {
          toFill[i][j+4] = toFill[i][j-1];
          mask[i][j+4] = 0;
      }
    }
  }
}

void fillByDiffusionL (ImageBase& toFill, ImageBase& mask, int sizeOfElt) {
  int height = mask.getHeight() -1;
  int width = mask.getWidth() -1;
 
  for (int i = 0; i < height; ++i) {
    for (int j = sizeOfElt; j < width; j += sizeOfElt) {
      for (int k=0; k < sizeOfElt; k++) {
        if (mask[i][j+k] != 0) {
          toFill[i][j+k] = toFill[i][j-sizeOfElt + k];
          mask[i][j+k] = 0;
        }
      }
    }
  }
}

void fillByDiffusionT (ImageBase& toFill, ImageBase& mask, int sizeOfElt) {
  int height = mask.getHeight() -1;
  int width = mask.getWidth() -1;
 
  for (int i = sizeOfElt; i < height; i += sizeOfElt) {
    for (int j = 0; j < width; ++j) {
      for (int k=0; k < sizeOfElt; k++) {
        if (mask[i+k][j] != 0) {
          toFill[i+k][j] = toFill[i-sizeOfElt + k][j];
          mask[i+k][j] = 0;
        }
      }
    }
  }
}

void fillByDiffusionR (ImageBase& toFill, ImageBase& mask, int sizeOfLElt, int sizeOfRElt) {
  int height = mask.getHeight() -1;
  int width = mask.getWidth() -1;

int stepI = 1;
int stepJ = 1;
if (sizeOfLElt > 0) stepI = sizeOfLElt;
if (sizeOfRElt > 0) stepJ = sizeOfRElt; 
  for (int i = sizeOfLElt; i < height; i += sizeOfLElt) {
    for (int j = sizeOfRElt; j < width; j += sizeOfRElt) {
        for (int u=0; u < sizeOfLElt; u++) {
          for (int v=0; v < sizeOfRElt; v++) {
            if (mask[i+u][j+v] != 0) {
              toFill[i+u][j+v] = toFill[i-sizeOfLElt + u][j -sizeOfRElt + v];
              mask[i+u][j+v] = 0;
            }
          }
        }
      
    }
  }
}

int main(int argc, char **argv) {

	if (argc != 3) {
	  printf("Usage: ImageIn.pgm \n"); 
		return 1;
	} 

	int num, isColonne;
	ofstream out;

  char nomOriginal[150] = "../../src/";
  char nomImage[150] = "../../src/";
  strcat (nomOriginal, argv[1]);
  strcat (nomImage, argv[2]);

  // Chargement d'une image
  ImageBase original;
  ImageBase image;

  original.load(nomOriginal);
  original.save("original.pgm");

  image.load(nomImage);
  image.save("trou.pgm");

  ImageBase mask(original.getWidth(), original.getHeight(), false);

  getMask(original, image, mask);
  mask.save("mask.ppm");

  //fillWithAverage(image, mask);
  //while(!fillByDilatation(image, mask));
  //fillByDiffusion3T(image, mask);
  //fillByDiffusion5L(image, mask);
  //fillByDiffusionL(image, mask, 1);
  fillByDiffusionT(image, mask, 5);
  //fillByDiffusionR(image, mask, 1, 8);
  image.save("deleted");
	return 0;
}
