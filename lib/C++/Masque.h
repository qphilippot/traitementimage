#ifndef MASQUE_HPP
#define MASQUE_HPP

#include <string>
#include <vector>
#include "ImageBase.h"

class Masque {
private: 
	unsigned int width;
	unsigned int height;

	std::string name;

	double *data;
	
public:
	static Masque identity();

	// équivalent à carte des différences entre voisins
	// (1962) forte sensibilité au bruit en raison de la taille des masques
	static Masque robertsX();
	static Masque robertsY();

	static Masque prewittX();
	static Masque prewittY();

	static Masque prewittDirectionnelX();
	static Masque prewittDirectionnelY();

	// Invariant par rotation
	static Masque laplace4();
	static Masque laplace8();

	// 1972
	// approxime gaussienne
	static Masque sobelX();
	static Masque sobelY();

	Masque(unsigned int width = 1, unsigned int height = 1, std::string name = std::string("undefined"));
	virtual ~Masque();

	void apply(ImageBase& img);
	void setData(std::vector<double>&);
};

#endif