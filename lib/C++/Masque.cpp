#include <string>
#include <vector>
#include "ImageBase.h"
#include "Masque.h"

Masque Masque::identity() {
	Masque mask (3, 3, std::string("identity"));
	std::vector<double> data;

	data.push_back(0); data.push_back(0); data.push_back(0);
	data.push_back(0); data.push_back(1); data.push_back(0);
	data.push_back(0); data.push_back(0); data.push_back(0);

	mask.setData(data);
	return mask;
}

Masque Masque::robertsX() {
	Masque mask (2, 2, std::string("robertsX"));
	std::vector<double> data;

	data.push_back(1); data.push_back(0);
	data.push_back(0); data.push_back(-1);

	mask.setData(data);
	return mask;
}

Masque Masque::robertsY() {
	Masque mask (2, 2, std::string("robertsY"));
	std::vector<double> data;

	data.push_back(0); data.push_back(1);
	data.push_back(-1); data.push_back(0);

	mask.setData(data);
	return mask;
}

Masque Masque::prewittX() {
	Masque mask (3, 3, std::string("prewittX"));
	std::vector<double> data;

	data.push_back(-0.33); data.push_back(0); data.push_back(0.33);
	data.push_back(-0.33); data.push_back(0); data.push_back(0.33);
	data.push_back(-0.33); data.push_back(0); data.push_back(0.33);

	mask.setData(data);
	return mask;
}

Masque Masque::prewittY() {
	Masque mask (3, 3, std::string("prewittY"));
	std::vector<double> data;

	data.push_back(-0.33); data.push_back(-0.33); data.push_back(-0.33);
	data.push_back(0); data.push_back(0); data.push_back(0);
	data.push_back(0.33); data.push_back(0.33); data.push_back(0.33);

	mask.setData(data);
	return mask;
}

Masque Masque::prewittDirectionnelX() {
	Masque mask (3, 3, std::string("prewittDirectionnelX"));
	std::vector<double> data;

	data.push_back(0); data.push_back(0.33); data.push_back(0.33);
	data.push_back(-0.33); data.push_back(0); data.push_back(0.33);
	data.push_back(-0.33); data.push_back(-0.33); data.push_back(0);

	mask.setData(data);
	return mask;
}

Masque Masque::prewittDirectionnelY() {
	Masque mask (3, 3, std::string("prewittDirectionnelY"));
	std::vector<double> data;

	data.push_back(-0.33); data.push_back(-0.33); data.push_back(0);
	data.push_back(-0.33); data.push_back(0); data.push_back(0.33);
	data.push_back(0); data.push_back(0.33); data.push_back(0.33);

	mask.setData(data);
	return mask;
}

Masque Masque::laplace4() {
	Masque mask (3, 3, std::string("laplace4"));
	std::vector<double> data;

	data.push_back(0); data.push_back(1); data.push_back(0);
	data.push_back(1); data.push_back(-4); data.push_back(1);
	data.push_back(0); data.push_back(1); data.push_back(0);

	mask.setData(data);
	return mask;
}

Masque Masque::laplace8() {
	Masque mask (3, 3, std::string("prewittDirectionnelY"));
	std::vector<double> data;

	data.push_back(1); data.push_back(1); data.push_back(1);
	data.push_back(1); data.push_back(-8); data.push_back(1);
	data.push_back(1); data.push_back(1); data.push_back(1);

	mask.setData(data);
	return mask;
}

Masque Masque::sobelX() {
	Masque mask (3, 3, std::string("sobelX"));
	std::vector<double> data;

	data.push_back(-0.25); data.push_back(0); data.push_back(0.25);
	data.push_back(-0.5); data.push_back(0); data.push_back(0.5);
	data.push_back(-0.25); data.push_back(0); data.push_back(0.25);

	mask.setData(data);
	return mask;
}

Masque Masque::sobelY() {
	Masque mask (3, 3, std::string("prewittDirectionnelY"));
	std::vector<double> data;

	data.push_back(-0.25); data.push_back(-0.5); data.push_back(-0.25);
	data.push_back(0); data.push_back(0); data.push_back(0);
	data.push_back(0.25); data.push_back(0.5); data.push_back(0.25);

	mask.setData(data);
	return mask;
}

Masque::Masque(unsigned int width, unsigned int height, std::string name) :
	width(width),
	height(height),
	name(name) 
{
	unsigned int size = width * height;
	data = new double [size];

	for (unsigned int i = 0; i < size; ++i) {
		data[i] = 0;
	}
}

Masque::~Masque() {
	delete[] data;
}

/* 
	Le bais est-il correct ?
	biais = (w / 2) v (w / 2) -1 v  (w / 2) + 1
*/
void Masque::apply(ImageBase& img) {
	const unsigned int imgWidth = img.getWidth();
	const unsigned int imgHeight = img.getHeight();
	const unsigned int biaisW = width / 2;
	const unsigned int biaisH = height / 2;

	ImageBase tmp (imgWidth, imgHeight, false);
	tmp.copy(img);

	for (unsigned int i = 0; i < imgHeight; ++i) {
		for (unsigned int j = 0; j < imgWidth; ++j) {
			// On calcule la valeur du pixel (i, j)
			img[i][j] = 0;
			for (unsigned int u = 0; u < height; ++u) {
				for (unsigned int v = 0; v < width; ++v) {
					unsigned int ip = i - biaisH + u;
					unsigned int jp = j - biaisW + v;

					if ((ip < imgHeight) && (jp < imgWidth)) {
						img[i][j] += data[u * width + v] * tmp[ip][jp];
					} 
				}
			}
		}
	}
}

void Masque::setData(std::vector<double>& mask) {
	if (mask.size() == (width * height)) {
		for (unsigned int i = 0; i < height; ++i) {
			for (unsigned int j = 0; j < width; ++j) {
				data [i*width + j] = mask.at(i*width + j);
			}
		}
	}
}