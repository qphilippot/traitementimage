#include "../../lib/C++/ImageBase.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <fstream>

using namespace std;

int* getHistogrammeG(ImageBase& im) {
  int* histogramme = new int[256] ();
  for (int i=0; i<255; i++) {
    histogramme[i] = 0;
  }
  // Parcours de la grille 
  int h = im.getHeight(); 
  int w = im.getWidth();

  for (int i = 0; i< h; i++) {
    for (int j = 0 ; j < w; j++ ) {
      histogramme[int(im[i][j])]++;
    }
  }
  
  return histogramme;
}
void getHistogrammeRGB(ImageBase& im, int* R, int* V, int* B) {
  for (int i=0; i<256; i++) {
    R[i] = V[i] = B[i] = 0;
  }
  int h = im.getHeight(); 
  int w = im.getWidth();
  for (int i = 0; i< h; i++) {
    for (int j = 0 ; j < w; j++ ) {
      R[(int)im[i*3][j*3+0]]++; 
      V[(int)im[i*3][j*3+1]]++; 
      B[(int)im[i*3][j*3+2]]++;
    }
  }
}

void seuillerRGB (ImageBase& im, int minR, int maxR, int minV, int maxV, int minB, int maxB) {
  int w = im.getWidth();
  int h = im.getHeight();
  
  for (int i = 0; i < h; i++) {
    for (int j = 0; j < w; j++) {
      // Seuillage R
      if (im[i*3][j*3 + 0] < minR) {
	im[i*3][j*3 + 0] = 0;
      } else {
	if (im[i*3][j*3 + 0] > maxR) {
	  im[i*3][j*3 + 0] = 255;
	}
      }
      // Seuillage V
      if (im[i*3][j*3 + 1] < minV) {
	im[i*3][j*3 + 1] = 0;
      } else {
	if (im[i*3][j*3 + 1] > maxV) {
	  im[i*3][j*3 + 1] = 255;
	}
      }
      // Seuillage B
      if (im[i*3][j*3 + 2] < minB) {
	im[i*3][j*3 + 2] = 0;
      } else {
	if (im[i*3][j*3 + 2] > maxB) {
	  im[i*3][j*3 + 2] = 255;
	}
      }
    }
  }
}

int getAMin (int* histogramme) {
  int plusPetitGris = 0;
  while ( (histogramme[plusPetitGris] == 0) && (plusPetitGris < 255) ) {
    plusPetitGris++;
  }
  return plusPetitGris;
}

int getAMax (int* histogramme) {
  int plusGrandGris = 255;
  while ( (histogramme[plusGrandGris] == 0) && (plusGrandGris >= 0)) {
    plusGrandGris--;
  }
  return plusGrandGris;
}

void expansionDynamique (ImageBase& im, int* histogramme) {
  int aMin = getAMin(histogramme);
  int aMax = getAMax(histogramme);
  double alpha = (-255 * aMin) / (aMax - aMin);
  double beta = 255 / (aMax - aMin);

  cout << alpha << endl;
  cout << beta << endl;
  int w = im.getWidth();
  int h = im.getHeight();
  
  for (int i = 0; i < h; i++) {
    for (int j = 0; j < w; j++) {
      im[i][j] = alpha + beta * im[i][j];
    }
  }
  
}

void expansionDynamiqueRGB (ImageBase& im, int* r, int* v, int* b) {
  int aMinR = getAMin(r);
  int aMaxR = getAMax(r);
  double alphaR = (-255 * aMinR) / (aMaxR - aMinR);
  double betaR = 255 / (aMaxR - aMinR);

  cout << alphaR << endl;
  cout << betaR << endl;

  int aMinV = getAMin(v);
  int aMaxV = getAMax(v);
  double alphaV = (-255 * aMinV) / (aMaxV - aMinV);
  double betaV = 255 / (aMaxV - aMinV);

  cout << alphaV << endl;
  cout << betaV << endl;

  int aMinB = getAMin(b);
  int aMaxB = getAMax(b);
  double alphaB = (-255 * aMinB) / (aMaxB - aMinB);
  double betaB = 255 / (aMaxB - aMinB);

  cout << alphaB << endl;
  cout << betaB << endl;

  int w = im.getWidth();
  int h = im.getHeight();
  
  for (int i = 0; i < h; i++) {
    for (int j = 0; j < w; j++) {
      im[i*3][j*3 + 0] = alphaR + betaR * im[i*3][j*3 + 0];
      im[i*3][j*3 + 1] = alphaV + betaV * im[i*3][j*3 + 1];
      im[i*3][j*3 + 2] = alphaB + betaB * im[i*3][j*3 + 2];
    }
  }
  
}

void griser (ImageBase& color, ImageBase& ndg) {
  int w = color.getWidth();
  int h = color.getHeight();
  
  for (int i = 0; i < h; i++) {
    for (int j = 0; j < w; j++) {
      double r = color[i*3][j*3 + 0];
      double g = color[i*3][j*3 + 1];
      double b = color[i*3][j*3 + 2];

      ndg[i][j] = int(0.299 * r + 0.587 * g + 0.114 * b); 
    }
  }
}

void seuiller (ImageBase* imIn, int seuil) {
  for (int x = 0; x < imIn->getHeight(); ++x) {
    for(int y = 0; y < imIn->getWidth(); ++y) {
      if ((*imIn)[x][y] < seuil) {
	(*imIn)[x][y] = 0;
      } else {
     	(*imIn)[x][y] = 255;
      }
    }
  }
}

void flouter (ImageBase& couleur) {
   int w = couleur.getWidth();
   int h = couleur.getHeight();
   
   ImageBase copie (w,h,true);

   for (int i = 0; i < h; i++) {
     for (int j = 0; j < w; j++) {
       copie[i*3][j*3 + 0] = couleur[i*3][j*3 + 0];
       copie[i*3][j*3 + 1] = couleur[i*3][j*3 + 1];
       copie[i*3][j*3 + 2] = couleur[i*3][j*3 + 2];
     }
   }
   h--;
   w--;
   for (int i = 1; i < h; i++) {
     for (int j = 1; j < w; j++) {
       for (int k = 0; k < 3; k++) {
	 double p1 = copie[(i-1)*3][(j-1)*3 + k];
	 double p2 = copie[(i-1)*3][j*3+k];
	 double p3 = copie[(i-1)*3][(j+1)*3 + k];
	 double p4 = copie[i*3][(j-1)*3 + k];
	 double p5 = copie[i*3][(j+1)*3 + k];
	 double p6 = copie[(i+1)*3][(j-1)*3 + k];
	 double p7 = copie[(i+1)*3][j*3 + k];
	 double p8 = copie[(i+1)*3][(j+1)*3 + k];
	 
	 int pixel =  (1.0/9) * (p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8);
	 couleur[i*3][j*3 + k] = pixel;
       }
     }
   }
}

int main(int argc, char **argv) {

	if (argc != 2) {
	  printf("Usage: ImageIn.pgm \n"); 
		return 1;
	} 

	int num, isColonne;
	ofstream out;
	
	char nomImageIn[150] = "../../src/";
	strcat (nomImageIn, argv[1]);
       
	// Chargement d'une image
	ImageBase imageCouleur;
	//const char * str = nomImageIn.c_str();
	imageCouleur.load(nomImageIn);
	flouter(imageCouleur);
	imageCouleur.save("floute.ppm");

	return 0;
}
