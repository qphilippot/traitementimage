#include "../../lib/C++/ImageBase.h"
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <fstream>

using namespace std;

int* getHistogrammeG(ImageBase& im) {
  int* histogramme = new int[256] ();
  for (int i=0; i<255; i++) {
    histogramme[i] = 0;
  }
  // Parcours de la grille 
  int h = im.getHeight(); 
  int w = im.getWidth();

  for (int i = 0; i< h; i++) {
    for (int j = 0 ; j < w; j++ ) {
      histogramme[int(im[i][j])]++;
    }
  }
  
  return histogramme;
}
void getHistogrammeRGB(ImageBase& im, int* R, int* V, int* B) {
  for (int i=0; i<256; i++) {
    R[i] = V[i] = B[i] = 0;
  }
  int h = im.getHeight(); 
  int w = im.getWidth();
  for (int i = 0; i< h; i++) {
    for (int j = 0 ; j < w; j++ ) {
      R[(int)im[i*3][j*3+0]]++; 
      V[(int)im[i*3][j*3+1]]++; 
      B[(int)im[i*3][j*3+2]]++;
    }
  }
}

void seuiller (ImageBase& im, int minR, int maxR, int minV, int maxV, int minB, int maxB) {
  int w = im.getWidth();
  int h = im.getHeight();
  
  for (int i = 0; i < h; i++) {
    for (int j = 0; j < w; j++) {
      // Seuillage R
      if (im[i*3][j*3 + 0] < minR) {
	im[i*3][j*3 + 0] = 0;
      } else {
	if (im[i*3][j*3 + 0] > maxR) {
	  im[i*3][j*3 + 0] = 255;
	}
      }
      // Seuillage V
      if (im[i*3][j*3 + 1] < minV) {
	im[i*3][j*3 + 1] = 0;
      } else {
	if (im[i*3][j*3 + 1] > maxV) {
	  im[i*3][j*3 + 1] = 255;
	}
      }
      // Seuillage B
      if (im[i*3][j*3 + 2] < minB) {
	im[i*3][j*3 + 2] = 0;
      } else {
	if (im[i*3][j*3 + 2] > maxB) {
	  im[i*3][j*3 + 2] = 255;
	}
      }
    }
  }
}

int getAMin (int* histogramme) {
  int plusPetitGris = 0;
  while ( (histogramme[plusPetitGris] == 0) && (plusPetitGris < 255) ) {
    plusPetitGris++;
  }
  return plusPetitGris;
}

int getAMax (int* histogramme) {
  int plusGrandGris = 255;
  while ( (histogramme[plusGrandGris] == 0) && (plusGrandGris >= 0)) {
    plusGrandGris--;
  }
  return plusGrandGris;
}

void expansionDynamique (ImageBase& im, int* histogramme) {
  int aMin = getAMin(histogramme);
  int aMax = getAMax(histogramme);
  double alpha = (-255 * aMin) / (aMax - aMin);
  double beta = 255 / (aMax - aMin);

  cout << alpha << endl;
  cout << beta << endl;
  int w = im.getWidth();
  int h = im.getHeight();
  
  for (int i = 0; i < h; i++) {
    for (int j = 0; j < w; j++) {
      im[i][j] = alpha + beta * im[i][j];
    }
  }
  
}

void expansionDynamiqueRGB (ImageBase& im, int* r, int* v, int* b) {
  int aMinR = getAMin(r);
  int aMaxR = getAMax(r);
  double alphaR = (-255 * aMinR) / (aMaxR - aMinR);
  double betaR = 255 / (aMaxR - aMinR);

  cout << alphaR << endl;
  cout << betaR << endl;

  int aMinV = getAMin(v);
  int aMaxV = getAMax(v);
  double alphaV = (-255 * aMinV) / (aMaxV - aMinV);
  double betaV = 255 / (aMaxV - aMinV);

  cout << alphaV << endl;
  cout << betaV << endl;

  int aMinB = getAMin(b);
  int aMaxB = getAMax(b);
  double alphaB = (-255 * aMinB) / (aMaxB - aMinB);
  double betaB = 255 / (aMaxB - aMinB);

  cout << alphaB << endl;
  cout << betaB << endl;

  int w = im.getWidth();
  int h = im.getHeight();
  
  for (int i = 0; i < h; i++) {
    for (int j = 0; j < w; j++) {
      im[i*3][j*3 + 0] = alphaR + betaR * im[i*3][j*3 + 0];
      im[i*3][j*3 + 1] = alphaV + betaV * im[i*3][j*3 + 1];
      im[i*3][j*3 + 2] = alphaB + betaB * im[i*3][j*3 + 2];
    }
  }
  
}

void griser (ImageBase& color, ImageBase& ndg) {
  int w = color.getWidth();
  int h = color.getHeight();
  
  for (int i = 0; i < h; i++) {
    for (int j = 0; j < w; j++) {
      double r = color[i*3][j*3 + 0];
      double g = color[i*3][j*3 + 1];
      double b = color[i*3][j*3 + 2];

      ndg[i][j] = int(0.299 * r + 0.587 * g + 0.114 * b); 
    }
  }
}

int main(int argc, char **argv) {

	if (argc != 2) {
	  printf("Usage: ImageIn.pgm \n"); 
		return 1;
	} 

	int num, isColonne;
	ofstream out;

	char nomImageIn[150] = "../../src/";
	strcat (nomImageIn, argv[1]);
	

       
	// Chargement d'une image
	ImageBase imageCouleur;
	//const char * str = nomImageIn.c_str();
	imageCouleur.load(nomImageIn);
	imageCouleur.save("image.ppm");

	ImageBase imageNDG(imageCouleur.getWidth(), imageCouleur.getHeight(), false);

	griser(imageCouleur, imageNDG);
	imageNDG.save("ndg.pgm");

	
	
	int r[256] = {};
	int v[256] = {};
	int b[256] = {};

	getHistogrammeRGB(imageCouleur, r,v,b);
	out.open("couleur.dat");
	
	for (int i = 0; i < 256; i++) {
	  out << i << " "<< r[i] << " " << v[i] << " " << b[i] << endl;
	}
	
	out.close();

	int* histogramme = getHistogrammeG(imageNDG);
	
	out.open("ndg.dat");
	for (int i = 1; i < 255; i++) {
	  out << i << " "<< r[i] << " " << v[i] << " " << b[i] << endl;
	}
	out.close();
	return 0;
}
