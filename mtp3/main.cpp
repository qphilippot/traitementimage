#include "../lib/C++/ImageBase.h"
#include "../lib/C++/huffman.h"

#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <sstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <fstream>

using namespace std;

ImageBase griser (ImageBase& color) {
  const unsigned int w = color.getWidth();
  const unsigned int h = color.getHeight();
  
  ImageBase ndg (w, h, false);
  for (int i = 0; i < h; ++i) {
    for (int j = 0; j < w; ++j) {
      double r = color[i*3][j*3 + 0];
      double g = color[i*3][j*3 + 1];
      double b = color[i*3][j*3 + 2];

      // Calcul de la luminance
      ndg[i][j] = int(0.299 * r + 0.587 * g + 0.114 * b); 
    }
  }

  return ndg;
}

int main(int argc, char **argv) {
	if (argc != 2) {
		printf("Usage: prog <fileName> \n"); 
		return 1;
	}
	
	char* fileName = argv[1];
	
  ImageBase originale;
  originale.load(fileName);


    originale.save("originale.pgm");
  if (originale.getColor()) {
    originale.griser();
    //originale.copy(griser(originale));
    originale.save("grise.pgm");
  }

   FILE *src, *dst, *frq, *rest;


   /* Ouverture du fichier source en lecture */
   if ((src=fopen("15_Girafes.pgm", "rb"))==NULL)
   {
      perror("fopen");
      return -1;
   }


   /* Ouverture du fichier source en lecture */
   if ((rest=fopen("restore.pgm", "wb"))==NULL)
   {
      perror("fopen tralala");
      return -1;
   }

   /* Ouverture du fichier cible en écriture */
   if ((dst=fopen("compressed.pgm", "wb"))==NULL)
   {
      perror("fopen");
      return -1;
   }

   /* Ouverture d'un éventuel fichier de liste de fréquences, en lecture */
   if (argc>4)
   {
      if ((frq=fopen(argv[4], "rb"))==NULL)
      {
         perror("fopen");
         return -1;
      }
   }
   else
      frq=NULL;

   /* Allocation mémoire pour les données diverses necessaires à la construction de l'arbre */
   if ((arbre_d=(struct arbre_data *)malloc(512*sizeof(struct arbre_data)))==NULL)
   {
      perror("malloc");
      return -1;
   }

   /* Allocation d'une zone mémoire pour l'arbre */
   if ((arbre=(struct arbre *)malloc(512*sizeof(struct arbre)))==NULL)
   {
      free(arbre_d);
      perror("malloc");
      return -1;
   }


  // Parcours de la grille
  unsigned int occurence[256] = {0}; 
  int h = originale.getHeight(); 
  int w = originale.getWidth();
  for (int i = 0; i< h; i++) {
    for (int j = 0 ; j < w; j++ ) {
      occurence[(unsigned int)originale[i][j]]++;
    }
  }

  fstream histo;
  histo.open("histo.dat", fstream::in | fstream::out | fstream::app);
  for (int i = 0; i < 256; i++) { 
    histo << i << " "<< occurence[i] << endl;
  }

  histo.close();
  
  ImageBase e (w,h,false);
   for (int i = 0; i < h; i++) {
    for (int j = 0 ; j < w; j++ ) {
      e[i][j] = ((j != 0) ? (originale[i][j] - originale[i][j-1] + 128) : originale[i][j]);
    }
  }
  e.save("difference.pgm");


for (int i = 0; i < 256; i++) {
  occurence[i] = 0;
}

  for (int i = 0; i< h; i++) {
    for (int j = 0 ; j < w; j++ ) {
      occurence[(unsigned int)e[i][j]]++;
    }
  }
  histo.open("diff.dat", fstream::in | fstream::out | fstream::app);
  for (int i = 0; i < 256; i++) { 
    histo << i << " "<< occurence[i] << endl;
  }

  histo.close();
   

   FILE *diff;


   /* Ouverture du fichier source en lecture */
   if ((diff=fopen("difference.pgm", "rb"))==NULL)
   {
      perror("fopen");
      return -1;
   }
  //huffman_creer_fichier_frequences(src, frq);
   huffman_compacter(src, dst, frq);
   huffman_compacter(diff, rest, NULL);
   //huffman_decompacter(dst, rest, frq);
/*
   if (*argv[1]=='c')
      huffman_compacter(src, dst, frq);
   else if (*argv[1]=='d')
      huffman_decompacter(src, dst, frq);
   else if (*argv[1]=='f')
      huffman_creer_fichier_frequences(src, dst);
*/
   /* Libération de la mémoire */
   free(arbre_d);
   free(arbre);

   /* Fermeture des fichiers */
   fclose(src);
   fclose(dst);
   fclose(rest);
   if (frq!=NULL)
      fclose(frq);

  #ifdef LINUX_COMPIL
     printf("Linux rules!\n");
  #endif


	return 0;
}
