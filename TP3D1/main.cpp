#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../lib/C++/ImageBase.h"

using namespace std;

int main(int argc, char **argv) {
  if (argc != 5) {
    printf("Usage: Image.img height width pronf\n"); 
    return 1;
  }

  const char* filename = argv[1];
  int width = atoi(argv[2]);
  int height = atoi(argv[3]);
  int epa = atoi(argv[4]);
  
  FILE* file = fopen(filename, "rb");
  unsigned short min = 9999;
  unsigned short max = 0;
  //cout << nbElementX << endl;
  //int* line = new int [nbElementX];
  unsigned char elt[2];
  
  unsigned short*** img = new unsigned short** [width];
  for (int i=0; i<width;i++) {
    img[i] = new unsigned short* [height];
    for (int j=0; j<height; j++) {
      img[i][j] = new unsigned short [epa];
    }
  }

  for (int k=0; k<epa; k++) {
    for (int j=0; j<height; j++) {
      for (int i=0; i<width;i++) {
	fread(&elt, sizeof(elt), 1, file);
	unsigned short line = (elt[0]*256 + elt[1]);
	img[i][j][k] = line;
	if (line > max) max = line;
	if (line < min) min = line;
      }
    }
  }

  //  Affichage des paramètres :
  cout << "max :" << max << endl;
  cout << "min :" << min << endl;
  cout << "(200,200,20) :" << img[200][200][20] << endl;

  // Reconstruction de l'image :

  
  ImageBase im(width, height, false); // Création de la structure contenant l'image ppm
  
  for (int i=0; i< width;i++) {
     for (int j=0; j< height;j++) {
       int min = 256;
        for (int k=0; k<epa;k++) {
	  if (img[i][j][k] < min) min = img[i][j][k]; 
	}
	im[height-1-j][i] = min;
     }
  }
  
  im.save("imageMinIP.ppm");


   for (int i=0; i< width;i++) {
     for (int j=0; j< height;j++) {
       int sum = 0;
        for (int k=0; k<epa;k++) {
	  sum += img[i][j][k];
	}
	int average = int(double(sum) / epa);
	if (average > 255) average = 255;
	im[height-j-1][i] = average;
     }
   }

      
 // Normalisation :
   // recherche du max
   int max2 = 0;
    for (int j=0; j<height; j++) {
      for (int i=0; i<width;i++) {
	if (im[j][i] > max2) max2 = im[j][i];
    }
  }
    cout << "max2:" << max2 << endl;
    // Normalisation :
      for (int j=0; j<height; j++) {
	for (int i=0; i<width;i++) {
	  im[j][i] = (int) (255 * ((double) im[j][i] / max2));
	}
    }
   
   /*
 // Normalisation :
   // seuillage
    for (int j=0; j<height; j++) {
      for (int i=0; i<width;i++) {
	if (im[j][i] > 255) im[j][i] = 255;
    }
  }*/
  im.save("imageAIP.ppm");
  
  // Normalisation :
  for (int k=0; k<epa; k++) {
    for (int j=0; j<height; j++) {
      for (int i=0; i<width;i++) {
	img[i][j][k] = 255 * ((double)img[i][j][k] / max);
	//if(img[i][j][k] > 255) img[i][j][k] = 255;
	//cout << img[i][j][k] << endl;
      }
    }
  }
  
  
  //
  
    
  for (int i=0; i<width;i++) {
     for (int j=0; j <height;j++) {
       int max = 0;
        for (int k=0; k<epa;k++) {
	  if (img[i][j][k] > max) max = img[i][j][k]; 
	}
       	im[height-j-1][i] = max;
     }
  }
  im.save("imageMIP.ppm");

}
